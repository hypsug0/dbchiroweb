//$.fn.select2.defaults.set("theme", "bootstrap");
// $.fn.select2.defaults.set("theme", "classic");

const asyncForEach = async (array, callback) => {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array)
    }
}

Vue.component('ValidationProvider', VeeValidate.ValidationProvider)

const getCookie = (name) => {
    let cookieValue = null
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';')
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim()
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === name + '=') {
                cookieValue = decodeURIComponent(
                    cookie.substring(name.length + 1)
                )
                break
            }
        }
    }
    return cookieValue
}

const $http = axios.create({
    withCredentials: true,
})

const csrftoken = getCookie('csrftoken')
console.debug('set csrftoken', csrftoken)
if (csrftoken) {
    console.debug('set csrftoken', csrftoken)
    $http.defaults.headers.common['X-CSRFToken'] = csrftoken
}

const _debounce = (func, timeout = 300) => {
    let timer
    return (...args) => {
        clearTimeout(timer)
        timer = setTimeout(() => {
            func.apply(this, args)
        }, timeout)
    }
}

const storeDicts = () => {
    // if (localStorage.getItem('dicts') === null) {
    if (!localStorage.hasOwnProperty('dicts')) {
        $http
            .get('/api/v1/dicts')
            .then((response) => {
                console.debug('Prepare to store dicts in localStorage')
                localStorage.setItem('dicts', JSON.stringify(response.data))
                localStorage.setItem(
                    'dictSpecies',
                    JSON.stringify(
                        response.data.Specie.sort((a, b) => {
                            const ca = a.codesp,
                                cb = b.codesp
                            return ca < cb ? -1 : ca > cb ? 1 : 0
                        }).sort((a, b) => Number(b.sp_true) - Number(a.sp_true))
                    )
                )
            })
            .catch((err) => {
                console.error(err)
            })
    }
}


const storeNomenclatures = () => {
    // if (localStorage.getItem('dicts') === null) {
    if (userId && !localStorage.hasOwnProperty('nomenclatures')) {
        const params = { with_nomenclatures: true, active: true }
        $http
            .get('/api/v1/nomenclatures/types', { params })
            .then((response) => {
                if ((Object.keys(response.data)).includes('results')) {
                    localStorage.setItem('nomenclatures', JSON.stringify(response.data.results))
                }
            })
            .catch((err) => {
                console.error(err)
            })
    }
}

storeDicts()
storeNomenclatures()

const dataTableLanguage = {
    processing: 'Traitement en cours...',
    search: 'Rechercher&nbsp;:',
    lengthMenu: 'Afficher _MENU_ &eacute;l&eacute;ments',
    info:
        "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
    infoEmpty:
        "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
    infoFiltered: '(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)',
    infoPostFix: '',
    loadingRecords: 'Chargement en cours...',
    zeroRecords: 'Aucun &eacute;l&eacute;ment &agrave; afficher',
    emptyTable: 'Aucune donnée disponible dans le tableau',
    paginate: {
        first: 'Premier',
        previous: 'Pr&eacute;c&eacute;dent',
        next: 'Suivant',
        last: 'Dernier',
    },
    aria: {
        sortAscending: ': activer pour trier la colonne par ordre croissant',
        sortDescending: ': activer pour trier la colonne par ordre décroissant',
    },
}

const searchControlOptions = {
    url:
        'https://nominatim.openstreetmap.org/search?format=json&accept-language=fr-FR&q={s}',
    jsonpParam: 'json_callback',
    propertyName: 'display_name',
    propertyLoc: ['lat', 'lon'],
    markerLocation: true,
    autoType: true,
    autoCollapse: true,
    minLength: 2,
    zoom: 13,
    text: 'Rechercher...',
    textCancel: 'Annuler',
    textErr: 'Erreur',
}

// Map objects and functions

var dataLoaded = {
    metaPlace: false,
    Place: false,
}

const addSpinner = () => {
    $('#main').append("<div id='spinner'></div>")
}

const displaySpinner = (dataState) => {
    const spinner = $('#spinner')
    if (!dataState) {
        spinner.hide()
    } else {
        spinner.show()
    }
}

const checkLoadStates = (states) =>
    states.metaPlace && states.Place ? false : true
