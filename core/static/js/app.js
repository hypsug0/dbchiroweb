/*****************************************************************
 * Fix Bootstrap droprown-menu hidden in table bottom in panels  *
 *****************************************************************/

// fix menu overflow under the responsive table
// Source: https://stackoverflow.com/questions/26018756/bootstrap-button-drop-down-inside-responsive-table-not-visible-because-of-scroll
$(document).click(function (event) {
    //hide all our dropdowns
    $('.dropdown-menu[data-parent]').hide()
})

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})
$(function () {
    $('[data-toggle="popover"]').popover()
})

const copyTextContent = (element) => {
    console.debug('element', element)
    navigator.clipboard.writeText(element.textContent)
    //lert('Texte copié: ' + element.textContent)
    console.debug(element.textContent)
    $('#' + element.id).attr('data-original-title', 'UUID copié')
    console.debug('w#', $('#' + element.id).tooltip())

    // $('#' + element.id).attr('data-original-title', 'UUID copié')
}

$(document).on('click', '.table-responsive [data-toggle="dropdown"]', () => {
    // if the button is inside a modal
    if ($('body').hasClass('modal-open')) {
        throw new Error(
            'This solution is not working inside a responsive table inside a modal, you need to find out a way to calculate the modal Z-index and add it to the element'
        )
    }

    $buttonGroup = $(this).parent()
    if (!$buttonGroup.attr('data-attachedUl')) {
        const ts = +new Date()
        $ul = $(this).siblings('ul')
        $ul.attr('data-parent', ts)
        $buttonGroup.attr('data-attachedUl', ts)
        $(window).resize(function () {
            $ul.css('display', 'none').data('top')
        })
    } else {
        $ul = $('[data-parent=' + $buttonGroup.attr('data-attachedUl') + ']')
    }
    if (!$buttonGroup.hasClass('open')) {
        $ul.css('display', 'none')
        return
    }
    dropDownFixPosition($(this).parent(), $ul)

    const dropDownFixPosition = (button, dropdown) => {
        const dropDownTop = button.offset().top + button.outerHeight()
        dropdown.css('top', dropDownTop + 'px')
        dropdown.css('left', button.offset().left + 'px')
        dropdown.css('position', 'absolute')
        dropdown.css('width', dropdown.width())
        dropdown.css('heigt', dropdown.height())
        dropdown.css('display', 'block')
        dropdown.appendTo('body')
    }
})

/********************
 *    SIDEBAR       *
 ********************/
$(document).ready(function () {
    const trigger = $('.sidebar-open-icon')
        // overlay = $('.overlay')
    let isClosed = false

    trigger.click(() => {
        sideBarOpenIconCross()
    })

    const sideBarOpenIconCross = () => {
        if (isClosed == true) {
            // overlay.hide()
            trigger.removeClass('is-open')
            trigger.addClass('is-closed')
            trigger
                .children('.fas')
                .removeClass('fa-chevron-circle-left')
                .addClass('fa-chevron-circle-right')
        } else {
            // overlay.show()
            trigger.removeClass('is-closed')
            trigger.addClass('is-open')
            trigger
                .children('.fas')
                .removeClass('fa-chevron-circle-right')
                .addClass('fa-chevron-circle-left')
        }
        isClosed = !isClosed
    }

    $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled')
    })
})

const isInteger = (value) => {
    if (typeof value != 'string') return false
    return (
        !isNaN(value) &&
        parseInt(Number(value)) == value &&
        !isNaN(parseInt(value, 10))
    )
}

const getQueryParams = (params, url) => {
    let href = url
    // this is an expression to get query strings
    let regexp = new RegExp('[?&]' + params + '=([^&#]*)', 'i')
    let qString = regexp.exec(href)
    return qString ? qString[1] : null
}

const getQueryStrings = () => {
    // const qs = new URLSearchParams(window.location.href.search)
    // const assoc = {}
    // var decode = function (s) {
    //     return decodeURIComponent(s.replace(/\+/g, ' '))
    // }
    // const queryString = location.search.substring(1)
    // const searchTest = new URLSearchParams(location.search)
    // console.debug('searchTest',searchTest)
    // console.debug(queryString)
    // const keyValues = queryString.split('&')
    // console.debug(keyValues)

    // keyValues.forEach((e, i) => {
    //     key = e.split('=')
    //     if (key.length > 1) {
    //         assoc[decode[key[0]]] = decode([key[1]])
    //     }
    // })
    // console.debug(assoc)
    // return assoc
    function paramsToObject(entries) {
        const result = {}
        for (const [key, value] of entries) {
            // each 'entry' is a [key, value] tupple
            result[key] = isInteger(value) ? parseInt(value) : value
        }
        return result
    }
    const urlParams = new URLSearchParams(location.search)
    const entries = urlParams.entries() //returns an iterator of decoded [key,value] tuples
    const params = paramsToObject(entries) //{abc:"foo",def:"[asf]",xyz:"5"}
    return params
}
console.debug('getQueryStrings', getQueryStrings())

/***************************
 * Some Leaflet components *
 ***************************/
const mapPopupOptions = {
    maxWidth: '500',
    // className: 'leaflet-popup'
}

const placeMarkerStyle = {
    radius: 5,
    fillColor: '#ff7800',
    color: '#000',
    weight: 1,
    opacity: 1,
    fillOpacity: 0.8,
}

const initMap = (mapId, legend=true) => {
    console.log('INITMAP', mapId)
    const mapScale = L.control.scale()
    const mapTileLayerControl = new L.control.layers(null, null)
    mapTileLayers.forEach((e) =>
        mapTileLayerControl.addBaseLayer(
            L.tileLayer(e[1], { attribution: e[2] }),
            e[0]
        )
    )
    const defaultLayer = L.tileLayer(mapTileLayers[0][1], {
        attribution: mapTileLayers[0][2],
    })

    const locateControl = L.control.locate({
        position: "topleft",
        strings: {
          title: 'Ma position'
        },
        icon: 'fa fa-location-arrow',
      })
    const fullscreenControl = new L.Control.Fullscreen({
        pseudoFullscreen: true,
        title: {
            'false': 'Voir en plein écran',
            'true': 'Sortir du plein écran'
        }
    })

    map = L.map(mapId)
    console.log('fullscreenControl', fullscreenControl)
    const zoomControl = new L.control.zoom({
        zoomInTitle: "Zoom avant",
        zoomOutTitle: "Zoom arrière"
    })
    map.zoomControl.remove()
    map.setView(defaultCenter, 10)
    mapTileLayerControl.addTo(map)
    defaultLayer.addTo(map)
    mapScale.addTo(map)
    zoomControl.addTo(map)
    locateControl.addTo(map)
    fullscreenControl.addTo(map)
    if (legend) {
        const legendControl = new L.control.Legend({
            title: "Légende",
            collapsed: true,
            position: "bottomright",
            legends: [{
                label: "Localité",
                type: "circle",
                radius: 5,
                fillColor: '#ff7800',
                color: '#000',
                weight: 1,
                opacity: 1,
                fillOpacity: 0.8,
            },
            {
                label: "Métasite",
                type: "polygon",
                sides: "5",
                color: '#8A2BE2',
                fillColor: '#9370DB',
                weight: 2,
                opacity: 0.8,
                fillOpacity: 0.2,
            }]
        })
        legendControl.addTo(map)
    }
    return map
}

/********************
 * Vue Components   *
 ********************/

Vue.component('ValidationProvider', VeeValidate.ValidationProvider)
Vue.component('ValidationObserver', VeeValidate.ValidationObserver)
Vue.component('v-select', VueSelect.VueSelect)

Vue.component('autocomplete', {
    template: `
        <div class="autocomplete">
    <div class="autocomplete__box" :class="{'autocomplete__searching' : showResults}">

      <!--<img v-if="!isLoading" class="autocomplete__icon" src="../assets/search.svg">-->
      <!--<img v-else class="autocomplete__icon animate-spin" src="../assets/loading.svg">-->

      <div class="autocomplete__inputs form-group form-group-sm">
      <label v-if="label" class="control-label">{{label}}</label>
        <input class="form-control"
          v-model="display"
          :placeholder="placeholder"
          :disabled="disableInput"
          :maxlength="maxlength"
          :class="inputClass"
          @click="search"
          @input="search"
          @keydown.enter="enter"
          @keydown.tab="close"
          @keydown.up="up"
          @keydown.down="down"
          @keydown.esc="close"
          @focus="focus"
          @blur="blur"
          type="text"
          autocomplete="off">
        <input :name="name" type="hidden" :value="value">
      </div>

      <!-- clearButtonIcon -->
      <span v-show="!disableInput && !isEmpty && !isLoading && !hasError" class="autocomplete__icon autocomplete--clear" @click="clear">
        <span v-if="clearButtonIcon" :class="clearButtonIcon"></span>
        <i v-else class="fa fa-times"></i>
        <!--<img v-else src="../assets/close.svg">-->

      </span>
    </div>

    <ul v-show="showResults" class="autocomplete__results" :style="listStyle">
      <slot name="results">
        <!-- error -->
        <li v-if="hasError" class="autocomplete__results__item autocomplete__results__item--error">{{ error }}</li>

        <!-- results -->
        <template v-if="!hasError">
          <slot name="firstResult"></slot>
          <li
              v-for="(result, key) in results"
              :key="key"
              @click.prevent="select(result)"
              class="autocomplete__results__item"
              :class="{'autocomplete__selected' : isSelected(key) }"
              v-html="formatDisplay(result)">
          </li>
          <slot name="lastResult"></slot>
        </template>

        <!-- no results -->
        <li v-if="noResultMessage" class="autocomplete__results__item autocomplete__no-results">
          <slot name="noResults">No Results.</slot>
        </li>
      </slot>
    </ul>
  </div>`,
    props: {
        /**
         * Data source for the results
         *   `String` is a url, typed input will be appended
         *   `Function` received typed input, and must return a string; to be used as a url
         *   `Array` and `Object` (see `results-property`) are used directly
         */
        source: {
            type: [String, Function, Array, Object],
            required: true,
        },
        label: {
            type: String,
            default: null,
        },
        /**
         * http method
         */
        method: {
            type: String,
            default: 'get',
        },
        /**
         * Input placeholder
         */
        placeholder: {
            default: 'Search',
        },
        /**
         * Preset starting value
         */
        initialValue: {
            type: [String, Number],
        },
        /**
         * Preset starting display value
         */
        initialDisplay: {
            type: String,
        },
        /**
         * CSS class for the surrounding input div
         */
        inputClass: {
            type: [String, Object],
        },
        /**
         * To disable the input
         */
        disableInput: {
            type: Boolean,
        },
        /**
         * name property of the input holding the selected value
         */
        name: {
            type: String,
        },
        /**
         * api - property of results array
         */
        resultsProperty: {
            type: String,
        },
        /**
         * Results property used as the value
         */
        resultsValue: {
            type: String,
            default: 'id',
        },
        /**
         * Results property used as the display
         */
        resultsDisplay: {
            type: [String, Function],
            default: 'name',
        },
        /**
         * Callback to format the server data
         */
        resultsFormatter: {
            type: Function,
        },
        /**
         * Whether to show the no results message
         */
        showNoResults: {
            type: Boolean,
            default: true,
        },
        /**
         * Additional request headers
         */
        requestHeaders: {
            type: Object,
        },
        /**
         * Credentials: same-origin, include, *omit
         */
        credentials: {
            type: String,
        },
        /**
         * Optional clear button icon class
         */
        clearButtonIcon: {
            type: String,
        },
        /**
         * Optional max input length
         */
        maxlength: {
            type: Number,
        },
    },
    data() {
        return {
            value: null,
            display: null,
            results: null,
            selectedIndex: null,
            loading: false,
            isFocussed: false,
            error: null,
            selectedId: null,
            selectedDisplay: null,
            eventListener: false,
        }
    },
    computed: {
        showResults() {
            return Array.isArray(this.results) || this.hasError
        },
        noResults() {
            return Array.isArray(this.results) && this.results.length === 0
        },
        noResultMessage() {
            return (
                this.noResults &&
                !this.isLoading &&
                this.isFocussed &&
                !this.hasError &&
                this.showNoResults
            )
        },
        isEmpty() {
            return !this.display
        },
        isLoading() {
            return this.loading === true
        },
        hasError() {
            return this.error !== null
        },
        listStyle() {
            if (this.isLoading) {
                return {
                    color: '#ccc',
                }
            }
        },
    },
    methods: {
        /**
         * Search wrapper method
         */
        search() {
            this.selectedIndex = null
            switch (true) {
                case typeof this.source === 'string':
                    // No resource search with no input
                    if (!this.display || this.display.length < 1) {
                        return
                    }
                    return this.resourceSearch(this.source + this.display)
                case typeof this.source === 'function':
                    // No resource search with no input
                    if (!this.display || this.display.length < 1) {
                        return
                    }
                    return this.resourceSearch(this.source(this.display))
                case Array.isArray(this.source):
                    return this.arrayLikeSearch()
                default:
                    throw new TypeError()
            }
        },
        /**
         * Debounce the typed search query before making http requests
         * @param {String} url
         */
        resourceSearch: _.debounce(function (url) {
            if (!this.display) {
                this.results = []
                return
            }
            this.loading = true
            this.setEventListener()
            this.request(url)
        }, 200),
        /**
         * Make an http request for results
         * @param {String} url
         */
        request(url) {
            let promise = fetch(url, {
                method: this.method,
                credentials: this.getCredentials(),
                headers: this.getHeaders(),
            })
            return promise
                .then((response) => {
                    if (response.ok) {
                        this.error = null
                        return response.json()
                    }
                    throw new Error('Network response was not ok.')
                })
                .then((response) => {
                    this.results = this.setResults(response)
                    this.emitRequestResultEvent()
                    this.loading = false
                })
                .catch((error) => {
                    this.error = error.message
                    this.loading = false
                })
        },
        /**
         * Set some default headers and apply user supplied headers
         */
        getHeaders() {
            const headers = {
                Accept: 'application/json, text/plain, */*',
            }
            if (this.requestHeaders) {
                for (var prop in this.requestHeaders) {
                    headers[prop] = this.requestHeaders[prop]
                }
            }
            return new Headers(headers)
        },
        /**
         * Set default credentials and apply user supplied value
         */
        getCredentials() {
            let credentials = 'same-origin'
            if (this.credentials) {
                credentials = this.credentials
            }
            return credentials
        },
        /**
         * Set results property from api response
         * @param {Object|Array} response
         * @return {Array}
         */
        setResults(response) {
            if (this.resultsFormatter) {
                return this.resultsFormatter(response)
            }
            if (this.resultsProperty && response[this.resultsProperty]) {
                return response[this.resultsProperty]
            }
            if (Array.isArray(response)) {
                return response
            }
            return []
        },
        /**
         * Emit an event based on the request results
         */
        emitRequestResultEvent() {
            if (this.results.length === 0) {
                this.$emit('noResults', { query: this.display })
            } else {
                this.$emit('results', { results: this.results })
            }
        },
        /**
         * Search in results passed via an array
         */
        arrayLikeSearch() {
            this.setEventListener()
            if (!this.display) {
                this.results = this.source
                this.$emit('results', { results: this.results })
                this.loading = false
                return true
            }
            this.results = this.source.filter((item) => {
                return this.formatDisplay(item)
                    .toLowerCase()
                    .includes(this.display.toLowerCase())
            })
            this.$emit('results', { results: this.results })
            this.loading = false
        },
        /**
         * Select a result
         * @param {Object}
         */
        select(obj) {
            if (!obj) {
                return
            }
            this.value =
                this.resultsValue && obj[this.resultsValue]
                    ? obj[this.resultsValue]
                    : obj.id
            this.display = this.formatDisplay(obj)
            this.selectedDisplay = this.display
            this.$emit('selected', {
                value: this.value,
                display: this.display,
                selectedObject: obj,
            })
            this.$emit('input', this.value)
            this.close()
        },
        /**
         * @param  {Object} obj
         * @return {String}
         */
        formatDisplay(obj) {
            switch (typeof this.resultsDisplay) {
                case 'function':
                    return this.resultsDisplay(obj)
                case 'string':
                    if (!obj[this.resultsDisplay]) {
                        throw new Error(
                            `"${this.resultsDisplay}" property expected on result but is not defined.`
                        )
                    }
                    return obj[this.resultsDisplay]
                default:
                    throw new TypeError()
            }
        },
        /**
         * Register the component as focussed
         */
        focus() {
            this.isFocussed = true
        },
        /**
         * Remove the focussed value
         */
        blur() {
            this.isFocussed = false
        },
        /**
         * Is this item selected?
         * @param {Object}
         * @return {Boolean}
         */
        isSelected(key) {
            return key === this.selectedIndex
        },
        /**
         * Focus on the previous results item
         */
        up() {
            if (this.selectedIndex === null) {
                this.selectedIndex = this.results.length - 1
                return
            }
            this.selectedIndex =
                this.selectedIndex === 0
                    ? this.results.length - 1
                    : this.selectedIndex - 1
        },
        /**
         * Focus on the next results item
         */
        down() {
            if (this.selectedIndex === null) {
                this.selectedIndex = 0
                return
            }
            this.selectedIndex =
                this.selectedIndex === this.results.length - 1
                    ? 0
                    : this.selectedIndex + 1
        },
        /**
         * Select an item via the keyboard
         */
        enter() {
            if (this.selectedIndex === null) {
                this.$emit('nothingSelected', this.display)
                return
            }
            this.select(this.results[this.selectedIndex])
            this.$emit('enter', this.display)
        },
        /**
         * Clear all values, results and errors
         */
        clear() {
            this.display = null
            this.value = null
            this.results = null
            this.error = null
            this.$emit('clear')
        },
        /**
         * Close the results list. If nothing was selected clear the search
         */
        close() {
            if (!this.value || !this.selectedDisplay) {
                this.clear()
            }
            if (this.selectedDisplay !== this.display && this.value) {
                this.display = this.selectedDisplay
            }
            this.results = null
            this.error = null
            this.removeEventListener()
            this.$emit('close')
        },
        /**
         * Add event listener for clicks outside the results
         */
        setEventListener() {
            if (this.eventListener) {
                return false
            }
            this.eventListener = true
            document.addEventListener('click', this.clickOutsideListener, true)
            return true
        },
        /**
         * Remove the click event listener
         */
        removeEventListener() {
            this.eventListener = false
            document.removeEventListener(
                'click',
                this.clickOutsideListener,
                true
            )
        },
        /**
         * Method invoked by the event listener
         */
        clickOutsideListener(event) {
            if (this.$el && !this.$el.contains(event.target)) {
                this.close()
            }
        },
    },
    mounted() {
        this.value = this.initialValue
        this.display = this.initialDisplay
        this.selectedDisplay = this.initialDisplay
        console.debug('AUTOCOMPLETE Mounted', this.source)
    },
})

/*-****************
 *  Some helpers  *
 ******************/

const generateQueryString = (filters) => {
    return Object.keys(filters)
        .filter((key) => !!filters[key])
        .map(
            (key) =>
                `${encodeURIComponent(key)}=${encodeURIComponent(filters[key])}`
        )
        .join('&')
}

/****************
 * Map objects  *
 * **************/

const legendElement = (title, color, radius) => {
    const span = document.createElement('span')
    icon = document.createElement('i')
    icon.style.background = color
    icon.style.borderRadius = radius ? radius : null
    span.textContent = title
    span.prepend(icon)
    span.append(document.createElement('br'))
    return span
}

const showDisclaimer = () => {
    const div = document.getElementById('info legend')
    while (div.hasChildNodes()) {
        div.removeChild(div.lastChild)
    }
    const elements = [
        legendElement('Localisation simple', '#ff4c41'),
        legendElement('Localisation sur un métasite', '#8A2BE2'),
        legendElement('Emprise du métasite', '#9370DB', 0),
    ]
    div.append(...elements)
    return
}

const hideDisclaimer = () => {
    const div = document.getElementById('info legend')
    while (div.hasChildNodes()) {
        div.removeChild(div.lastChild)
    }
    const icon = document.createElement('i')
    icon.classList.add('fa', 'fa-info-circle')
    div.append(icon)
    return
}

const actionButtons = (label, style, icon, url) => {
    const a = document.createElement('a')
    const i = document.createElement('i')
    a.classList.add(`text-${style}`)
    i.classList.add('fa', 'fa-fw', icon)
    a.href = url
    a.target = '_blank'
    a.append(i)
    // a.type = 'button';
    a.title = label
    return a
}

const placeActionButtonGroup = (id, model, id_session = null) => {
    const btnGroup = document.createElement('div')
    btnGroup.classList.add('right')
    btnGroup.append(
        actionButtons(
            'Fiche détaillée',
            'primary',
            'fa-info-circle',
            `/${model}/${id}/detail`
        ),
        actionButtons('Editer', 'info', 'fa-edit', `/${model}/${id}/update`),
        actionButtons(
            'Ajouter une session',
            'success',
            'fa-calendar-alt',
            `/${model}/${id}/session/add`
        )
        // actionButtons(
        //     'Supprimer',
        //     'danger',
        //     'fa-trash',
        //     `/${model}/${id}/delete`
        // )
    )
    return btnGroup.outerHTML
}


// Sighting Form app

const countDetailBiomForm = [
    'sex',
    'age',
    'time',
    'device',
    'method',
    'manipulator',
    'validator',
    'transmitter',
    'ab',
    'd5',
    'd3',
    'pouce',
    'queue',
    'tibia',
    'pied',
    'cm3',
    'tragus',
    'poids',
    'testicule',
    'epididyme',
    'tuniq_vag',
    'gland_taille',
    'gland_coul',
    'mamelle',
    'gestation',
    'epiphyse',
    'chinspot',
    'usure_dent',
    'comment',
]

const countDetailAcousticForm = ['time', 'method', 'count', 'unit', 'comment']

const countDetailOtherForm = [
    'time',
    'method',
    'sex',
    'age',
    'count',
    'unit',
    'precision',
    'transmitter',
    'comment',
]

const countDetailTelemetryForm = [
    'time',
    'method',
    'sex',
    'age',
    'unit',
    'precision',
    'transmitter',
    'comment',
]

const CountDetailObject = () => {
    return {
        id_countdetail: null,
        method: null,
        sex: null,
        age: null,
        precision: null,
        count: null,
        unit: null,
        time: null,
        device: null,
        manipulator: null,
        validator: null,
        transmitter: null,
        ab: null,
        d5: null,
        pouce: null,
        queue: null,
        tibia: null,
        pied: null,
        cm3: null,
        tragus: null,
        poids: null,
        testicule: null,
        epididyme: null,
        tuniq_vag: null,
        gland_taille: null,
        gland_coul: null,
        mamelle: null,
        gestation: null,
        epiphyse: null,
        chinspot: null,
        usure_dent: null,
        comment: null,
    }
}

const SightingObject = (session_id) => {
    return {
        session: session_id,
        id_sighting: null,
        codesp: null,
        breed_colo: null,
        total_count: 0,
        observer: null,
        is_doubtful: false,
        comment: null,
        extra_data: null,
        countdetails: [],
        updated_by: null,
        created_by: null,
    }
}

Vue.component('headerPlaceSelect', {
    data() {
        return {
            options: [],
            loading: false,
        }
    },
    props: {
        value: {
            type: Number,
        },
        session: {
            type: Number,
        },
    },
    template: `<v-select class="form-control v-select-style" label="name" :reduce="e => e.id_place" :filterable="false" :options="options" @search="onSearch" v-model="model">
    <template slot="no-options">
      Tapez pour rechercher une localité
    </template>
    <template v-slot:option="option">
        <span class="label label-default">{{option.type?.code}}</span>&nbsp;{{option.name}}<br>
    <small class="text-muted">{{ option.areas.find(r => r.area_type.code == 'mun')? option.areas.find(r =>
        r.area_type.code ==
        'mun').name :
        '' }}&nbsp;({{ option.areas.find(r => r.area_type.code == 'mun')? option.areas.find(r =>
            r.area_type.code ==
            'mun').code :
            '' }})</small>
    </template>
    <template v-slot:selected-option="option">
    {{option.name}}
    </template>
  </v-select>`,
    computed: {
        model: {
            get() {
                return this.value
            },
            set(newValue) {
                this.$emit('input', newValue)
            },
        },
    },
    methods: {
        onSearch(search, loading) {
            if (search.length >= 2) {
                this.loading = true
                this.search(loading, search, this)
            }
        },
        onInit() {
            if (this.model) {
                this.apiCall(this.model)
            }
        },
        apiCall(id) {
            $http
                .get(`/api/v1/place/search?id_place=${parseInt(id)}`)
                .then((response) => {
                    this.options = response.data.results
                    this.loading = false
                })
                .catch(function (error) {
                    console.error(error)
                })
        },
        search: _debounce((loading, search, vm) => {
            params = { search: search }

            $http
                .get(`/api/v1/place/search`, { params })
                .then((response) => {
                    console.debug('response', response)
                    vm.options = response.data.results
                    console.debug('OPTIONS', vm.options)
                    loading(false)
                })
                .catch(function (error) {
                    console.error(error)
                })
        }, 350),

    },
    mounted() {
        this.onInit()
    },
})

if (!!document.getElementById('TopBarPlaceSearch')) {
    const TopBarPlaceSearch = new Vue({
        el: '#TopBarPlaceSearch',
        data: {
            id_place: null,
        },
        watch: {
            id_place: function (val, oldVal) {
                window.location = `/place/${val}/detail`
            },
        },
    })
}

Vue.component('areaSelect', {
    data() {
        return {
            options: [],
            loading: true,
            dicts: Object,
        }
    },
    props: {
        value: {
            type: Number,
        },
        session: {
            type: Number,
        },
        areatype: {
            type: Number,
        },
    },
    template: `
            <v-select id="Area" class="form-control v-select-style" label="name" :reduce="e => e.id" :filterable="false" :options="options" @search="onSearch" v-model="model" placeholder="Cherchez un zonage">
                <template slot="no-options">
                    Tapez pour rechercher un zonage
                </template>
                <template v-slot:option="option">
                    <span class="label label-default">{{option.area_type.code}}</span>&nbsp;<code>{{option.code}}</code><br>
                    {{ option.name }}
                </template>
                <template v-slot:selected-option="option">
                    {{ option.name }}
                </template>
            </v-select>
`,
    computed: {
        model: {
            get() {
                return this.value
            },
            set(newValue) {
                this.$emit('input', newValue)
            },
        },
    },
    watch: {
        areatype: {
            handler(val, oldval) {
                console.log('areasekectareatype', val, oldval)
            }
        }

    },

    methods: {
        getDicts() {
            this.dicts = JSON.parse(localStorage.getItem('dicts'))
        },
        onSearch(search, loading) {
            if (search.length >= 2) {
                this.loading = true
                this.search(loading, search, this)
            }
        },
        onInit() {
            if (this.model) {
                this.apiCall(this.model)
            }
            this.getDicts()
        },
        apiCall(id) {
            $http
                .get(`/api/v1/geodata/areas/list?id=${id}`)
                .then((response) => {
                    this.options = response.data.results
                    this.loading = false
                })
                .catch(function (error) {
                    console.error(error)
                })
        },
        search: _debounce((_loading, search, vm) => {
            params = { search }
            console.log('this.areatype',vm.areatype,)
            if (vm.areatype) {
                params.area_type=vm.areatype
            }
            $http
                .get('/api/v1/geodata/areas/list', {params})
                .then((response) => {
                    vm.options = response.data.results
                    vm.loading = false
                })
                .catch(function (error) {
                    console.error(error)
                })
        }, 350),
    },
    mounted() {
        this.onInit()
    },
})

Vue.component('metaplaceSelect', {
    data() {
        return {
            options: [],
        }
    },
    props: {
        value: {
            type: Number,
        },
        session: {
            type: Number,
        },
    },
    template: `<v-select class="form-control v-select-style" label="name" :reduce="e => e.id_metaplace" :filterable="false" :options="options" @search="onSearch" v-model="model" placeholder="Cherchez un métasite">
    <template slot="no-options">
      Tapez pour rechercher un métasite
    </template>
    <template v-slot:option="option">
        <span class="label label-default">{{option.type?.code || '-'}}</span>&nbsp;{{ option.name }}
    </template>
    <template v-slot:selected-option="option">
        <span class="label label-default">{{option.type?.code || '-'}}</span>&nbsp;{{ option.name }}
    </template>
  </v-select>`,
    computed: {
        model: {
            get() {
                return this.value
            },
            set(newValue) {
                this.$emit('input', newValue)
            },
        },
    },
    methods: {
        onSearch(search, loading) {
            if (search.length >= 2) {
                loading(true)
                this.search(loading, search, this)
            }
        },
        onInit() {
            if (this.model) {
                this.apiCall(this.model)
            }
        },
        apiCall(id) {
            $http
                .get(`/api/v1/metaplace?id_metaplace=${id_metaplace}`)
                .then((response) => {
                    this.options = response.results
                    loading(false)
                })
                .catch(function (error) {
                    console.error(error)
                })
        },
        search: _debounce((loading, search, vm) => {
            $http
                .get(`/api/v1/metaplace?search=${escape(search)}`)
                .then((response) => {
                    console.debug('response', response)
                    vm.options = response.data.results
                    console.debug('OPTIONS', vm.options)
                    loading(false)
                })
                .catch(function (error) {
                    console.error(error)
                })
        }, 350),
    },
    mounted() {
        this.onInit()
    },
})

Vue.component('transmitterSelect', {
    data() {
        return {
            options: [],
        }
    },
    props: {
        value: {
            type: Number,
        },
        session: {
            type: Number,
        },
    },
    template: `<v-select class="form-control v-select-style" label="reference" :reduce="e => e.id_transmitter" :filterable="false" :options="options" @search="onSearch" v-model="model">
    <template slot="no-options">
      Tapez pour rechercher un émetteur
    </template>
    <template v-slot:option="option">
    {{ option.reference }}
  </template>
  <template v-slot:selected-option="option">
    {{ option.reference }}
  </template>
  </v-select>`,
    computed: {
        model: {
            get() {
                return this.value
            },
            set(newValue) {
                this.$emit('input', newValue)
            },
        },
    },
    methods: {
        onSearch(search, loading) {
            if (search.length >= 2) {
                loading(true)
                this.search(loading, search, this)
            }
        },
        onInit() {
            if (this.model) {
                this.apiCall(this.model)
            }
        },
        apiCall(id = null) {
            $http
                .get(`/api/v1/transmitter/search?id=${id}`)
                .then((response) => {
                    this.options = response.data
                    loading(false)
                })
                .catch(function (error) {
                    console.error(error)
                })
        },
        search: _debounce((loading, search, vm) => {
            $http
                .get(`/api/v1/transmitter/search?available&q=${escape(search)}`)
                .then((response) => {
                    vm.options = response.data
                    loading(false)
                })
                .catch(function (error) {
                    console.error(error)
                })
        }, 350),
    },
    mounted() {
        this.onInit()
    },
})

Vue.component('studySelect', {
    data() {
        return {
            options: [],
        }
    },
    props: {
        value: {
            type: Number,
        },
        session: {
            type: Number,
        },
    },
    template: `<v-select class="form-control v-select-style" label="reference" :reduce="e => e.id_study" :filterable="false" :options="options" @search="onSearch" v-model="model">
    <template slot="no-options">
      Rechercher une étude
    </template>
    <template v-slot:option="option">
    {{ option.name }}
  </template>
  <template v-slot:selected-option="option">
    {{ option.name }}
  </template>
  </v-select>`,
    computed: {
        model: {
            get() {
                return this.value
            },
            set(newValue) {
                this.$emit('input', newValue)
            },
        },
    },
    methods: {
        onSearch(search, loading) {
            if (search.length >= 2) {
                loading(true)
                this.search(loading, search, this)
            }
        },
        onInit() {
            if (this.model) {
                this.apiCall(this.model)
            }
        },
        apiCall(id = null) {
            $http
                .get(`/api/v1/study/list?id=${id}`)
                .then((response) => {
                    this.options = response.data.results
                    loading(false)
                })
                .catch(function (error) {
                    console.error(error)
                })
        },
        search: _debounce((loading, search, vm) => {
            const params = { search: search }
            $http
                .get(`/api/v1/study/list`, { params })
                .then((response) => {
                    vm.options = response.data.results
                    loading(false)
                })
                .catch(function (error) {
                    console.error(error)
                })
        }, 350),
    },
    mounted() {
        this.onInit()
    },
})

Vue.component('specieSelect', {
    data() {
        return {
            selected: this.value,
            dict: [],
            options: [],
            disabled: true,
            helpText: '',
        }
    },
    props: {
        value: {
            type: Number,
        },
        showLockSwitch: {
            type: Boolean,
            default: false,
        },
    },
    template: `
    <span>
        <v-select
            :disabled="disabled" class="form-control v-select-style"
            :options="options" :reduce="e => e.id" label="codesp" v-model="model"
            data-toggle="tooltip" data-placement="right"
            :title="helpText">
            <template v-slot:option="option">
                <span class="label label-default" :class="formatCodesp(option.sp_true, option.codesp)">{{option.codesp}}</span> {{option.sci_name}}
            </template>
            <template #selected-option="{ sp_true, codesp, sci_name }">
            <span :class="{ 'text-success': sp_true}">{{sci_name}}</span>
            </template>
        </v-select>
        <i v-if="showLockSwitch" class="fas fa-edit" :class="disabled ? 'text-muted':'text-warning'" @click="editState()"></i>
    </span>
    `,
    computed: {
        model: {
            get() {
                console.debug('actual', this.selected, this.disabled)
                // // this.disabled = this.value != null
                if (!this.value) {
                    this.disabled = false
                }
                return this.value
            },
            set(newValue) {
                console.debug('test', newValue)
                this.$emit('input', newValue)
            },
        },
        selectedDetail: function () {
            return this.options.find((e) => e.id == this.model)
        },
    },
    methods: {
        formatCodesp(spTrue, codeSp) {
            if (codeSp != null && typeof codeSp === 'string') {
                return spTrue
                    ? 'label-success'
                    : codeSp.startsWith('0')
                        ? 'label-primary'
                        : 'label-default'
            }
        },
        editState() {
            console.debug('disable bef>', this.disabled)
            this.disabled = !this.disabled
            console.debug('disable aft>', this.disabled)
        },
        getSpecieDict() {
            this.edit = this.value == null
            if (localStorage.getItem('species') === null) {
                console.debug('No dict in localStorage')
                $http
                    .get('/api/v1/dict/specie')
                    .then((response) => {
                        console.debug(
                            'Saving downloaded dicts in localStorage',
                            response.data
                        )
                        this.options = response.data
                        localStorage.setItem(
                            'species',
                            JSON.stringify(this.options)
                        )
                    })
                    .catch((err) => {
                        console.error(err)
                    })
            } else {
                console.debug('loading species dict from localStorage')
                this.options = JSON.parse(localStorage.getItem('species'))
            }
            this.options = this.options
                .sort((a, b) => {
                    const ca = a.codesp,
                        cb = b.codesp
                    return ca < cb ? -1 : ca > cb ? 1 : 0
                })
                .sort((a, b) => Number(b.sp_true) - Number(a.sp_true))
        },
    },
    mounted() {
        this.getSpecieDict()
        // this.edit = this.value == null
        console.debug(
            'mounted edit',
            this.value,
            this.disabled,
            this.value != null
        )

        this.helpText = this.showLockSwitch
            ? "Cliquez sur l'icône d'édition pour modifier le taxon"
            : 'Choisissez un taxon'
        console.debug('Helptext', this.showLockSwitch, this.helpText)
    },
})

Vue.component('dictSelect', {
    data() {
        return {
            selected: this.value,
        }
    },
    props: {
        value: {
            type: Number,
        },
        options: {
            type: Array,
        },
        placeholder: {
            type: String,
        }
    },
    template: `<v-select class="form-control v-select-style" :options="options" :reduce="e => e.id" label="code" v-model="model" :placeholder="placeholder" >
        <template v-slot:option="option">
          <span data-toggle="tooltip" data-placement="left" :title="option.short_descr ? option.short_descr : option.name ? option.name: option.descr">
          <span class="label label-default">{{option.code}}</span>&nbsp;{{option.short_descr ? option.short_descr : option.name ? option.name: option.descr}}
          </span>
        </template>
        <template #selected-option="{ code, name, short_descr, descr }">
          {{short_descr ? short_descr : name ? name: descr}}
        </template>
      </v-select>`,
    computed: {
        model: {
            get() {
                return this.value
            },
            set(newValue) {
                this.$emit('input', newValue)
            },
        },
    },
})

Vue.component('userSelectOne', {
    data() {
        return {
            options: [],
        }
    },
    props: {
        value: {
            type: Number,
        },
        session: {
            type: Number,
        },
    },
    template: `<v-select class="form-control v-select-style" label="full_name" :reduce="e => e.id" :filterable="false" :options="options" @search="onSearch" v-model="model">
    <template slot="no-options">
      Tapez pour rechercher un utilisateur
    </template>
    <template v-slot:option="option">
    {{ option.full_name }}
  </template>
  <template v-slot:selected-option="option">
    {{ option.full_name }}
  </template>
  </v-select>`,
    computed: {
        model: {
            get() {
                return this.value
            },
            set(newValue) {
                this.$emit('input', newValue)
            },
        },
    },
    methods: {
        onSearch(search, loading) {
            if (search.length > 2) {
                loading(true)
                this.search(loading, search, this)
            }
        },
        onInit() {
            if (this.model) {
                this.apiCall(null, this.model)
            }
        },
        apiCall(q = null, id = null) {
            const qsParams = []
            let qs
            if (id) {
                qs = `id=${id}`
            } else {
                qsParams.push(`q=${escape(q)}`)
                if (session) {
                    qs.push(`session=${this.session}`)
                }
                qs = qsParams.join('&')
            }
            console.trace('User Search QS', qs)
            $http
                .get(`/accounts/api/v1/profile/search?${qs}`)
                .then((response) => {
                    this.options = response.data
                    loading(false)
                })
                .catch(function (error) {
                    console.error(error)
                })
        },
        search: _debounce((loading, search, vm) => {
            $http
                .get(`/accounts/api/v1/profile/search?q=${escape(search)}`)
                .then((response) => {
                    vm.options = response.data
                    console.debug('iotuibs', this.options)
                    loading(false)
                })
                .catch(function (error) {
                    console.error(error)
                })
        }, 350),
    },
    mounted() {
        this.onInit()
    },
})

const sigthingFormApp = (id_session, type_contact, editMode) => {
    VeeValidate.extend('positive', (value) => {
        return value >= 0
    })

    VeeValidate.extend('required', {
        validate(value) {
            return {
                required: true,
                valid: ['', null, undefined].indexOf(value) === -1,
            }
        },
        computesRequired: true,
    })

    VeeValidate.configure({
        classes: true,
        classNames: {
            valid: 'has-success',
            invalid: 'has-error',
            dirty: 'has-warning', // multiple classes per flag!
        },
    })

    return {
        el: '#sessionSightingEdit',
        delimiters: ['[[', ']]'],
        data() {
            return {
                data: [],
                dicts: {},
                form_fields:
                    type_contact == 'vm'
                        ? countDetailBiomForm
                        : type_contact == 'du'
                            ? countDetailAcousticForm
                            : countDetailOtherForm,
                url: sightingDataListUrl,
                loadingMessage: true,
                initialDataLoaded: false,
                postLoad: false,
                dictsLoaded: false,
                newSighting: SightingObject(id_session),
                editMode: editMode,
                newCountDetail: CountDetailObject(),
                hover: true,
                isVisible: false,
                showRawData: false,
                selectedItem: 0,
                typeContact: type_contact,
                moreSightingForm: false,
                moreCountDetailForm: false,
                messages: [],
                countDetailSelectedItem: -1,
                postInProgress: 0,
                sightingToDelete: -1,
            }
        },
        methods: {
            /**
             * Look for a specific item in dicts by code
             * @param {dict} dict
             * @param {str} code
             * @returns a dict item
             */
            findCondition(dict, code) {
                // Look for item in dicts
                return dict.find((e) => e.code === code)
            },
            /**
             * Display specific count detail form
             * @param {int} icd
             */
            displayCountDetailForm(icd) {
                this.countDetailSelectedItem = icd
            },
            /**
             * Select a taxa item to show
             * @param {*} i
             */
            selectItem(i) {
                this.selectedItem = i
                this.countDetailSelectedItem = -1
            },
            /**
             * Add a new specie
             */
            addNewSighting() {
                if (!this.data.find((e) => e.codesp == null)) {
                    const newSightingObject = JSON.parse(
                        JSON.stringify(this.newSighting)
                    )
                    const newCountDetailObject = { ...this.newCountDetail }
                    const allCountDetails = []
                    this.data.forEach((es) =>
                        allCountDetails.push(...es.countdetails)
                    )
                    const lastCountDetail =
                        allCountDetails[allCountDetails.length - 1]

                    if (lastCountDetail) {
                        newCountDetailObject.time = lastCountDetail.time
                        newCountDetailObject.method = lastCountDetail.method
                        newCountDetailObject.precision =
                            lastCountDetail.precision
                        newCountDetailObject.unit = lastCountDetail.unit
                    } else {
                        if (type_contact == 'vm') {
                            newCountDetailObject.count = 1
                            newCountDetailObject.precision = this.filteredDict(
                                this.dicts.CountPrecision
                            ).find((i) => i.code === 'precis')?.id
                            newCountDetailObject.unit = this.filteredDict(
                                this.dicts.CountUnit
                            ).find((i) => i.code === 'precis')?.id
                            console.debug('UNIT', this.filteredDict(
                                this.dicts.CountUnit, type_contact
                            ))
                        }
                    }

                    newSightingObject.countdetails.push(newCountDetailObject)
                    if (type_contact == 'du') {
                        newSightingObject.total_count = 1
                    }
                    this.data.push(newSightingObject)
                    this.selectedItem = this.data.length - 1
                    this.countDetailSelectedItem = 0
                } else {
                    alert(
                        'Vous ne pouvez pas entrez d autre taxon si tous ne sont pas complétés'
                    )
                }
            },
            /**
             * Add a new countdetail to specific sighting
             * @param {int} is is sighting array position
             */
            addNewCountDetail(is) {
                const newCountDetail = { ...this.newCountDetail }
                console.debug(
                    `<addNewCountDetail> ${this.data[is].countdetails.length} countdetails items`
                )
                if (this.data[is].countdetails.length > 0) {
                    console.debug('<addNewCountDetail> has countdetails items')
                    const lastCountDetail =
                        this.data[is].countdetails[
                        this.data[is].countdetails.length - 1
                        ]
                    // const lastCountDetail = this.data[is].countdetails.slice(-1)
                    console.debug(
                        'newObject before',
                        lastCountDetail,
                        newCountDetail
                    )
                    if (type_contact == 'vm') {
                        newCountDetail.count = 1
                    }
                    newCountDetail.time = lastCountDetail.time
                    newCountDetail.method = lastCountDetail.method
                    newCountDetail.precision =
                        newCountDetail.precision || lastCountDetail.precision
                    newCountDetail.unit = lastCountDetail.unit

                    console.debug('newObject after', newCountDetail)
                    console.debug('countdetails', this.data[is].countdetails)
                }
                this.data[is].countdetails.push(newCountDetail)
                this.countDetailSelectedItem =
                    this.data[is].countdetails.length - 1
            },
            /**
             * Delete a specific countdetail
             * @param {int} is is sighting array position
             * @param {int} icd is countdetail array position
             */
            delCountDetail(is, icd) {
                this.data[is].countdetails.splice(icd, 1)
            },
            /**
             * Get a specific specie value from dictSpecie
             * @param {*} isp is id specie
             * @param {*} field is field name
             * @returns field value
             */
            getSpecieValue(isp, field) {
                specieList = this.dicts.Specie
                return isp ? specieList.find((r) => r.id === isp)[field] : null
            },
            /**
             * Post a sighting item
             * @param {*} d is sighting data
             * @returns REST request
             */
            postItem(d) {
                console.debug('PostInProgress', this.postInProgress)
                console.debug('POST datas', d)
                const sightingUpdateUrl = d.id_sighting
                    ? `/api/v1/observation/${d.id_sighting}/update`
                    : `/api/v1/session/${id_session}/observation/add`
                const $httpMethod = d.id_sighting ? $http.put : $http.post

                return $httpMethod(sightingUpdateUrl, d)
                    .then((resp) => {
                        ++this.postInProgress
                        return resp
                    })
                    .catch((err) => {
                        console.error(err)
                        this.messages.push({
                            type: 'danger',
                            message: err.response.data,
                        })
                    })
            },
            /**
             *
             * @param {*} i
             */
            delSighting(i) {
                id = this.data[i].id_sighting
                if (id) {
                    $http
                        .delete(`/api/v1/observation/${id}/delete`)
                        .then((r) => {
                            console.debug('DELETE', r)
                            this.data.splice(i, 1)
                        })
                } else {
                    this.data.splice(i, 1)
                }
                this.sightingToDelete = -1
            },
            /**
             * Post all sightings data
             */
            postData() {
                const callback = () => {
                    this.getData()
                }
                this.messages.length = 0
                this.messages.push({
                    type: 'info',
                    message: `Saving data in progress`,
                })
                this.postInProgress = 0
                axios
                    .all(this.data.map((item) => this.postItem(item)))
                    .then(
                        axios.spread((...responses) => {
                            console.debug(responses)
                            responses.forEach((item, index) =>
                                console.debug('TEST', item, index)
                            )
                            if (
                                responses.every(
                                    (item) =>
                                        item?.status == 200 ||
                                        item?.status == 201
                                )
                            ) {
                                this.messages.length = 0
                                this.messages.push({
                                    type: 'success',
                                    message: 'Données enregistrées avec succès',
                                })

                                this.getData()
                            }
                            this.postInProgress = 0
                        })
                    )
                    .catch((err) => {
                        this.messages.length = 0
                        console.debug('ERROR', err)
                        this.messages.push({ type: 'danger', message: err })
                    })
            },
            /**
             * Filtered dict by contact
             * @param {*} dict
             * @param {*} contact
             * @returns
             */
            filteredDict(dict, contact) {
                contact = contact || null
                return contact ? dict.filter((e) => e.contact == contact) : dict
            },
            /**
             * Return html formatted selected value
             * @param {*} dict
             * @param {*} value
             * @returns
             */
            selectedValueHtml(dict, value) {
                // console.debug(
                //     'selectedValue',
                //     dict.find((e) => e.id == value)
                // )
                const r = dict.find((e) => e.id == value)
                return value
                    ? `<span class="label label-default">${r?.code}</span> ${r?.short_decr ? r.short_descr : r?.descr
                    }`
                    : '-'
            },
            selectedCode(dict, value, code) {
                return dict.find((e) => e.id == value)?.code == code
            },
            getDicts() {
                this.dicts = JSON.parse(localStorage.getItem('dicts'))
                this.dictSpecies = JSON.parse(
                    localStorage.getItem('dictSpecies')
                )
            },
            sumCountDetails(is) {
                const CountSum = (a, c) => a + parseInt(c.count)
                // const notNullData = this.data[is].countdetails.filter(e => e.count !== null)
                return this.data[is].countdetails
                    .filter((e) => e.count !== null)
                    .reduce(CountSum, 0)
            },
            getData() {
                if (Object.entries(this.dicts).length === 0) {
                    this.getDicts()
                }
                $http
                    .get(this.url)
                    .then((response) => {
                        this.data = response.data?.results
                        if (this.data.length == 0) {
                            this.addNewSighting()
                        }
                        console.debug('this.data', this.data)
                    })
                    .then(() => {
                        this.initialDataLoaded = true
                    })
                    .catch((err) => {
                        console.error(err)
                        this.messages.push({ type: 'error', message: err })
                    })
            },
            dupSpecies() {
                this.messages.length = 0
                const cleanArray = [...new Set(this.species)]
                let duplicates = [...this.species]
                cleanArray.forEach((item) => {
                    const i = duplicates.indexOf(item)
                    duplicates = duplicates
                        .slice(0, i)
                        .concat(duplicates.slice(i + 1, duplicates.length))
                })
                // console.debug('<dupSpecies>', duplicates) //[ 1, 5 ]
                if (duplicates.length > 0) {
                    this.messages.push({
                        type: 'danger',
                        message: `Espèce(s) ${duplicates.map(
                            (e) => `${this.getSpecieValue(e, 'sci_name')}`
                        )} en double`,
                    })
                }
            },
        },
        computed: {
            species() {
                return this.data.map((e) => e.codesp)
            },
        },
        watch: {
            data: {
                handler() {
                    if (type_contact != 'du') {
                        this.data.forEach((e, i) => {
                            if (
                                e.total_count != null &&
                                e.countdetails.length == 0
                            ) {
                            } else {
                                e.total_count = this.sumCountDetails(i)
                            }
                        })
                    }
                    this.dupSpecies()
                },
                deep: true,
            },
        },
        mounted() {
            this.getDicts()
            this.getData()
            this.messages.push()
        },
        // options
    }
}

Vue.component('session-group-item', {
    props: ['data'],
    template: `<div class="list-group-item">
    <h3 class="list-group-item-heading">
        <span class="pull-right label label-info label-lg">{{data.properties.sessions.length}} sessions</span>
        <i class="fa fa-map-marker fa-fw"></i><a :href="'/place/'+data.id+'/detail'" target="_blank">{{data.properties.name}}</a>
    </h3>
    <div class="list-group">
      <div v-for="(se, ie) in data.properties.sessions" class="list-group-item">
        <h4 class="list-group-item-heading">
          <span class="pull-right label label-default label-lg">{{se.contact.descr}}</span>
          <span class="text-info"><a :href="'/session/'+se.id_session+'/detail'">{{se.date_start}}</a></span>
        </h4>
        <p class="list-group-item-text">
          <dl class="dl-horizontal">
            <dt>Observateur principal</dt>
            <dd>
              {{se.main_observer?.full_name}}</dd>
            <dt>Observations</dt>
            <dd>
              <ul class="list-unstyled">
                <li v-for='(o, io) in se.observations' :class="{'text-muted':!o.codesp.sp_true}">
                <small><span v-show="o.breed_colo"  class="fa-stack">
                <i class="fas fa-circle fa-stack-2x text-success"></i>
                <i class="fas fa-baby fa-stack-1x fa-inverse"></i>
              </span></small>

                <!--<i v-show="o.breed_colo" class="fas fa-baby fa-fw text-success"></i>-->
                  <strong>{{o.codesp.common_name_fr}}</strong>
                  (<i>{{o.codesp.sci_name}}</i>)
                  : {{o.total_count ? o.total_count: '-'}}

                </li>
              </ul>
            </dd>
          </dl>
        </p>
      </div>
    </div>
  </div>`,
})

Vue.component('pie-chart', {
    extends: VueChartJs.Pie,
    props: ['chartData', 'options'],
    mounted() {
        this.renderChart(this.chartData, this.options)
    },
})

const studyDataApp = (pk) => {
    // Vue.component('pie-chart', {
    //     extends: Pie,
    //     data() {
    //         return {
    //             options: {
    //                 parsing: {
    //                     xAxisKey: 'label',
    //                     yAxisKey: 'value',
    //                 },
    //             },
    //         }
    //     },
    //     props: {
    //         chartData: {
    //             type: Object,
    //             default: null,
    //         },
    //     },
    //     mounted() {
    //         this.renderChart(this.chartData, this.options)
    //     },
    // })

    return {
        el: '#StudyData',
        delimiters: ['[[', ']]'],
        data() {
            return {
                pk: pk,
                msg: 'Test',
                placeData: [],
                sessionData: [],
                sightingData: [],

                loading: true,
                contactChartDict: {
                    vv: { label: 'Contact visuel', color: '#e89bda' },
                    du: { label: 'Contact acoustique', color: '#7ed6a0' },
                    vm: { label: 'Vu en main', color: '#c6c63d' },
                    te: { label: 'Contact télémétrique', color: '#517eef' },
                    nc: { label: 'Non communiqué', color: '#aaaaaa' },
                },
                contactChartOptions: {
                    options: {
                        parsing: {
                            xAxisKey: 'label',
                            yAxisKey: 'value',
                        },
                    },
                },
                dicts: {},
                dictSpecies: {},
            }
        },
        methods: {
            getSightings() {
                $http
                    .get(`/api/v1/study/${pk}/datas?format=json`)
                    .then((r) => {
                        console.debug(r)
                        this.sightingData = r.data.results.features
                        //this.countSessions()
                        this.loading = false
                    })
                    .catch((error) => console.error(error))
            },
            getDicts() {
                this.dicts = JSON.parse(localStorage.getItem('dicts'))
                this.dictSpecies = JSON.parse(
                    localStorage.getItem('dictSpecies')
                )
            },
            countPlaces() {
                return this.sightingData.length
            },
            // countSessions2() {
            //     this.sessions = []
            //     console.error('TypeOf Fata', typeof(this.sightingData));
            //     console.error('Data', this.sightingData);
            //     this.sessions = [...this.sightingData.forEach(e => this.sessions.push(...e.properties.sessions))]
            // }
        },
        computed: {
            sessions() {
                return Array.prototype.concat.apply(
                    [],
                    this.sightingData.map((e) => e.properties.sessions)
                )
            },
            minDate() {
                //return this.sessions.map(a=> e.date_start).reduce((a, b) => { return a < b ? a : b; })
                return new Date(
                    Math.min(
                        ...this.sessions.map((e) => new Date(e.date_start))
                    )
                ).toLocaleDateString()
            },
            maxDate() {
                return new Date(
                    Math.max(
                        ...this.sessions.map(
                            (e) =>
                                new Date(e.date_end ? e.date_end : e.date_start)
                        )
                    )
                ).toLocaleDateString()
            },
            contacts() {
                const contacts = [
                    ...new Set(this.sessions.map((e) => e.contact.code)),
                ]
                console.debug('contacts', contacts)
                const data = []
                contacts.forEach((e) => {
                    data.push({
                        code: e,
                        value: this.sessions.filter(
                            (se) => se.contact.code == e
                        ).length,
                    })
                })
                return data
            },
            taxa() {
                const sightings = this.sessions
                    .map((e) => e.observations)
                    .flat()
                console.debug(sightings)
                const listTaxa = [
                    ...new Set(sightings.map((e) => e.codesp.codesp)),
                ]
                const taxa = []
                listTaxa.forEach((e) => {
                    taxa.push({
                        code: e,
                        common_name: this.dictSpecies.find(
                            (sp) => sp.codesp == e
                        ).common_name_fr,
                        sci_name: this.dictSpecies.find((sp) => sp.codesp == e)
                            .sci_name,
                        sp_true: this.dictSpecies.find((sp) => sp.codesp == e)
                            .sp_true,
                        value: sightings.filter(
                            (se) => se.codesp.codesp == e && se.total_count > 0
                        ).length,
                    })
                })
                return taxa.sort((a, b) => b.value - a.value)
            },
            main_observers() {
                const main_observers = this.sessions
                    .map((e) => e.main_observer)
                    .flat()
                return [...new Set(main_observers.map((e) => e.full_name))]
            },
            chartData() {
                return {
                    labels: [
                        ...this.contacts.map(
                            (e) => this.contactChartDict[e.code].label
                        ),
                    ],
                    datasets: [
                        {
                            label: 'ds1',
                            backgroundColor: [
                                ...this.contacts.map(
                                    (e) =>
                                        this.contactChartDict[e.code].color ||
                                        '#ef517b'
                                ),
                            ],
                            data: [...this.contacts.map((e) => e.value)],
                        },
                    ],
                }
            },
        },
        mounted() {
            this.getSightings(), this.getDicts()
        },
    }
}

const strNormalize = (str) => {
    return str
        .toLowerCase()
        .normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '')
}

const studyListApp = (userId) => {
    return {
        el: '#studyList',
        delimiters: ['[[', ']]'],
        data() {
            return {
                loading: true,
                fullStudyList: [],
                studyList: [],
                search: {},
            }
        },
        methods: {
            getStudies() {
                $http
                    .get(`/api/v1/study/list?format=json`)
                    .then((r) => {
                        console.debug(r)
                        this.fullStudyList = r.data.results
                        this.studyList = [...this.fullStudyList]
                        this.loading = false
                    })
                    .catch((error) => console.error(error))
            },
            filterStudies() {
                let filteredStudies = [...this.fullStudyList]
                if (this.search.name) {
                    filteredStudies = filteredStudies.filter(
                        (e) =>
                            e.name &&
                            strNormalize(e.name).includes(
                                strNormalize(this.search.name)
                            )
                    )
                }
                if (this.search.year) {
                    filteredStudies = filteredStudies.filter(
                        (e) => e.year && e.year.includes(this.search.year)
                    )
                }
                if (this.search.myStudies) {
                    filteredStudies = filteredStudies.filter(
                        (e) =>
                            e.project_manager && e.project_manager?.id == userId
                    )
                }
                if (this.search.manager) {
                    filteredStudies = filteredStudies.filter(
                        (e) =>
                            e.project_manager &&
                            strNormalize(e.project_manager?.full_name).includes(
                                strNormalize(this.search.manager)
                            )
                    )
                }
                if (this.search.type_espace) {
                    filteredStudies = filteredStudies.filter(
                        (e) =>
                            e.type_espace &&
                            strNormalize(e.type_espace).includes(
                                strNormalize(this.search.type_espace)
                            )
                    )
                }
                if (this.search.type_etude) {
                    filteredStudies = filteredStudies.filter(
                        (e) =>
                            e.type_etude &&
                            strNormalize(e.type_etude).includes(
                                strNormalize(this.search.type_etude)
                            )
                    )
                }
                if (this.search.public_funding) {
                    filteredStudies = filteredStudies.filter(
                        (e) => e.public_funding
                    )
                }
                if (this.search.public_report) {
                    filteredStudies = filteredStudies.filter(
                        (e) => e.public_report
                    )
                }
                if (this.search.public_raw_data) {
                    filteredStudies = filteredStudies.filter(
                        (e) => e.public_raw_data
                    )
                }
                if (this.search.confidential) {
                    filteredStudies = filteredStudies.filter(
                        (e) => e.confidential
                    )
                }
                this.studyList = filteredStudies
            },
        },
        watch: {
            search: {
                handler(val, oldVal) {
                    console.debug('book list changed')
                    this.filterStudies()
                },
                deep: true,
            },

            //(search, oldSearch) {
            //    console.debug(search)
            //    this.filterStudies()
            //}
        },
        mounted() {
            this.getStudies()
        },
    }
}

Vue.component('switch-active-status', {
    data() {
        return {
            error: null,
        }
    },
    props: ['data'],
    template: `<transition name="modal">
    <div class="modal" style="display: block">
     <div class="modal-dialog" role="document">
       <div class="modal-content">
         <div class="modal-header">
            <a class="pull-right text-danger" @click="$emit('close')"><i class="fas fa-times-circle"></i></a>
           <h4 class="modal-title">
             {{data.is_active ? 'Désactiver' : 'Activer' }} l'observateur {{data.full_name}}
           </h4>
         </div>
         <div class="modal-body">
         <div v-if="error" class="alert alert-danger" role="alert">{{error}}</div>
            <p>Etes vous certain de vouloir {{data.is_active ? 'désactiver' : 'activer' }} le compte observateur {{data.full_name}}?</p>
            <button class="btn btn-warning btn-block" @click="revertActiveStatus(data.id)">{{data.is_active ? 'Désactiver' : 'Activer' }}</button>
         </div>
       </div>
     </div>
   </div>
   </transition> `,
    methods: {
        revertActiveStatus(id) {
            $http
                .patch(`/accounts/api/v1/profile/${id}/switch-active-status`)
                .then((res) => {
                    console.debug('<revertActiveStatus> response', res)
                    this.$emit('close')
                })
                .catch((error) => {
                    this.error = error
                    console.debug(error)
                })
        },
    },
})

const UserListApp = (userId, rootApiUrl) => {
    return {
        el: '#UserList',
        delimiters: ['[[', ']]'],
        data() {
            return {
                loading: true,
                filters: {},
                data: {},
                fullusersList: {},
                usersList: [],
                qs: null,
                userId: userId,
                pagination_url: null,
                switchUserStatusModal: false,
                itemData: null,
            }
        },
        methods: {
            openSwitchUserStatusModal: function (data) {
                this.itemData = data
                this.switchUserStatusModal = true
            },
            closeSwitchUserStatusModal: function () {
                this.switchUserStatusModal = false
            },
            getQsValue(url, key) {
                const urlObj = new URL(url)
                const urlParams = new URLSearchParams(urlObj.search)
                console.debug('urlParams for', url, urlParams)
                return urlParams.get(key)
            },
            getUsers() {
                // if(this.pagination_url && this.getQsValue(this.pagination_url,'page') != null){
                //     this.qs = `${this.qs}?page=${this.getQsValue(this.pagination_url,'page')}`
                // }
                const queryUrl = this.pagination_url
                    ? this.pagination_url
                    : this.qs
                        ? `${rootApiUrl}?${this.qs}`
                        : rootApiUrl
                // console.debug('TEST', this.getQsValue(this.pagination_url, 'page'))
                console.debug('queryUrl', queryUrl)
                $http
                    .get(queryUrl)
                    .then((r) => {
                        console.debug(r)
                        this.data = r.data
                        this.fullUsersList = r.data.results
                        this.usersList = [...this.fullUsersList]
                        this.loading = false
                    })
                    .catch((error) => console.error(error))
            },
            search() {
                this.pagination_url = null
                this.getUsers()
            },
        },
        watch: {
            filters: {
                handler(val, oldVal) {
                    console.debug(val, oldVal)
                    this.qs = generateQueryString(val)
                },
                deep: true,
            },
            pagination_url: {
                handler(val, oldVal) {
                    console.debug(val, oldVal)
                    this.getUsers()
                },
            },
            switchUserStatusModal: {
                handler(val, oldVal) {
                    console.debug(val, oldVal)
                    this.getUsers()
                },
            },
            //(search, oldSearch) {
            //    console.debug(search)
            //    this.filterStudies()
            //}
        },
        mounted() {
            this.getUsers()
        },
    }
}

const placeMarkerOptions = () => {
    return {
        pointToLayer: function (feature, latlng) {
            return L.circleMarker(latlng, placeMarkerStyle)
        },
        onEachFeature: function onEachFeature(feature, layer) {
            const props = feature.properties
            const content = `
            <h4><a href="/place/${feature.id
                }/detail" target="_blank" title="Fiche détaillée">${props.name
                }</a></h4>
            <span class="label label-${props.type ? 'success' : 'default'}">${props.type ? props.type.code : 'nd'
                }</span> ${props.type ? props.type.descr : 'Type non défini'}

            <span class="text-right">
                ${placeActionButtonGroup(feature.id, 'place')}
                </span>
            `
            layer.bindPopup(content, mapPopupOptions)
            // if (feature.id) {
            //     markersById[feature.id] = layer
            // }
        },
    }
}

const metaplaceMarkerOptions = () => {
    return {
        pointToLayer: function (feature, latlng) {
            return L.circleMarker(latlng, placeMarkerStyle)
        },
        onEachFeature: function onEachFeature(feature, layer) {
            const props = feature.properties
            const content = `
            <h4><a href="/place/${feature.id
                }/detail" target="_blank" title="Fiche détaillée">${props.name
                }</a></h4>
            <span class="label label-${props.type ? 'success' : 'default'}">${props.type ? props.type.code : 'nd'
                }</span> ${props.type ? props.type.descr : 'Type non défini'}

            <span class="text-right">
                ${placeActionButtonGroup(feature.id, 'metaplace')}
                </span>
            `
            layer.bindPopup(content, mapPopupOptions)
        },
    }
}

const SpecieDistribution = (rootApiUrl) => {
    return {
        el: '#SpecieDistribution',
        delimiters: ['[[', ']]'],
        data() {
            return {
                filters: { codesp: null, type: null, period: null },
                data: {},
                qs: '',
                dicts: {},
                featuresLayer: null,
                center: [45, 5],
                mapTheme: {
                    count_observations: {
                        title: "Nombre d'observations",
                        style: this.setCountDataColor,
                    },
                    count_taxa: {
                        title: "Nombre d'espèces",
                        style: this.setCountTaxaColor,
                    },
                    last_data: {
                        title: 'Dernière observation',
                        style: this.setYearColor,
                    },
                    gite: {
                        title: 'Gîtes',
                        style: this.setGite,
                    },
                    breed_colo: {
                        title: 'Colonie de reproduction',
                        style: this.setBreedColoColor,
                    },
                },
                mapPeriod: {
                    all: {
                        title: "Toute période",
                        value: null,
                    },
                    wintering: {
                        title: "Hivernant",
                        value: "Hivernant",
                    },
                    spring: {
                        title: "Transit printanier",
                        value: "Transit printanier",
                    },
                    summer: {
                        title: 'Estivage',
                        value: "Estivage",
                    },
                    fall: {
                        title: 'Transit automnal',
                        value: "Transit automnal",
                    },
                },
                mapStyle: {
                    title: 'Dernière observation',
                    style: this.setYearColor,
                },
            }
        },
        methods: {
            getData() {
                // const queryUrl = `${rootApiUrl}?${this.qs}`
                const queryUrl = `${rootApiUrl}`
                const params = this.filters
                console.debug('queryUrl', queryUrl)
                $http
                    .get(queryUrl, { params })
                    .then((r) => {
                        console.debug(r)
                        this.data = r.data
                    })
                    .catch((error) => console.error(error))
            },
            getDicts() {
                this.dicts = JSON.parse(localStorage.getItem('dicts'))
                console.debug(this.dicts)
            },
            setBreedColoColor(feature) {
                return feature.properties.breed_colo ? '#FF2C2C' : '#2C8DFF'
            },
            setGite(feature) {
                return feature.properties.has_gite ? '#1A5D1A' : '#2C8DFF'
            },
            setYearColor(feature) {
                const todayYear = new Date().getFullYear()
                const lastData = feature.properties.last_data
                return lastData == todayYear
                    ? '#FC4E2A'
                    : todayYear - lastData < 2
                        ? '#FD8D3C'
                        : todayYear - lastData < 5
                            ? '#FEB24C'
                            : todayYear - lastData < 10
                                ? '#FED976'
                                : '#FFEDA0'
            },
            setCountDataColor(feature) {
                const countObs = feature.properties.count_observations
                return countObs > 100
                    ? '#FC4E2A'
                    : countObs > 50
                        ? '#FD8D3C'
                        : countObs > 20
                            ? '#FEB24C'
                            : countObs > 10
                                ? '#FED976'
                                : '#FFEDA0'
            },
            setCountTaxaColor(feature) {
                const countTaxa = feature.properties.count_taxa
                return countTaxa > 20
                    ? '#FC4E2A'
                    : countTaxa > 15
                        ? '#FD8D3C'
                        : countTaxa > 10
                            ? '#FEB24C'
                            : countTaxa > 5
                                ? '#FED976'
                                : '#FFEDA0'
            },
            setMapStyle(feature) {
                return {
                    fillColor: this.mapStyle.style(feature),
                    weight: 1,
                    opacity: 1,
                    color: 'white',
                    dashArray: '1',
                    fillOpacity: 0.7,
                }
            },
            selectedLayer(f) {
                return {
                    fillColor: rgba(0, 0, 0, 0),
                    weight: 1,
                    opacity: 1,
                    color: 'white',
                    dashArray: '1',
                    fillOpacity: 0.7,
                }
            },
            countTaxaColorStyle(f) {
                return {
                    fillColor: this.setCountDataColor(f.properties.count_taxa),
                    weight: 1,
                    opacity: 1,
                    color: 'white',
                    dashArray: '1',
                    fillOpacity: 0.7,
                }
            },
            setupLeafletMap() {
                this.map = initMap('mapContainer', false)
                this.selectedLayer = L.featureGroup().addTo(this.map)
                this.featuresLayer = L.featureGroup().addTo(this.map)
            },
        },
        watch: {
            filters: {
                handler(val) {
                    if (!!val.type) {
                        console.debug('filters', val)
                        this.qs = generateQueryString(val)
                        this.getData()
                    }
                },
                deep: true,
            },
            mapStyle: {
                handler(val, oldVal) {
                    console.debug(val)
                    this.featuresLayer.setStyle(this.setMapStyle)
                },
                deep: true,
            },
            data: {
                handler(val, oldVal) {
                    this.featuresLayer.clearLayers()
                    console.debug('mapStyle', this.mapStyle.style)
                    const data = L.geoJSON(val, {
                        style: this.setMapStyle,
                        onEachFeature: function onEachFeature(feature, layer) {
                            const p = feature.properties
                            const content = `
                                <h4><a href="/synthesis/area/${feature.id
                                }" target="_blank" title="Fiche détaillée">${p.name
                                }</a></h4>
                                <span class="label label-default">${p.area_type.code
                                }</span>&nbsp;<small class="text-muted">${p.code
                                }</small>
                                <strong>Données</strong>
                                <dl class="dl-horizontal">
                                    <dt>Observations</dt>
                                    <dd>${p.count_observations}</dd>
                                    <dt>Espèces</dt>
                                    <dd>${p.count_taxa}</dd>
                                    <dt>Sessions</dt>
                                    <dd>${p.count_sessions}</dd>
                                    <dt>Localités</dt>
                                    <dd>${p.count_places}</dd>
                                    <dt>Dernière donnée</dt>
                                    <dd>${p.last_data}</dd>
                                    <dt>Colonie de reproduction</dt>
                                    <dd>${p.breed_colo ? 'Oui' : 'Non'}</dd>
                                </dl>

                                `
                            layer.bindPopup(content, mapPopupOptions)
                        },
                    })
                    if (val.features.length > 0) {
                        data.addTo(this.featuresLayer)
                        this.map.fitBounds(this.featuresLayer.getBounds())
                    }
                },
            },
        },
        computed: {
            mapThemeList() {
                return Object.keys(this.mapTheme)
            },
            mapPeriodList() {
                return Object.keys(this.mapPeriod)
            },
        },
        mounted() {
            this.getDicts()
            this.setupLeafletMap()
            // this.getData()
        },
    }
}

Vue.component('placeMap', {
    data() {
        return {
            filters: { no_page: true, in_bbox: null },
            placeData: {},
            metaplaceData: {},
            featuresLayer: null,
            center: [45, 5],
            placeLayer: L.featureGroup(),
            metaplaceLayer: L.featureGroup(),
            featureLayer: L.featureGroup(),
            zoomMessage: true,
        }
    },
    props: {
        feature: {
            type: Object,
        },
    },
    template: `
    <div id="mapContainer">
    <div v-if="zoomMessage" id="zoomMessage" class="text-center text-warning" role="alert">
      <i class="fa fa-info-circle fa-fw"></i><strong>Zoomez pour afficher les données</strong>
    </div>
  </div>
    `,

    methods: {
        getDatas(sourceUrl, layer, style) {
            const queryUrl = `${sourceUrl}${this.qs ? '?' + this.qs : ''}`
            let data = {}
            console.debug('queryUrl', queryUrl)
            $http
                .get(queryUrl)
                .then((r) => {
                    console.debug('R', r)
                    data = r.data
                    console.debug('DATA', data)
                    layer.clearLayers()
                    const mapData = L.geoJSON(data, style)
                    console.debug('add Data to map', data)
                    layer.addLayer(mapData)
                })
                .catch((error) => console.error(error))
            return data
        },
        setupLeafletMap() {
            this.map = initMap('mapContainer')
            this.placeLayer = L.featureGroup().addTo(this.map)
            this.metaplaceLayer = L.featureGroup().addTo(this.map).bringToBack()
            this.featureLayer = L.featureGroup().addTo(this.map)
            let debounce = null
            this.map.on('moveend', () => {
                this.filters.in_bbox = this.map.getBounds().toBBoxString()
                if (this.map.getZoom() >= 13) {
                    this.zoomMessage = false
                    clearTimeout(debounce)
                    debounce = setTimeout(this.displayData, 500)
                } else {
                    this.placeLayer.clearLayers()
                    this.metaplaceLayer.clearLayers()
                    this.zoomMessage = true
                }
            })
            this.map.addControl(
                new L.Control.Search({
                    url: 'https://nominatim.openstreetmap.org/search?format=json&accept-language=fr-FR&q={s}',
                    jsonpParam: 'json_callback',
                    propertyName: 'display_name',
                    propertyLoc: ['lat', 'lon'],
                    markerLocation: true,
                    autoType: true,
                    autoCollapse: true,
                    minLength: 2,
                    zoom: 13,
                    text: 'Recherche...',
                    textCancel: 'Annuler',
                    textErr: 'Erreur',
                })
            )
        },
        displayData() {
            var style = {
                radius: 5,
                fillColor: '#ff7800',
                color: '#000',
                weight: 1,
                opacity: 1,
                fillOpacity: 0.8,
            }
            console.debug('displayData')
            this.getDatas(
                metaplaceBaseUrl,
                this.metaplaceLayer,
                metaplaceMarkerOptions()
            )
            this.getDatas(placeBaseUrl, this.placeLayer, placeMarkerOptions())
        },
    },
    watch: {
        filters: {
            handler(val) {
                if (!!val.type) {
                    console.debug('filters', val)
                    this.qs = generateQueryString(val)
                    this.getPlaceData()
                }
            },
            deep: true,
        },
        feature: {
            handler(val, oldVal) {
                const geojsonMarkerOptions = {
                    radius: 12,
                    fillColor: 'rgba(0,0,0,0)',
                    color: '#ff7800',
                    weight: 2,
                    opacity: 1,
                    fillOpacity: 0,
                }
                this.featureLayer.clearLayers()
                const data = L.geoJSON(val, {
                    style: {
                        color: '#8A2BE2',
                        fillColor: '#9370DB',
                        weight: 2,
                        opacity: 0.8,
                        fillOpacity: 0.2,
                    },
                    pointToLayer: function (feature, latlng) {
                        return L.circleMarker(latlng, geojsonMarkerOptions)
                    },
                })
                data.addTo(this.featureLayer)
                this.featureLayer.bringToBack()
                this.map.fitBounds(this.featureLayer.getBounds()).setZoom(13)
            },
        },
    },
    computed: {
        qs() {
            return generateQueryString(this.filters)
        },
    },
    mounted() {
        this.setupLeafletMap()
        // this.getData()
    },
})

const PlaceMap = () => {
    return {
        el: '#PlaceMap',
    }
}

const ModelSearch = (
    rootApiUrl,
    userId,
    prefilledFilters,
    dataElementId = 'dataTable',
    dataType = 'geojson'
) => {
    console.debug('prefilledFilters', prefilledFilters)
    return {
        el: '#ModelSearch',
        delimiters: ['[[', ']]'],
        data() {
            return {
                loading: true,
                advancedSearch: false,
                limit_message: true,
                filters: prefilledFilters,
                data: {},
                dataList: [],
                center: [45, 5],
                qs: null,
                userId: userId,
                pagination_url: null,
                page: 1,
                itemData: null,
                map: null,
                feature: null,
                periods: periods,
                dateInputLimit: null,
                dicts: Object,
                exportUrl: countdetailExportUrl,
                fullExportUrl: countdetailExportUrl,
                dataExport: {},
                nextPage: null,
                area_type: null,
            }
        },
        methods: {
            setDateInputLimit() {
                function addLeadingZeros(n) {
                    if (n <= 9) {
                        return '0' + n
                    }
                    return n
                }
                const tomorrow = new Date()
                this.dateInputLimit =
                    tomorrow.getFullYear() +
                    '-' +
                    addLeadingZeros(tomorrow.getMonth() + 1) +
                    '-' +
                    addLeadingZeros(tomorrow.getDate())
                console.debug('this.dateInputLimit 123', this.dateInputLimit)
                // console.debug('this.fullExportUrl', this.fullExportUrl)
            },
            getDicts() {
                this.dicts = JSON.parse(localStorage.getItem('dicts'))
                console.debug(this.dicts)
            },
            getQsValue(url, key) {
                const urlObj = new URL(url)
                const urlParams = new URLSearchParams(urlObj.search)
                console.debug('urlParams for', url, urlParams)
                return urlParams.get(key)
            },
            launchExport() {
                this.dataExport = {}
                $http
                    .post(this.fullExportUrl)
                    .then((response) => (this.dataExport = response.data))
                    .catch((e) => console.error(e))
            },
            getData() {
                this.loading = true
                const queryUrl = this.pagination_url
                    ? this.pagination_url
                    : this.qs
                        ? `${rootApiUrl}?geojson=1&page_size=30&${this.qs}`
                        : `${rootApiUrl}?geojson=1&page_size=30`
                console.debug('queryUrl', queryUrl)
                $http
                    .get(queryUrl)
                    .then((r) => {
                        console.debug('DATA', r, r.data.results)
                        this.data = r.data
                        this.dataList = r.data.results
                        console.debug('dataList', this.dataList)
                        this.nextPage = r.data.next
                        console.debug('nextPage', this.nextPage)
                        this.loading = false
                    })
                    .catch((error) => console.error(error))
            },
            search() {
                this.pagination_url = null
                this.page = 1
                this.getData()
            },
            getNextData() {
                const dataElement = document.getElementById(dataElementId)
                if (dataElement) {
                    dataElement.onscroll = _debounce(() => {
                        let bottomOfElement =
                            dataElement.offsetHeight + dataElement.scrollTop >
                            dataElement.scrollHeight * 0.95
                        if (bottomOfElement && this.nextPage) {
                            this.loading = true
                            const url = this.qs
                                ? `${rootApiUrl}?geojson=1&${this.qs}&page_size=30&page=${this.page + 1
                                }`
                                : `${rootApiUrl}?geojson=1&page_size=30&page=${this.page + 1
                                }`
                            $http.get(url).then((r) => {
                                this.nextPage = r.data.next
                                console.debug('nextPage update', this.nextPage)
                                if (dataType === 'geojson') {
                                    this.dataList.features.push(
                                        ...r.data.results.features
                                    )
                                } else {
                                    this.dataList.push(...r.data.results)
                                }
                                this.loading = false
                            })
                            this.page += 1
                        }
                    }, 350)
                }
            },
        },
        watch: {
            area_type: {
                handler(val, oldVal) {
                    console.log('area_type', val, oldVal)
                }
            },
            filters: {
                handler(val, oldVal) {
                    console.debug('filters', val, oldVal)
                    this.qs = generateQueryString(val)
                    this.fullExportUrl = this.qs
                        ? `${this.exportUrl}?${this.qs}`
                        : this.exportUrl
                    console.debug('newUrl', this.fullExportUrl)
                },
                deep: true,
            },
            pagination_url: {
                handler(val, oldVal) {
                    console.debug(val, oldVal)
                    this.getData()
                },
            },
        },
        mounted() {
            console.debug('filter', this.filters)
            this.qs = generateQueryString(this.filters)
            this.fullExportUrl = this.qs
                ? `${this.exportUrl}?${this.qs}`
                : this.exportUrl
            this.getDicts()
            this.getData()
            this.getNextData()
            this.setDateInputLimit()
        },
    }
}

const metaplaceDetail = (placesUrl, id_metaplace) => {
    const map = initMap('mapContainer')
    const markers = L.markerClusterGroup({
        chunkedLoading: true,
        spiderfyDistanceMultiplier: 3,
        maxClusterRadius: 14,
    })
    // Géolocalisation de l'utilisateur

    const polygon_style = {
        color: '#8A2BE2',
        fillColor: '#9370DB',
        weight: 2,
        opacity: 0.8,
        fillOpacity: 0.2,
    }

    // Ajout du marker de la localité
    console.debug('metasite', metasite)
    const metasiteObject = L.geoJson(metasite, polygon_style)
    metasiteObject.addTo(map).bringToBack()
    map.fitBounds(metasiteObject.getBounds())
    const params = { metaplace: id_metaplace }
    $http
        .get(placesUrl, { params })
        .then((r) => {
            this.data = r.data.results
            markers
                .addLayer(L.geoJson(data, placeMarkerOptions()))
                .addTo(map)
                .bringToFront()
            map.spin(false)
        })
        .catch((error) => console.error(error))
}

const leafletForm = () => {
    window.addEventListener('map:init', function (event) {
        const places = L.markerClusterGroup({
            chunkedLoading: true,
            spiderfyDistanceMultiplier: 3,
            maxClusterRadius: 14,
        })

        const metaplaces = L.featureGroup()

        const style_data = {
            radius: 6,
            fillColor: '#ff4c41',
            color: '#ddd',
            weight: 2,
            opacity: 1,
            fillOpacity: 0.8,
        }

        const style_data_metasite = {
            radius: 6,
            fillColor: '#8A2BE2',
            color: '#ddd',
            weight: 2,
            opacity: 1,
            fillOpacity: 0.8,
        }

        const polygon_style = {
            color: '#8A2BE2',
            fillColor: '#9370DB',
            weight: 2,
            opacity: 0.8,
            fillOpacity: 0.2,
        }

        const map = event.detail.map

        // Fonction plein écran
        map.addControl(new L.Control.Fullscreen())

        // Géolocalisation de l'utilisateur

        const locateOptions = {
            flyTo: true,
            title: 'Ma position',
            icon: 'fa fa-location-arrow',
        }

        L.control.locate(locateOptions).addTo(map)

        // Recherche par adresse
        map.addControl(
            new L.Control.Search({
                url: 'https://nominatim.openstreetmap.org/search?format=json&accept-language=fr-FR&q={s}',
                jsonpParam: 'json_callback',
                propertyName: 'display_name',
                propertyLoc: ['lat', 'lon'],
                markerLocation: true,
                autoType: true,
                autoCollapse: true,
                minLength: 2,
                zoom: 13,
                text: 'Recherche...',
                textCancel: 'Annuler',
                textErr: 'Erreur',
            })
        )

        // Add legend
        const legend = L.control({
            position: 'bottomright',
        })

        legend.onAdd = function (map) {
            const div = L.DomUtil.create('div', 'info legend')
            div.setAttribute('onmouseenter', 'showDisclaimer()')
            div.setAttribute('onmouseleave', 'hideDisclaimer()')
            div.id = 'info legend'
            div.innerHTML = "<b class='fa fa-info-circle'></b>"
            return div
        }
        legend.addTo(map)

        let locationLayer = Object

        const overlayText = L.control({
            position: 'topright',
        })
        overlayText.onAdd = function (map) {
            const div = L.DomUtil.create('div', 'zoomInfo')
            div.innerHTML +=
                '<p class="label label-primary">Zoomez pour afficher les données</p>'
            return div
        }
        overlayText.addTo(map)

        let controller

        const loadPlaces = (controller) => {
            const signal = controller.signal
            console.debug('before abort', controller, signal)

            // Download GeoJSON data with Ajax
            console.debug('after abort', controller, signal)
            map.spin(true)
            const dataBboxUrl =
                placeBaseUrl +
                '?format=json&in_bbox=' +
                map.getBounds().toBBoxString()
            console.debug('url', dataBboxUrl)
            console.debug('$http', $http.get(dataBboxUrl, { signal }))
            $http
                .get(dataBboxUrl, { signal })
                .then(function (resp) {
                    const data = resp.data
                    console.debug('data', data)
                    if (data.results) {
                        const placeLayer = L.geoJson(data.results, {
                            onEachFeature: function onEachFeature(
                                feature,
                                layer
                            ) {
                                var props = feature.properties
                                var content =
                                    '<h3>Site&nbsp;:&nbsp;' +
                                    props.name +
                                    '</h3><p><ul><li><b>Type</b>: ' +
                                    (props.type_data
                                        ? props.type_data.descr
                                        : 'Non défini') +
                                    '</li><li><b>Nombre de sessions</b> ' +
                                    (props.total_sessions
                                        ? props.total_sessions
                                        : 'Aucune') +
                                    '&nbsp;(Dernière: ' +
                                    (props.last_session
                                        ? props.last_session.date_start__max
                                        : 'Aucune') +
                                    ')</li></ul> </p><div class="text-right"><div class="btn-group" role="group"><a type="button" class="btn btn-default btn-sm" align="center" style="color:white;" href="/place/' +
                                    feature.id +
                                    '/detail" target="_blank"><i class="fa fa-fw fa-info-circle"></i></a><a type="button" class="btn btn-info btn-sm" align="center" style="color:white;" href="/place/' +
                                    feature.id +
                                    '/update" target="_blank"><i class="fa fa-fw fa-edit"></i></a></div></div>'
                                layer.bindPopup(content)
                            },
                            pointToLayer: function (feature, latlng) {
                                // Set a different color if location is in a metaplace
                                if (feature.properties.metaplace) {
                                    return L.circleMarker(
                                        latlng,
                                        style_data_metasite
                                    )
                                }
                                return L.circleMarker(latlng, style_data)
                            },
                        })
                        places.clearLayers()
                        places.addLayer(placeLayer).addTo(map).bringToFront()
                    }
                })
                .then(function () {
                    map.spin(false)
                })
                .catch(function (error) {
                    console.debug('<loadPlace leaflet error>', error)
                })
        }

        const loadMetaplace = (controller) => {
            const signal = controller.signal
            console.debug('before abort', controller, signal)
            // if (controller) controller.abort()
            const dataBboxUrl =
                metaplaceBaseUrl +
                '?format=json&in_bbox=' +
                map.getBounds().toBBoxString()
            $http
                .get(dataBboxUrl, { signal })
                .then((resp) => {
                    const data = resp.data
                    if (data.results) {
                        const metaplaceLayer = L.geoJSON(data.results, {
                            style: polygon_style,
                            onEachFeature: function onEachFeature(
                                feature,
                                layer
                            ) {
                                var props = feature.properties
                                var content =
                                    '<h3>MetaSite&nbsp;:&nbsp;' +
                                    props.name +
                                    '</h3><p><ul><li><b>Type</b>: ' +
                                    (props.type_data
                                        ? props.type_data.descr
                                        : 'Non défini') +
                                    '</li></ul> </p><div class="text-right"><div class="btn-group" role="group"><a type="button" class="btn btn-default btn-sm" align="center" style="color:white;" href="/metaplace/' +
                                    feature.id +
                                    '/detail" target="_blank"><i class="fa fa-fw fa-info-circle"></i></a><a type="button" class="btn btn-info btn-sm" align="center" style="color:white;" href="/metaplace/' +
                                    feature.id +
                                    '/update" target="_blank"><i class="fa fa-fw fa-edit"></i></a></div></div>'
                                layer.bindPopup(content)
                            },
                        })
                        metaplaces.clearLayers()
                        metaplaces
                            .addLayer(metaplaceLayer)
                            .addTo(map)
                            .bringToBack()
                    }
                })
                .then(function () {
                    map.spin(false)
                })
                .catch(function (error) {
                    console.debug('loadMetaplace Leaflet error', error)
                })
        }

        var debounce = null

        const loadGeoData = () => {
            if (controller) controller.abort()
            controller = new AbortController()
            loadPlaces(controller)
            loadMetaplace(controller)
        }

        map.on('moveend', function () {
            var currZoom = map.getZoom()
            if (currZoom >= 13) {
                map.removeControl(overlayText)
                places.clearLayers()
                metaplaces.clearLayers()
                clearTimeout(debounce)
                debounce = setTimeout(loadGeoData, 1000)
            } else {
                places.clearLayers()
                metaplaces.clearLayers()
                overlayText.addTo(map)
            }
        })

        // Trigger zoom on coordinate with manual filling
        $('#id_x,#id_y,#id_srid').on('input', function () {
            proj4.defs([
                ['4326', '+proj=longlat +datum=WGS84 +no_defs +type=crs'],
                [
                    '2154',
                    '+proj=lcc +lat_0=46.5 +lon_0=3 +lat_1=49 +lat_2=44 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs +type=crs',
                ],
            ])
            let x = $('#id_x').val()
            let y = $('#id_y').val()
            let srid = $('#id_srid').val()
            if ((x && y)) {
                console.debug('XY', x, y)
                console.debug('map._layers', map._layers)
                console.debug(
                    proj4(srid, '4326', [parseFloat(x), parseFloat(y)])
                )
                const coords = proj4(srid, '4326', [
                    parseFloat(x),
                    parseFloat(y),
                ])
                map.setView(coords.reverse(), 16)
            }
        })
        // Reset fields if users draw/edit the leaflet marker
        map.on('draw:created draw:edited', function (e) {
            $('#id_x').val('') && $('#id_y').val('')
        })
    })
}

const AreaSynthesis = (apiUrl) => {
    return {
        el: '#AreaSynthesis',
        delimiters: ['[[', ']]'],
        data() {
            return {
                data: {},
                loading: true,
                id: null,
                area_type: null,
                dicts: Object,
            }
        },
        watch: {
            id: function (val, oldVal) {
                window.location = `/synthesis/area/${val}`
            },
        },
        methods: {
            getDicts() {
                this.dicts = JSON.parse(localStorage.getItem('dicts'))
            },
            getData() {
                const queryUrl = `${apiUrl}`
                $http
                    .get(queryUrl)
                    .then((r) => {
                        console.debug(r)
                        this.data = r.data
                        this.loading = false
                        console.debug(this.data)
                    })
                    .catch((error) => console.error(error))
            },
        },
        mounted() {
            this.getDicts()
            this.getData()
        },
    }
}

const genericApiVue = (apiUrl, el, autoreload = false) => {
    return {
        el: el,
        delimiters: ['[[', ']]'],
        data() {
            return {
                data: {},
                loading: true,
                timer: '',
            }
        },
        methods: {
            getData() {
                const queryUrl = `${apiUrl}`
                $http
                    .get(queryUrl)
                    .then((r) => {
                        console.debug(r)
                        this.data = r.data
                        this.loading = false
                        if (this.data['export_file'] && autoreload) {
                            this.cancelAutoUpdate()
                        }
                    })
                    .catch((error) => console.error(error))
            },
            cancelAutoUpdate() {
                clearInterval(this.timer)
            },
        },
        // mounted() {
        //     this.getData()
        // },
        created() {
            this.getData()
            if (autoreload) {
                this.timer = setInterval(this.getData, 3000)
            }
        },
        beforeDestroy() {
            if (autoreload) {
                this.cancelAutoUpdate()
            }
        },
    }
}

const homeAreaSearch = () => {
    return {
        el: '#homeAreaSearch',
        data: {
            id: null,
            area_type: null,
            dicts: Object,
        },
        watch: {
            id: function (val, oldVal) {
                window.location = `/synthesis/area/${val}`
            },
        },
        methods: {
            getDicts() {
                this.dicts = JSON.parse(localStorage.getItem('dicts'))
            },
        },
        mounted() {
            this.getDicts()
        }
    }
}

Vue.component('session-detail', {
    data: () => {
        return {
            displayObs: false,
        }
    },
    props: ['session'],
    template: `
    <div style="display: contents">
        <tr @click="displayObs = !displayObs" :class="displayObs ? 'active':''">
            <td>
                <a :href="'/session/'+session.id+'/detail'" class="text-primary"><i class="fa fa-info-circle"
                    title="Fiche détaillée" ></i></a>
                <a href="'/session/'+session.id+'/update'" class="text-info"><i class="fa fa-edit" title="Editer"></i></a>
              </td>
            <td><strong>{{session.date_start}}</strong><i class="text-muted">{{session.time_start? ' '+session.time_start:''}}</i> - {{session.date_end}}<i class="text-muted">{{session.time_end ? ' '+session.time_end:''}}</i></td>
            <td class="text-center">{{session.contact.code}}</td>
            <td>{{session.main_observer.full_name}} <span v-if="session.other_observer.length">{{session.other_observer.length}}</span></td>
            <td class="text-center">{{session.observations.length === 0 ? '!':session.observations.length}}</td>
            <td class="text-center">{{session.study ? session.study.name : '-'}}</td>
        </tr>
        <tr v-if="displayObs && session.observations.length">
            <td colspan="6">
                <div style="display: contents">
                    <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>Taxon</th>
                            <th>Nombre total</th>
                            <th>Reproduction</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="o in session.observations">
                            <td>{{o.codesp.common_name_fr}} (<i>{{o.codesp.sci_name}}</i>)</td>
                            <td>{{o.total_count}}</td>
                            <td v-html="o.breed_colo ? '<strong>oui</strong>':'non'"></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </td>
        </tr>
    </div>`,
})

Vue.component('sessionList', {
    props: ['place', 'newSessionUrl'],
    data: function () {
        return {
            data: null,
            count: 0,
            displayedObs: null,
            page_size: 10,
            page: 1,
            nextUrl: null,
            previousUrl: null,
        }
    },
    template: `
    <div class="panel panel-default panel-info panel-table">
        <!-- Default panel contents -->
        <div class="panel-heading">
          <div class="pull-right btn-group btn-group-xs">
            <a class="btn btn-default" data-toggle="collapse" data-parent="#accordion" href="#collapseSession"
              title="voir/cacher la liste"><span class="caret"></span></a>
            <a :href="newSessionUrl" class="pull-right btn btn-primary btn-xs"><i
                class="fi-plus"></i> Nouvelle session</a>
          </div>
          <h4><i class="fa fa-fw fa-calendar-alt"></i> Sessions d'inventaires</a>
            <span class="badge">{{ count }}</span>
          </h4>
        </div>
        <div v-if="data && data.length > 0" id="collapseSession" class="panel-body">
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>Actions</th>
                    <th>Date</th>
                    <th class="text-center">Methode</th>
                    <th>Observateurs</th>
                    <th class="text-center">Observations</th>
                    <th>Etude</th>
                </tr>
            </thead>
            <tbody>
            <slot v-if="data" v-for="(session, index) in data">
            <tr @click="setDisplayedObs(index)" :class="displayedObs === index ? 'success':''">
                <td>
                    <a :href="'/session/'+session.id_session+'/detail'" class="text-primary"><i class="fa fa-info-circle"
                        title="Fiche détaillée" ></i></a>
                    <a :href="'/session/'+session.id_session+'/update'" class="text-info"><i class="fa fa-edit" title="Editer"></i></a>
                    <a :href="'/session/'+session.id_session+'/sighting/edit'" class="text-info"><i class="fa fa-eye" title="Editer les observations"></i></a>
                </td>
                <td><strong>{{session.date_start}}</strong><i class="text-muted">{{session.time_start? ' '+session.time_start:''}}</i> - {{session.date_end}}<i class="text-muted">{{session.time_end ? ' '+session.time_end:''}}</i></td>
                <td class="text-center">{{session.contact.descr}}</td>
                <td>{{session.main_observer.full_name}} <span v-if="session.other_observer.length" class="badge">+{{session.other_observer.length}}</span></td>
                <td class="text-center" v-html="session.observations.length === 0 ? '!':session.observations.length"></td>
                <td><span v-if="session.study"><a :href="studyUrl(session)">{{session.study.name}}</a></span></span v-else>-</span></td>
            </tr>
            <tr v-if="displayedObs === index">
                <td style="padding: 0" colspan="6">
                    <div style="padding: 10px" v-if="session.other_observer.length">
                        <strong>Autres observateurs</strong> : {{session.other_observer.map(i => i.full_name).join(', ')}}
                    </div>
                    <div  v-if="session.observations.length" style="display: contents" >
                        <table style="margin-bottom: 0;" class="table table-condensed">
                        <thead class="bg-info text-white">
                            <tr>
                                <th>Taxon</th>
                                <th class="text-center">Nombre total</th>
                                <th class="text-center">Reproduction</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr :class="o.breed_colo ? 'success':''" v-for="o in session.observations.sort((a,b)=> a.total_count < b.total_count)">
                                <td>
                                    <span class="label" :class="o.codesp.sp_true ? 'label-success':'label-default'">{{o.codesp.codesp}}</span>
                                    <strong>{{o.codesp.common_name_fr}}</strong> (<i>{{o.codesp.sci_name}}</i>)</td>
                                <td class="text-center">{{o.total_count}}</td>
                                <td class="text-center"><i v-show="o.breed_colo" class="fas fa-baby fa-fw text-success"></i></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="text-warning" v-else>
                        <i class="fa fa-fw fa-exclamation-triangle "></i>La session ne dispose pas d'observations ou vous ne disposez pas de droits suffisants pour les visualier.
                    </div>
                </td>
            </tr>
            </slot>
            </tbody>
        </table>
        <div v-if="pages.length > 1" class="panel-footer text-right">
            <nav aria-label="Page navigation">
                <ul class="float-right pagination pagination-sm" >
                    <li v-for="p in pages" :class="p === page?'active':''" ><a @click="setPage(p)">{{p}}</a></li>
                </ul>
            </nav>
        </div>
    </div>
    `,
    watch: {
        page: function () {
            this.displayedObs = null

            this.getData()
        },
    },
    mounted() {
        this.getData()
    },
    computed: {
        pages() {
            return Array.from(
                { length: Math.ceil(this.count / this.page_size) },
                (value, index) => index + 1
            )
        },
    },
    methods: {
        async getData() {
            const queryUrl = `/api/v1/sessions`
            params = {
                place: this.place,
                page_size: this.page_size,
                page: this.page,
            }
            await $http
                .get(queryUrl, { params: params })
                .then((r) => {
                    this.data = r.data.results
                    this.count = r.data.count
                    this.nextUrl = r.data.next
                    this.previousUrl = r.data.previous
                    // this.loading = false
                })
                .catch((error) => console.error(error))
        },
        setDisplayedObs(index) {
            this.displayedObs =
                this.displayedObs === null || this.displayedObs !== index
                    ? index
                    : null
        },
        studyUrl(session) {
            return session.study ? `/study/${session.study.pk}/detail` : null
        },
        setPage(page) {
            this.page = page
        },
    },
})

const placeDetail = (place, newSessionUrl) => {
    return {
        el: '#placeDetail',
        data: {
            place: place,
            newSessionUrl: newSessionUrl,
        },
    }
}
