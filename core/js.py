DateAndTimeInput = """
        $(function () {
        let Today = new Date(new Date().getFullYear(),
                             new Date().getMonth(),
                             new Date().getDate(),
                             23,59,59
                             )
            $('.dateinput').datetimepicker({
                useCurrent: false,
                format: 'DD/MM/YYYY',
                maxDate: Today,
                showTodayButton: true,
                showClear: true,
                showClose: true
            });
            $('.timeinput').datetimepicker({
                useCurrent: false,
                defaultDate:false,
                format: 'HH:mm',
            });
        });
"""

DateInput = """
        $(function () {
            let Today = new Date(new Date().getFullYear(),
                                 new Date().getMonth(),
                                 new Date().getDate(),
                                 23,59,59
                                 )
            $('.dateinput').datetimepicker({
                useCurrent: false,
                format: 'DD/MM/YYYY',
                maxDate: Today,
                showTodayButton: true,
                showClear: true,
                showClose: true
            });
        });
"""


TimeInput = """
        $(function () {
            $('.timeinput').datetimepicker({
                format: 'HH:mm'
            });
        });
"""
