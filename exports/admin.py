from django.contrib import admin

from .models import Exports

# Register your models here.


class ExportsAdmin(admin.ModelAdmin):
    model = Exports
    list_display = (
        "id",
        "export_file",
        "created_by",
        "expiration_date",
        "active",
        "timestamp_create",
    )
    search_fields = ("created_by", "export_file")
    sortable_field_name = ["__all__"]


# Register your models here.
admin.site.register(Exports, ExportsAdmin)
