from django.urls import path
from rest_framework.routers import DefaultRouter

from .api import ExportViewSet
from .views import ExportDetail, ExportList

app_name = "exports"
router = DefaultRouter()

router.register(r"api/v1/exports", ExportViewSet, basename="exports")

urlpatterns = router.urls
urlpatterns += [
    # url de recherches de localités
    path("exports/<int:pk>", ExportDetail.as_view(), name="export_detail"),
    path("exports/", ExportList.as_view(), name="export_list"),
]
