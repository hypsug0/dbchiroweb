import datetime
import os

from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _

# Create your models here.


class Exports(models.Model):
    export_file = models.FileField(
        blank=False, null=False, upload_to="exports", verbose_name=_("Fichiers à exporter")
    )
    expiration_date = models.DateTimeField(
        blank=False,
        null=False,
        verbose_name=_("Date d'expiration"),
        editable=False,
    )
    search_qs = models.JSONField(blank=True, null=True, verbose_name=_("Critères de recherche"))
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        db_index=True,
        editable=True,
        related_name="exports",
        on_delete=models.SET_NULL,
    )
    timestamp_create = models.DateTimeField(auto_now_add=True, editable=False)
    active = models.BooleanField(default=True)
    ready = models.BooleanField(default=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # self.deactivate_exports()

    @classmethod
    def disable_exports(cls, *args, **kwargs):
        now = datetime.datetime.now()
        exports = cls.objects.filter(expiration_date__lte=now, active=True)
        for e in exports:
            e.active = False
            if os.path.isfile(e.export_file.path):
                os.remove(e.export_file.path)
            e.save()

    def save(self, *args, **kwargs):
        self.expiration_date = datetime.datetime.now() + datetime.timedelta(days=7)
        super().save(*args, **kwargs)

    def notify(self, *args, **kwargs):
        url = "http://" + settings.ALLOWED_HOSTS[0] + self.export_file.url
        # url = instance.export_file.url
        txt_message = f"""
        Votre nouvel export est disponible.
        Vous trouverez-ci-après le lien vers le nouvel export que vous avez demandé.
        ce lien expirera le {self.expiration_date}

        {url}

        --
        {settings.SITE_NAME}
        """
        html_message = f"""
        <h1>Votre nouvel export est disponible</h1>
        <p>
            Vous trouverez-ci-après le lien vers le nouvel export que vous avez demandé.<br>
            <span style="color: red">Ce lien expirera le <b>{self.expiration_date}</b></span>
        </p>
        <p>
            Lien : <a href='{url}'>{url}</a>
        </p>
        <p>
            --<br>
            {settings.SITE_NAME}
        </p>"""
        self.created_by.email_user(
            f"[{settings.SITE_NAME}] Nouvel export disponible",
            txt_message,
            settings.DEFAULT_FROM_EMAIL,
            html_message=html_message,
        )


# @receiver(post_save, sender=Exports)
# def export_notify_user(sender, instance, created, **kwargs):
#     if created:
#         csv_file =
#         url = "http://" + settings.ALLOWED_HOSTS[0] + instance.export_file.url
#         # url = instance.export_file.url
#         txt_message = f"""
#         Votre nouvel export est disponible.
#         Vous trouverez-ci-après le lien vers le nouvel export que vous avez demandé.
#         ce lien expirera le {instance.expiration_date}

#         {url}

#         --
#         {settings.SITE_NAME}
#         """
#         html_message = f"""
#         <h1>Votre nouvel export est disponible</h1>
#         <p>
#             Vous trouverez-ci-après le lien vers le nouvel export que vous avez demandé.<br>
#             <span style="color: red">Ce lien expirera le <b>{instance.expiration_date}</b></span>
#         </p>
#         <p>
#             Lien : <a href='{url}'>{url}</a>
#         </p>
#         <p>
#             --<br>
#             {settings.SITE_NAME}
#         </p>"""
#         instance.created_by.email_user(
#             f"[{settings.SITE_NAME}] Nouvel export disponible",
#             txt_message,
#             settings.DEFAULT_FROM_EMAIL,
#             html_message=html_message,
#         )
