from rest_framework.serializers import ModelSerializer

from accounts.serializer import ProfileSearchSerializer

from .models import Exports


class ExportsSerializer(ModelSerializer):
    created_by = ProfileSearchSerializer()

    class Meta:
        model = Exports
        fields = [
            "id",
            "export_file",
            "expiration_date",
            "created_by",
            "search_qs",
            "timestamp_create",
            "active",
            "ready",
        ]
