# Generated by Django 4.2.11 on 2024-03-26 14:11

from django.db import migrations, models
import uuid


def create_uuid(apps, schema_editor):
    Exports = apps.get_model("exports", "Exports")
    for export in Exports.objects.all():
        export.uuid = uuid.uuid4()
        export.save()


class Migration(migrations.Migration):
    dependencies = [
        ("exports", "0004_alter_exports_export_file"),
    ]

    operations = [
        migrations.AddField(
            model_name="exports",
            name="uuid",
            field=models.UUIDField(blank=True, null=True),
        ),
        migrations.RunPython(create_uuid),
        migrations.AlterField(
            model_name="exports",
            name="uuid",
            field=models.UUIDField(default=uuid.uuid4, editable=False, unique=True),
        ),
    ]
