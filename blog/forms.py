from crispy_forms.bootstrap import Div
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Button, Column, Layout, Row, Submit
from django import forms
from django.utils.translation import gettext_lazy as _

from .models import Actu


class ActuForm(forms.ModelForm):
    class Meta:
        model = Actu
        fields = ("title", "briefdescr", "text")

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = "submit"
        self.helper.form_show_errors = True
        self.helper.layout = Layout(
            Row(
                Column(
                    Submit("submit", _("Enregistrer"), css_class="btn-primary btn-sm"),
                    Button(
                        "cancel",
                        _("Retour"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group pull-right",
                    role="button",
                ),
            ),
            Div(
                Div(
                    Row(
                        Column(
                            Column("title", css_class="col-lg-12"),
                            Column("briefdescr", css_class="col-lg-12"),
                            Column("text", css_class="col-lg-12"),
                            css_class="col-lg-12",
                        ),
                    ),
                    css_class="panel-body",
                ),
                css_class="panel panel-default",
            ),
            Row(
                Column(
                    Submit("submit", _("Enregistrer"), css_class="btn-primary btn-sm"),
                    Button(
                        "cancel",
                        _("Retour"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group pull-right",
                    role="button",
                ),
            ),
        )

        super().__init__(*args, **kwargs)
