from django.urls import path

from .views import ActuCreate, ActuDelete, ActuDetail, ActuList, ActuUpdate, Home

app_name = "blog"

urlpatterns = [
    path("", Home.as_view(), name="home"),
    path("news/list", ActuList.as_view(), name="news_list"),
    path("news/<int:pk>/detail", ActuDetail.as_view(), name="news_detail"),
    path("news/add", ActuCreate.as_view(), name="news_create"),
    path("news/<int:pk>/update", ActuUpdate.as_view(), name="news_update"),
    path("news/<int:pk>/delete", ActuDelete.as_view(), name="news_delete"),
]
