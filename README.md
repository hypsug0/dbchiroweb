# dbChiro[web]

## Le projet

**dbChiro[web] est une base de donnée en ligne pour la collecte des données d'études des chauves-souris.**

Développée pour succéder à la base de donnée bdChironalpes du Groupe Chiroptère Rhône-Alpes, cette outil est développé avec le Framework Django associé à une base de données spatiale PostgreSQL/PostGIS. Il reprend en grande partie le modèle structurel de la base de donnée d'origine (dispositif MS Access/MapInfo).

Une démo est disponible ici: http://demo.dbchiro.org

Pour le moment, quatre types d'utilisateurs sont proposés dont les paramètres de connexion sont les suivants :

   * **Administrateur technique** qui a accès au backoffice;
   * **Administrateur des données** qui a accès en lecture et/ou modification à toutes l'intégralité des données;
   * **Coordinateur ou responsable** qui a accès à la totalité des localités et des données du territoire dont il est responsable ainsi que ses propres données;
   * **Observateur** qui n'a accès qu'aux localités non sensibles ou aux localités pour lesquelles il est autorisé et à ses propres données (en observateur principal ou observateur associé).

Pour les tester sur la démo:

|Compte|Identifiant|Mot de passe|
|-|-|-|
|**Administrateur technique**|dbadmin|dbAdmin#dbChiro*|
|**Coordinateur** du réseau/d'une section locale|responsable|Resp#dbChiro*|
|**Observateur**|observateur|Observ#dbChiro*|

Une liste de discussion dédiée au projet a été crée (dbchiro [ chez ] framalistes.org):

* Pour s'inscrire ➙ https://framalistes.org/sympa/subscribe/dbchiro
* Pour se désinscrire ➙ https://framalistes.org/sympa/sigrequest/dbchiro

Contributeurs recherchés pour contribuer au projet:

* **HTML/CSS/Javascript** pour l'interface utilisateur de la base de données;
* **Python/Django** et **PostgreSQL/PostGIS** pour la motorisation de la base de données;
* **QGIS/PyQGIS/PyQT** pour l'outil d'exploitation des données.

## Installation

Allez sur [http://docs.dbchiro.org](http://docs.dbchiro.org) ou dans `docs/source/installation.rst`
