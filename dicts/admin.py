import logging

from django.apps import AppConfig, apps
from django.contrib import admin

logger = logging.getLogger(__name__)


class ListAdminMixin(object):
    def __init__(self, model, admin_site):
        self.list_display = [field.name for field in model._meta.fields if field.name != "id"]
        super().__init__(model, admin_site)


class CustomApp(AppConfig):
    name = "dicts"

    def ready(self):
        models = apps.get_app_config("dicts").get_models()
        for model in models:
            admin_class = type("AdminClass", (ListAdminMixin, admin.ModelAdmin), {})
            try:
                admin.site.register(model, admin_class)
            except admin.sites.AlreadyRegistered as e:
                logger.error(e)
                pass
