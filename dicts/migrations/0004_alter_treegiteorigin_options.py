# Generated by Django 3.2.13 on 2022-07-18 19:45

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("dicts", "0003_alter_areatype_id"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="treegiteorigin",
            options={
                "verbose_name_plural": "Dictionnaire des origines des types de gîtes arboricoles"
            },
        ),
    ]
