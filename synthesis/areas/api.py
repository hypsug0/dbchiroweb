from django.db.models import Count, Q
from rest_framework.generics import RetrieveAPIView

from geodata.models import Areas

from .serializers import AreaSynthesisSerializer


class AreaSynthesisApi(RetrieveAPIView):
    queryset = Areas.objects.all()
    # template_name = "areas.html"
    serializer_class = AreaSynthesisSerializer

    def get_queryset(self):
        qs = super().get_queryset()
        qs = (
            qs.prefetch_related("places")
            .prefetch_related("places__sessions")
            .prefetch_related("places__sessions__observations__codesp")
            .prefetch_related("places__sessions__main_observer")
            .prefetch_related("places__sessions__study")
        )
        qs = qs.annotate(
            count_places=Count("places", distinct=True),
            count_sessions=Count("places__sessions", distinct=True),
            count_taxa=Count(
                "places__sessions__observations__codesp",
                filter=Q(places__sessions__observations__codesp__sp_true=True),
                distinct=True,
            ),
            count_obs=Count("places__sessions__observations", distinct=True),
            count_observers=Count("places__sessions__main_observer", distinct=True),
            count_studies=Count("places__sessions__study", distinct=True),
        )

        return qs
