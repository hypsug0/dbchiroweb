import logging

from rest_framework.serializers import IntegerField, ModelSerializer, SerializerMethodField

from accounts.models import Profile
from dicts.models import AreaType, Specie
from dicts.serializers import DictsGenericSerializer
from geodata.models import Areas
from management.models import Study

logger = logging.getLogger(__name__)


class AreaTypeSerializer(DictsGenericSerializer):
    class Meta:
        model = AreaType
        fields = DictsGenericSerializer.Meta.fields + ("name",)


class AreaSerializer(ModelSerializer):
    area_type = AreaTypeSerializer()

    class Meta:
        model = Areas
        fields = (
            "pk",
            "area_type",
            "name",
            "code",
        )


class ObserversAreaSynthesisSerializer(ModelSerializer):
    class Meta:
        model = Profile
        fields = ("pk", "full_name")


class SpeciesAreaSynthesisSerializer(ModelSerializer):
    # groupings = self

    class Meta:
        model = Specie
        fields = (
            "pk",
            "codesp",
            "sp_true",
            "sci_name",
            "common_name_fr",
        )


class StudiesAreaSynthesisSerializer(ModelSerializer):
    project_manager = ObserversAreaSynthesisSerializer()

    class Meta:
        model = Study
        fields = (
            "pk",
            "uuid",
            "name",
            "year",
            "project_manager",
        )


class AreaSynthesisSerializer(ModelSerializer):
    taxa = SerializerMethodField()
    studies = SerializerMethodField()
    area_type = AreaTypeSerializer()
    surrounding_areas = SerializerMethodField()
    count_places = IntegerField()
    count_sessions = IntegerField()
    count_taxa = IntegerField()
    count_obs = IntegerField()
    count_observers = IntegerField()
    count_studies = IntegerField()

    class Meta:
        model = Areas
        fields = (
            "pk",
            "area_type",
            "name",
            "code",
            "taxa",
            "surrounding_areas",
            "studies",
            "count_places",
            "count_sessions",
            "count_taxa",
            "count_obs",
            "count_observers",
            "count_studies",
        )

    def get_taxa(self, obj):
        qs = (
            Specie.objects.filter(sightings__session__place__areas=obj)
            .distinct()
            .order_by("-sp_true", "sys_order")
        )
        return SpeciesAreaSynthesisSerializer(qs, many=True).data

    def get_surrounding_areas(self, obj):
        qs = Areas.objects.filter(geom__intersects=obj.geom).order_by("id")
        return AreaSerializer(qs, many=True).data

    def get_studies(self, obj):
        qs = Study.objects.filter(sessions__place__areas=obj).distinct()
        # .distinct().order_by("id_study")
        logger.debug(f"study QS {qs}")
        return StudiesAreaSynthesisSerializer(qs, many=True).data
