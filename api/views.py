# import the logging library
import logging

from dal import autocomplete
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.serializers import serialize
from django.db.models import Q
from django.http import HttpResponse
from sinp_organisms.models import Organism

from accounts.models import UserFullName
from dicts.models import Specie
from geodata.models import Areas
from management.models import Study
from sights.models import Metaplace, Place

# from djgeojson.views import GeoJSONLayerView, TiledGeoJSONLayerView


# Get an instance of a logger
logger = logging.getLogger(__name__)


class ActiveUserAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return get_user_model().objects.none()

        qs = get_user_model().objects.filter(is_active=True).order_by("username")

        if self.q:
            qs = qs.filter(username__icontains=self.q)

        return qs


class AllUserAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return UserFullName.objects.none()

        qs = UserFullName.objects.all().order_by("username")

        if self.q:
            qs = qs.filter(
                Q(username__icontains=self.q)
                | Q(first_name__icontains=self.q)
                | Q(last_name__icontains=self.q)
            )
        return qs


class TaxaAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Specie.objects.none()

        qs = Specie.objects.all().order_by("-sp_true")

        if self.q:
            qs = qs.filter(
                Q(codesp__icontains=self.q)
                | Q(common_name_fr__icontains=self.q)
                | Q(sci_name__icontains=self.q)
            )

        return qs


class AreasAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated:
            return Areas.objects.none()

        qs = Areas.objects.all().order_by("code")

        if self.q:
            qs = qs.filter(name__icontains=self.q)

        return qs


class MetaplaceAutocomplete(LoginRequiredMixin, autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Metaplace.objects.none

        qs = Metaplace.objects.all()

        if self.q:
            qs = qs.filter(
                Q(name__icontains=self.q)
                | Q(type__descr__icontains=self.q)
                | Q(type__code__icontains=self.q)
            )
        logger.debug(qs)
        return qs


class PlaceAutocomplete(LoginRequiredMixin, autocomplete.Select2QuerySetView):
    def get_queryset(self):
        loggeduser = self.request.user
        userdetail = get_user_model().objects.get(id=loggeduser.id)
        places = Place.objects.filter(telemetric_crossaz=False)
        if userdetail.access_all_data is True:
            logger.debug("Chargement de la carte pour l'utilisateur avec accès total")
            qs = places
        elif userdetail.is_resp:
            logger.debug("Chargement de la carte pour le Coordinateur")
            resp_areas = userdetail.resp_areas.all()
            qs = places.filter(
                Q(areas__in=resp_areas)
                | (
                    (
                        ~Q(areas__in=resp_areas)
                        & (
                            Q(is_hidden=False)
                            | (
                                Q(is_hidden=True)
                                & (Q(created_by=loggeduser) | Q(authorized_user=loggeduser))
                            )
                        )
                    )
                )
            )
        else:
            logger.debug("Chargement de la carte pour l'obs lambda")
            qs = places.filter(
                Q(is_hidden=False) | (Q(is_hidden=True) & Q(authorized_user=loggeduser))
            )
        if self.q:
            qs = qs.filter(
                Q(name__icontains=self.q)
                | Q(areas__name__icontains=self.q)
                | Q(areas__code__contains=self.q)
            )
        return qs


class OrganismAutocomplete(LoginRequiredMixin, autocomplete.Select2QuerySetView):
    """Organism autocomplete api for forms autocompletion"""

    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Organism.objects.none

        qs = Organism.objects.filter(enabled=True).all()

        if self.q:
            qs = qs.filter(
                Q(label__icontains=self.q)
                | Q(short_label__icontains=self.q)
                | Q(uuid__icontains=self.q)
            )
        logger.debug(qs)
        return qs


@login_required
def places_as_geojson(request):
    """
    Vue permettant la génération d'un fichier geojson listant les localités que l'utilisateur est authorisé à voir

    :param request:
    :return:
    """
    loggeduser = request.user
    userdetail = get_user_model().objects.get(id=loggeduser.id)
    places = Place.objects.filter(telemetric_crossaz=False)
    if userdetail.access_all_data is True:
        logger.debug("Chargement de la carte pour l'utilisateur avec accès total")
        place_list = places
    elif userdetail.is_resp:
        logger.debug("Chargement de la carte pour le Coordinateur")
        resp_areas = userdetail.resp_areas.all()
        place_list = places.filter(
            Q(areas__in=resp_areas)
            | (
                (
                    ~Q(areas__in=resp_areas)
                    & (
                        Q(is_hidden=False)
                        | (
                            Q(is_hidden=True)
                            & (Q(created_by=loggeduser) | Q(authorized_user=loggeduser))
                        )
                    )
                )
            )
        )
    else:
        logger.debug("Chargement de la carte pour l'obs lambda")
        place_list = places.filter(
            Q(is_hidden=False) | (Q(is_hidden=True) & Q(authorized_user=loggeduser))
        )
    field = ("pk", "name", "metaplace")
    geojson = serialize("geojson", place_list, geometry_field="geom", fields=field)
    return HttpResponse(geojson, content_type="application/json")


@login_required
def metaplaces_as_geojson(request):
    """
    Vue permettant la génération d'un fichier geojson listant les métalocalités
    """
    metaplaces = Metaplace.objects.exclude(geom__isnull=True)
    field = (
        "pk",
        "name",
    )
    geojson = serialize("geojson", metaplaces, geometry_field="geom", fields=field)
    return HttpResponse(geojson, content_type="application/json")


@login_required
def places_in_metaplaces_as_geojson(request):
    """
    Vue permettant la génération d'un fichier geojson listant les localités que l'utilisateur est authorisé à voir

    :param request:
    :return:
    """
    loggeduser = request.user
    userdetail = get_user_model().objects.get(id=loggeduser.id)
    places = Place.objects.exclude(metaplace__isnull=True)
    if userdetail.access_all_data is True:
        logger.debug("Chargement de la carte pour l'utilisateur avec accès total")
        place_list = places
    elif userdetail.is_resp:
        logger.debug("Chargement de la carte pour le Coordinateur")
        resp_areas = userdetail.resp_areas.all()
        place_list = places.filter(
            Q(areas__in=resp_areas)
            | (
                (
                    ~Q(areas__in=resp_areas)
                    & (
                        Q(is_hidden=False)
                        | (
                            Q(is_hidden=True)
                            & (Q(created_by=loggeduser) | Q(authorized_user=loggeduser))
                        )
                    )
                )
            )
        )
    else:
        logger.debug("Chargement de la carte pour l'obs lambda")
        place_list = places.filter(
            Q(is_hidden=False) | (Q(is_hidden=True) & Q(authorized_user=loggeduser))
        )
    field = ("pk", "name")
    geojson = serialize("geojson", place_list, geometry_field="geom", fields=field)
    return HttpResponse(geojson, content_type="application/json")


@login_required
def places_in_metaplace_as_geojson(request, pk):
    """
    Vue permettant la génération d'un fichier geojson listant les localités que l'utilisateur est authorisé à voir

    :param request:
    :return:
    """
    loggeduser = request.user
    userdetail = get_user_model().objects.get(id=loggeduser.id)
    places = Place.objects.filter(metaplace=pk)
    if userdetail.access_all_data:
        logger.debug("Chargement de la carte pour l'utilisateur avec accès total")
        place_list = places
    elif userdetail.is_resp:
        logger.debug("Chargement de la carte pour le Coordinateur")
        resp_areas = userdetail.resp_areas.all()
        place_list = places.filter(
            Q(areas__in=resp_areas)
            | (
                (
                    ~Q(areas__in=resp_areas)
                    & (
                        Q(is_hidden=False)
                        | (
                            Q(is_hidden=True)
                            & (Q(created_by=loggeduser) | Q(authorized_user=loggeduser))
                        )
                    )
                )
            )
        )
    else:
        logger.debug("Chargement de la carte pour l'obs lambda")
        place_list = places.filter(
            Q(is_hidden=False) | (Q(is_hidden=True) & Q(authorized_user=loggeduser))
        )
    field = ("pk", "name")
    geojson = serialize("geojson", place_list, geometry_field="geom", fields=field)
    return HttpResponse(geojson, content_type="application/json")


class StudyAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated:
            return Study.objects.none()

        qs = Study.objects.all().order_by("name")

        if self.q:
            qs = qs.filter(name__icontains=self.q)

        return qs
