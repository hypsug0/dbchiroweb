# from django.contrib.auth.decorators import login_required
from django.urls import path

from .views import (  # GeoJSONTiledPlaceData,
    ActiveUserAutocomplete,
    AllUserAutocomplete,
    AreasAutocomplete,
    MetaplaceAutocomplete,
    PlaceAutocomplete,
    StudyAutocomplete,
    TaxaAutocomplete,
    metaplaces_as_geojson,
    places_as_geojson,
    places_in_metaplace_as_geojson,
)

app_name = "general_api"

urlpatterns = [
    path("place.geojson", places_as_geojson, name="geodata_place"),
    path("metaplaces.geojson", metaplaces_as_geojson, name="geodata_metaplaces"),
    path(
        "metaplaces/places.geojson",
        places_in_metaplace_as_geojson,
        name="geodata_places_in_metaplaces",
    ),
    path(
        "metaplaces/<int:pk>/places.geojson",
        places_in_metaplace_as_geojson,
        name="geodata_place_in_metaplace",
    ),
    # path(
    #     "place/<int:z>/<int:x>/<int:y>",
    #     GeoJSONTiledPlaceData.as_view(),
    #     name="placetiled",
    # ),
    path(
        "active-user-autocomplete/",
        ActiveUserAutocomplete.as_view(),
        name="active_user_autocomplete",
    ),
    path(
        "all-user-autocomplete/",
        AllUserAutocomplete.as_view(),
        name="all_user_autocomplete",
    ),
    path("taxa-autocomplete/", TaxaAutocomplete.as_view(), name="taxa_autocomplete"),
    path(
        "areas-autocomplete/",
        AreasAutocomplete.as_view(),
        name="areas_autocomplete",
    ),
    path("place-autocomplete/", PlaceAutocomplete.as_view(), name="place_autocomplete"),
    path(
        "metaplace-autocomplete/",
        MetaplaceAutocomplete.as_view(),
        name="metaplace_autocomplete",
    ),
    path(
        "v1/study/search",
        StudyAutocomplete.as_view(),
        name="study_autocomplete",
    ),
]
