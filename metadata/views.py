# Create your views here.

"""
    Vues de l'application Sights
"""

from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils.translation import gettext_lazy as _
from django.views.generic import DeleteView, DetailView, ListView
from django.views.generic.edit import CreateView, UpdateView

from metadata.forms import AcquisitionFrameworkForm, OrganismForm
from metadata.models import AcquisitionFramework, Organism

IMAGE_FILE_TYPES = ["png", "jpg", "jpeg"]
DOCUMENT_FILE_TYPES = ["doc", "docx", "odt", "pdf"]


class OrganismList(LoginRequiredMixin, ListView):
    model = Organism
    context_object_name = "organisms"
    template_name = "organism_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-eye fa-fw"
        context["title"] = _("Mise à jour d'un cadre d'acquisition")
        context["js"] = ""
        return context


class OrganismCreate(LoginRequiredMixin, CreateView):
    model = Organism
    template_name = "normal_form.html"
    form_class = OrganismForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-users fa-fw"
        context["title"] = _("Ajout d'un organisme")
        context["js"] = ""
        return context

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super().form_valid(form)


class OrganismUpdate(LoginRequiredMixin, UpdateView):
    model = Organism
    template_name = "normal_form.html"
    form_class = OrganismForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-users fa-fw"
        context["title"] = _("Modifier un organisme")
        context["js"] = ""
        return context

    def form_valid(self, form):
        form_valid = super().form_valid(form)
        form.instance.created_by = self.request.user
        return form_valid


class OrganismDetail(LoginRequiredMixin, DetailView):
    model = Organism
    template_name = "organism_detail.html"


class OrganismDelete(LoginRequiredMixin, DeleteView):
    model = Organism


class AcquisitionFrameworkCreate(LoginRequiredMixin, CreateView):
    model = AcquisitionFramework
    template_name = "normal_form.html"
    form_class = AcquisitionFrameworkForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-eye fa-fw"
        context["title"] = _("Ajout d'un cadre d'acquisition")
        context["js"] = ""
        return context


class AcquisitionFrameworkUpdate(LoginRequiredMixin, UpdateView):
    model = AcquisitionFramework
    form_class = AcquisitionFrameworkForm
    template_name = "normal_form.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-eye fa-fw"
        context["title"] = _("Mise à jour d'un cadre d'acquisition")
        context["js"] = ""
        return context


class AcquisitionFrameworkDelete(LoginRequiredMixin, DeleteView):
    model = AcquisitionFramework


class AcquisitionFrameworkDetail(LoginRequiredMixin, DetailView):
    model = AcquisitionFramework
    template_name = "acquisition_framework_detail.html"
