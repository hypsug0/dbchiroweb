import csv
import logging
from io import StringIO
from typing import Optional

import requests
from django.core.files.base import ContentFile

logger = logging.getLogger(__name__)


class Echo:
    """An object that implements just the write method of the file-like
    interface.
    """

    def write(self, value):
        """Write the value by returning it, instead of storing in a buffer."""
        return value


def iter_countdetail_items(items: list, pseudo_buffer: Echo, headers: list):
    writer = csv.writer(pseudo_buffer, delimiter="\t")
    yield writer.writerow(headers)

    for item in items:
        yield writer.writerow(
            [
                item.sighting.session.place.id_place,
                item.sighting.session.place.type.code
                if item.sighting.session.place.type
                else None,
                item.sighting.session.place.type.descr
                if item.sighting.session.place.type
                else None,
                item.sighting.session.place.name,
                item.sighting.session.place.x,
                item.sighting.session.place.y,
                item.sighting.session.contact.code if item.sighting.session.contact else None,
                item.sighting.session.date_start,
                item.sighting.session.main_observer,
                item.sighting.session.study.name if item.sighting.session.study else None,
                item.sighting.session.study.uuid if item.sighting.session.study else None,
                item.sighting.codesp.sp_true if item.sighting.codesp else None,
                item.sighting.codesp.codesp if item.sighting.codesp else None,
                item.sighting.codesp.sci_name if item.sighting.codesp else None,
                item.id_countdetail,
                item.method.code if item.method else None,
                item.time,
                item.count,
                item.unit.code if item.unit else None,
                item.precision.code if item.precision else None,
                item.device.ref if item.device else None,
                item.transmitter.reference if item.transmitter else None,
                item.ab,
                item.d5,
                item.d3,
                item.pouce,
                item.queue,
                item.tibia,
                item.pied,
                item.cm3,
                item.tragus,
                item.poids,
                item.testicule.code if item.testicule else None,
                item.epididyme.code if item.epididyme else None,
                item.tuniq_vag.code if item.tuniq_vag else None,
                item.gland_taille.code if item.gland_taille else None,
                item.gland_coul.code if item.gland_coul else None,
                item.mamelle.code if item.mamelle else None,
                item.epiphyse.code if item.epiphyse else None,
                item.chinspot.code if item.chinspot else None,
                item.usure_dent.code if item.usure_dent else None,
                item.etat_sexuel,
                item.usure_dent.code if item.usure_dent else None,
                item.comment,
            ]
        )


def generate_and_store_csv_export(queryset, headers):
    csv_buffer = StringIO()
    csv_writer = csv.writer(csv_buffer, delimiter="\t")
    csv_writer.writerow(headers)

    for item in queryset:
        csv_writer.writerow(
            [
                item.sighting.session.place.metaplace.id_metaplace
                if item.sighting.session.place.metaplace
                else None,
                item.sighting.session.place.metaplace.name
                if item.sighting.session.place.metaplace
                else None,
                (
                    item.sighting.session.place.metaplace.type.code
                    if item.sighting.session.place.metaplace.type
                    else None
                )
                if item.sighting.session.place.metaplace
                else None,
                item.sighting.session.place.metaplace.comment
                if item.sighting.session.place.metaplace
                else None,
                item.sighting.session.place.id_place,
                item.sighting.session.place.type.code
                if item.sighting.session.place.type
                else None,
                item.sighting.session.place.type.descr
                if item.sighting.session.place.type
                else None,
                item.sighting.session.place.name,
                item.sighting.session.place.x,
                item.sighting.session.place.y,
                item.sighting.session.place.altitude,
                item.sighting.session.place.comment,
                item.sighting.session.contact.code if item.sighting.session.contact else None,
                item.sighting.session.date_start,
                item.sighting.session.main_observer,
                item.sighting.session.study.name if item.sighting.session.study else None,
                item.sighting.session.study.uuid if item.sighting.session.study else None,
                item.sighting.session.comment,
                item.sighting.codesp.sp_true if item.sighting.codesp else None,
                item.sighting.codesp.codesp if item.sighting.codesp else None,
                item.sighting.codesp.sci_name if item.sighting.codesp else None,
                item.sighting.comment,
                item.id_countdetail,
                item.method.code if item.method else None,
                item.time,
                item.count,
                item.unit.code if item.unit else None,
                item.precision.code if item.precision else None,
                item.device.ref if item.device else None,
                item.transmitter.reference if item.transmitter else None,
                item.ab,
                item.d5,
                item.d3,
                item.pouce,
                item.queue,
                item.tibia,
                item.pied,
                item.cm3,
                item.tragus,
                item.poids,
                item.testicule.code if item.testicule else None,
                item.epididyme.code if item.epididyme else None,
                item.tuniq_vag.code if item.tuniq_vag else None,
                item.gland_taille.code if item.gland_taille else None,
                item.gland_coul.code if item.gland_coul else None,
                item.mamelle.code if item.mamelle else None,
                item.epiphyse.code if item.epiphyse else None,
                item.chinspot.code if item.chinspot else None,
                item.usure_dent.code if item.usure_dent else None,
                item.etat_sexuel,
                item.usure_dent.code if item.usure_dent else None,
                item.comment,
            ]
        )

    csv_file = ContentFile(csv_buffer.getvalue().encode("utf-8"))

    return csv_file


def get_altitude(x: float, y: float) -> Optional[int]:
    response = requests.get(
        f"https://wxs.ign.fr/calcul/alti/rest/elevation.json?lon={str(x)}&lat={str(y)}&zonly=true",
        timeout=5,
    )
    if response.status_code == 200:
        data = response.json()
        elevation_key = "elevations"
        if elevation_key in data:
            if isinstance(data[elevation_key], list):
                altitude = int(data[elevation_key][0])
                logger.debug(f"ALTITUDE IS {altitude}")
                return altitude if altitude > -20 else None
    return None
