from django.urls import path

from .api import EditSighting, GeoJSONSighting
from .views import SightingDetail, SightingEditForm, SightingSearch

urlpatterns = [
    # Sighting relative URLS
    # path(
    #     "session/<int:pk>/sighting/add",
    #     SightingCreate.as_view(),
    #     name="sighting_create",
    # ),
    path("sighting/<int:pk>/detail", SightingDetail.as_view(), name="sighting_detail"),
    # path("sighting/<int:pk>/update", SightingUpdate.as_view(), name="sighting_update"),
    # path("sighting/<int:pk>/delete", SightingDelete.as_view(), name="sighting_delete"),
    path("api/v1/search", GeoJSONSighting.as_view(), name="sighting_search_api"),
    path(
        "session/<int:pk>/sighting/edit",
        SightingEditForm.as_view(),
        name="sighting_edit_form",
    ),
    path(
        "api/v1/session/<int:session>/observation/list",
        EditSighting.as_view({"get": "list"}),
        name="count_detail_api",
    ),
    path(
        "api/v1/session/<int:session>/observation/add",
        EditSighting.as_view({"post": "create"}),
        name="post_observation_api",
    ),
    path(
        "api/v1/observation/<int:pk>/update",
        EditSighting.as_view({"put": "update"}),
        name="put_observation_api",
    ),
    path(
        "api/v1/observation/<int:pk>/delete",
        EditSighting.as_view({"delete": "destroy"}),
        name="delete_observation_api",
    ),
    path("sighting/search", SightingSearch.as_view(), name="sighting_search"),
]
