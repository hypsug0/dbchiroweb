"""
    Vues de l'application Sights
"""

from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.utils.translation import gettext_lazy as _
from django.views.generic import DetailView, TemplateView

from geodata.models import Areas
from sights.mixins import AdvancedUserViewMixin, SessionEditAuthMixin, SightingViewAuthMixin
from sights.models import CountDetail, Device, Session, Sighting
from sights.tables import (
    CountDetailAcousticTable,
    CountDetailBiomTable,
    CountDetailOtherTable,
    CountDetailTelemetryTable,
    SessionDeviceTable,
)

IMAGE_FILE_TYPES = ["png", "jpg", "jpeg"]
DOCUMENT_FILE_TYPES = ["doc", "docx", "odt", "pdf"]


class SightingDetail(SightingViewAuthMixin, DetailView):
    model = Sighting
    template_name = "sighting_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        pk = self.kwargs.get("pk")
        # Rendu des sessions
        context["icon"] = "fa fa-eye fa-fw"
        context["title"] = _("Détail d'une observation")
        context[
            "js"
        ] = """
        """
        # Rendu des observations
        context["countdetailicon"] = "fa fa-eye fa-fw"
        context["countdetailtitle"] = _("Détails de l'observation")
        context["countdetailcount"] = CountDetail.objects.filter(sighting=pk).distinct().count()
        if Sighting.objects.get(id_sighting=pk).session.contact.code:
            contact = Sighting.objects.get(id_sighting=pk).session.contact.code
            context["contact"] = contact
        else:
            contact = "nc"
        if contact in ("vm", "ca"):
            countdetailtable = CountDetailBiomTable(CountDetail.objects.filter(sighting=pk))
        elif contact == "du":
            countdetailtable = CountDetailAcousticTable(CountDetail.objects.filter(sighting=pk))
        elif contact == "te":
            countdetailtable = CountDetailTelemetryTable(CountDetail.objects.filter(sighting=pk))
        else:
            countdetailtable = CountDetailOtherTable(CountDetail.objects.filter(sighting=pk))
        context["countdetailtable"] = countdetailtable
        # Rendu des dispositifs
        context["deviceicon"] = "fi-target"
        context["devicetitle"] = _("Sessions d'inventaires")
        context["devicecount"] = Device.objects.filter(session=pk).count()
        context["devicetable"] = SessionDeviceTable(Device.objects.filter(session=pk))
        return context


class SightingMyList(LoginRequiredMixin, TemplateView):
    template_name = "sighting_search.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-map"
        context["title"] = _("Rechercher dans mes observations")
        context[
            "js"
        ] = """
        """
        context["only_mine"] = True
        return context


class SightingUserList(AdvancedUserViewMixin, LoginRequiredMixin, TemplateView):
    template_name = "sighting_search.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-map"
        context["title"] = _("Rechercher dans mes observations")
        context[
            "js"
        ] = """
        """
        context["userId"] = self.kwargs.get("pk")
        return context

    def get_queryset(self):
        user = get_user_model().objects.get(id=self.kwargs.get("pk"))
        loggeduser = get_user_model().objects.get(id=self.request.user.id)
        userlist = (
            Sighting.objects.filter(
                Q(created_by=user)
                | Q(observer=user)
                | Q(session__other_observer__username__contains=user.username)
            )
            .select_related("created_by")
            .select_related("updated_by")
            .select_related("observer")
            .select_related("session")
            .select_related("codesp")
            .prefetch_related("session__place")
            .prefetch_related("session__place__areas")
            .prefetch_related("session__place__type")
            .prefetch_related("session__contact")
            .prefetch_related("session__main_observer")
            .prefetch_related("session__other_observer")
            .prefetch_related("session__study")
        )
        if loggeduser.access_all_data or loggeduser.edit_all_data:
            return userlist.distinct().order_by("-timestamp_update")
        elif loggeduser.is_resp:
            return (
                userlist.filter(session__place__areas__in=loggeduser.resp_areas.all())
                .distinct()
                .order_by("-timestamp_update")
            )


class SightingSearch(LoginRequiredMixin, TemplateView):
    template_name = "sighting_search.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        enable_areas = Areas.objects.filter(coverage=True).all()
        context["enable_areas"] = enable_areas
        context["icon"] = "fa fa-fw fa-eye"
        context["title"] = _("Rechercher une observation")
        context[
            "js"
        ] = """
        """
        return context


class SightingEditForm(SessionEditAuthMixin, DetailView):
    model = Session
    template_name = "sighting_form.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-eye fa-fw"
        context["title"] = "Edition des observations"
        context["editMode"] = True
        return context
