# from rest_framework.serializers import SerializerMethodField
import logging

from drf_writable_nested.serializers import WritableNestedModelSerializer
from rest_framework_gis import serializers

from accounts.serializer import ProfileSearchSerializer
from dicts.models import Contact, Specie
from geodata.models import Areas
from sights.countdetail.serializers import CountDetailBaseSerializer
from sights.models import CountDetail, Session, Sighting
from sights.place.serializers import SimplePlaceSerializer

logger = logging.getLogger(__name__)


class SightingSpecie(serializers.ModelSerializer):
    class Meta:
        model = Specie
        fields = (
            "codesp",
            "sci_name",
            "common_name_fr",
            "sp_true",
        )


class AreasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Areas
        fields = ("area_type", "code", "name")


class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = (
            "descr",
            "code",
        )


class SessionSerializer(serializers.ModelSerializer):
    place_data = SimplePlaceSerializer(source="place")
    contact = ContactSerializer()
    main_observer = ProfileSearchSerializer()

    class Meta:
        model = Session
        fields = (
            "id_session",
            "name",
            "contact",
            "date_start",
            "place_data",
            "main_observer",
        )


class SightingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sighting
        depth = 1
        fields = (
            "id_sighting",
            "codesp",
            "created_by",
            "session",
            "breed_colo",
            "total_count",
            "period",
            "is_doubtful",
            "comment",
            "timestamp_create",
            "created_by",
            "updated_by",
        )


class GeoSightingSerializer(serializers.GeoFeatureModelSerializer):
    specie_data = SightingSpecie(source="codesp")
    session_data = SessionSerializer(source="session")
    creator = ProfileSearchSerializer(source="created_by")

    geom = serializers.GeometrySerializerMethodField()
    # countdetail = SerializerMethodField()

    class Meta(SightingSerializer.Meta):
        depth = 0
        geo_field = "geom"
        fields = SightingSerializer.Meta.fields + (
            "specie_data",
            "creator",
            "session_data",
        )

    def get_geom(self, sighting):
        return sighting.session.place.geom


class EditSightingListSerializer(serializers.ListSerializer):
    def create(self, validated_data):
        sightings = [Sighting(**item) for item in validated_data]
        return sightings


class EditSightingSerializer(SightingSerializer):
    """Sighting serializer for editing data (insert/update)
    from sighting and count details model"""

    countdetails = CountDetailBaseSerializer(many=True)

    class Meta:
        model = Sighting
        depth = 1
        fields = (
            "id_sighting",
            "session",
            "codesp",
            "breed_colo",
            "total_count",
            "observer",
            "is_doubtful",
            "comment",
            "extra_data",
            "countdetails",
            "created_by",
            "updated_by",
        )
        # list_serializer_class = EditSightingListSerializer

    def get_countdetails(self, sighting):
        """Retrieve all child count details

        Args:
            sighting (Sighting): Sighting

        Returns:
            list: A list of child count details
        """
        countdetails = CountDetail.objects.filter(sighting=sighting.id_sighting).all()
        return countdetails

    def create(self, validated_data):
        logger.debug(f"<EditSightingSerializer.create>\nself{self}")
        countdetails = validated_data.pop("countdetails")
        obj, created = self.Meta.model.objects.update_or_create(
            pk=validated_data.pop(self.Meta.model._meta.pk.name, None), defaults={**validated_data}
        )
        logger.debug(f"obj {obj.id_sighting} {obj}")
        logger.debug(f"created {created}")
        for countdetail in countdetails:
            countdetail["sighting"] = obj.id_sighting
            CountDetail.objects.create(**countdetail)
        return obj

    def update(self, instance, validated_data):
        countdetails_data = validated_data.pop("countdetails")
        # Unless the application properly enforces that this field is
        # always set, the following could raise a `DoesNotExist`, which
        # would need to be handled.
        countdetails = (instance.countdetails).all()
        countdetails = list(countdetails)
        logger.debug(f"validated_data {validated_data}")
        logger.debug(f"instance before {instance.updated_by}")

        instance.session = validated_data.get("session", instance.session)
        instance.codesp = validated_data.get("codesp", instance.codesp)
        instance.breed_colo = validated_data.get("breed_colo", instance.breed_colo)
        instance.total_count = validated_data.get("total_count", instance.total_count)
        instance.observer = validated_data.get("observer", instance.observer)
        instance.is_doubtful = validated_data.get("is_doubtful", instance.is_doubtful)
        instance.comment = validated_data.get("comment", instance.comment)
        instance.extra_data = validated_data.get("extra_data", instance.extra_data)
        instance.updated_by = validated_data.get("updated_by", instance.updated_by)
        instance.save()
        logger.debug(f"instance after {instance.updated_by}")
        for countdetail_data in countdetails_data:
            countdetail = countdetail_data.pop(0)
            countdetail.sighting = countdetail_data.get("sighting", instance.pk)
            countdetail.method = countdetail_data.get("method", countdetail.method)
            countdetail.sex = countdetail_data.get("sex", countdetail.sex)
            countdetail.age = countdetail_data.get("age", countdetail.age)
            countdetail.precision = countdetail_data.get("precision", countdetail.precision)
            countdetail.count = countdetail_data.get("count", countdetail.count)
            countdetail.unit = countdetail_data.get("unit", countdetail.unit)
            countdetail.save()

        return instance


class EditSightingSerializerTest(WritableNestedModelSerializer):
    countdetails = CountDetailBaseSerializer(many=True)

    class Meta:
        model = Sighting
        fields = (
            "id_sighting",
            "session",
            "codesp",
            "breed_colo",
            "total_count",
            "observer",
            "is_doubtful",
            "comment",
            "extra_data",
            "created_by",
            "updated_by",
            "countdetails",
        )

    def get_countdetails(self, sighting):
        """Retrieve all child count details

        Args:
            sighting (Sighting): Sighting

        Returns:
            list: A list of child count details
        """
        countdetails = CountDetail.objects.filter(sighting=sighting.id_sighting).all()
        return countdetails

    # def update(self, instance, validated_data):
    #     logger.debug(f"<update serializer> vd {validated_data}")
    #     logger.debug(f"<update serializer> instance {instance}")
    #     return super().update(self, instance, validated_data)
