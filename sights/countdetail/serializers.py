import logging

from rest_framework import serializers

from dicts.models import (
    Age,
    BiomChinspot,
    BiomDent,
    BiomEpiphyse,
    BiomEpipidyme,
    BiomGestation,
    BiomGlandCoul,
    BiomGlandTaille,
    BiomMamelle,
    BiomTesticule,
    BiomTuniqVag,
    CountPrecision,
    CountUnit,
    Method,
    Sex,
)
from sights.models import CountDetail

logger = logging.getLogger(__name__)

dict_fields = ("id", "code")

countdetail_fields = (
    "id_countdetail",
    "time",
    "method",
    "sex",
    "age",
    "precision",
    "count",
    "unit",
    "device",
    "manipulator",
    "validator",
    "transmitter",
    "ab",
    "d5",
    "pouce",
    "queue",
    "tibia",
    "pied",
    "cm3",
    "tragus",
    "poids",
    "testicule",
    "epididyme",
    "tuniq_vag",
    "gland_taille",
    "gland_coul",
    "mamelle",
    "gestation",
    "epiphyse",
    "chinspot",
    "etat_sexuel",
    "usure_dent",
    "comment",
)

# class DictBaseSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = None
#         fields = dict_fields = ("id", "code")


class AgeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Age
        fields = dict_fields


class BiomChinspotSerializer(serializers.ModelSerializer):
    class Meta:
        model = BiomChinspot
        fields = dict_fields


class BiomDentSerializer(serializers.ModelSerializer):
    class Meta:
        model = BiomDent
        fields = dict_fields


class BiomEpiphyseSerialized(serializers.ModelSerializer):
    class Meta:
        model = BiomEpiphyse
        fields = dict_fields


class BiomEpipidymeSerialized(serializers.ModelSerializer):
    class Meta:
        model = BiomEpipidyme
        fields = dict_fields


class BiomGestationSerialized(serializers.ModelSerializer):
    class Meta:
        model = BiomGestation
        fields = dict_fields


class BiomGlandCoulSerialized(serializers.ModelSerializer):
    class Meta:
        model = BiomGlandCoul
        fields = dict_fields


class BiomGlandTailleSerialized(serializers.ModelSerializer):
    class Meta:
        model = BiomGlandTaille
        fields = dict_fields


class BiomMamelleSerialized(serializers.ModelSerializer):
    class Meta:
        model = BiomMamelle
        fields = dict_fields


class BiomTesticuleSerialized(serializers.ModelSerializer):
    class Meta:
        model = BiomTesticule
        fields = dict_fields


class BiomTuniqVagSerialized(serializers.ModelSerializer):
    class Meta:
        model = BiomTuniqVag
        fields = dict_fields


class CountPrecisionSerialized(serializers.ModelSerializer):
    class Meta:
        model = CountPrecision
        fields = dict_fields


class CountUnitSerialized(serializers.ModelSerializer):
    class Meta:
        model = CountUnit
        fields = dict_fields


class MethodSerialized(serializers.ModelSerializer):
    class Meta:
        model = Method
        fields = dict_fields


class SexSerialized(serializers.ModelSerializer):
    class Meta:
        model = Sex
        fields = dict_fields


class CountDetailListSerializer(serializers.ListSerializer):
    def create(self, validated_data):
        logger.debug(f"CountDetailListSerializer vdata {validated_data}")

        countdetails = [CountDetail(**item) for item in validated_data]
        return CountDetail.objects.bulk_create(countdetails)


class CountDetailBaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = CountDetail
        fields = countdetail_fields


class CountDetailSerializer(CountDetailBaseSerializer):
    taxa = serializers.SerializerMethodField()
    session = serializers.SerializerMethodField()

    class Meta(CountDetailBaseSerializer.Meta):
        fields = CountDetailBaseSerializer.Meta.fields + (
            "taxa",
            "session",
        )

    def get_taxa(self, obj):
        return obj.sighting.codesp.pk

    def get_session(self, obj):
        return obj.sighting.session.pk

    def create(self):
        logger.debug("CREATE")
