import datetime
import logging
import threading

from django.contrib.auth.mixins import LoginRequiredMixin
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from exports.models import Exports
from exports.serializers import ExportsSerializer
from sights.mixins import countdetail_filtering, countdetail_list_permission
from sights.models import CountDetail
from sights.utils import generate_and_store_csv_export

# Create your views here.
logger = logging.getLogger(__name__)

fieldnames = [
    "id_metaplace",
    "metaplace_name",
    "metaplace_type_code",
    "metaplace_comment",
    "id_place",
    "place_type_code",
    "place_type_descr",
    "place_name",
    "place_x",
    "place_y",
    "place_altitude",
    "place_comment",
    "session_contact",
    "session_date_start",
    "session_is_comprehensive",
    "session_main_observer",
    "session_study_name",
    "session_study_uuid",
    "session_comment",
    "observation_sp_true",
    "observation_codesp",
    "observation_sci_name",
    "observation_comment",
    "id_countdetail",
    "countdetail_method",
    "countdetail_time",
    "countdetail_count",
    "countdetail_unit",
    "countdetail_precision",
    "countdetail_device",
    "countdetail_transmitter",
    "countdetail_sex",
    "countdetail_age",
    "countdetail_bio_status",
    "countdetail_behaviour",
    "countdetail_ab",
    "countdetail_d5",
    "countdetail_d3",
    "countdetail_pouce",
    "countdetail_queue",
    "countdetail_tibia",
    "countdetail_pied",
    "countdetail_cm3",
    "countdetail_tragus",
    "countdetail_poids",
    "countdetail_testicule",
    "countdetail_epididyme",
    "countdetail_tuniq_vag",
    "countdetail_gland_taille",
    "countdetail_gland_coul",
    "countdetail_mamelle",
    "countdetail_epiphyse",
    "countdetail_chinspot",
    "countdetail_usure_dent",
    "countdetail_etat_sexuel",
    "countdetail_usure_dent",
    "countdetail_manipulator",
    "countdetail_comment",
]

export_qs = (
    CountDetail.objects.all()
    .select_related("sighting")
    .select_related("sighting__session")
    .select_related("method")
    .select_related("unit")
    .select_related("device")
    .select_related("unit")
    .select_related("precision")
    .select_related("transmitter")
    .select_related("sex")
    .select_related("age")
    .select_related("bio_status")
    .select_related("testicule")
    .select_related("epididyme")
    .select_related("tuniq_vag")
    .select_related("gland_taille")
    .select_related("mamelle")
    .select_related("gestation")
    .select_related("epiphyse")
    .select_related("chinspot")
    .select_related("usure_dent")
    .select_related("gland_coul")
    .select_related("sighting__codesp")
    .select_related("sighting__session__place")
    .select_related("sighting__session__place__type")
    .select_related("sighting__session__study")
    .select_related("sighting__session__contact")
    .select_related("sighting__session__place__metaplace")
    .select_related("sighting__session__place__metaplace__type")
    .select_related("sighting__session__main_observer")
)


def gen_export(user, instance, query_params):
    export = instance
    csv_qs = countdetail_list_permission(user, export_qs)
    csv_qs = countdetail_filtering(user, csv_qs, query_params)
    csv_file = generate_and_store_csv_export(csv_qs, fieldnames)
    t = datetime.datetime.now()
    timestamp = t.strftime("%Y%m%d_%H%M")
    export.export_file.save(f"export_{timestamp}_{user.username}.tsv", csv_file)
    export.ready = True
    export.save()
    export.notify()


class CountDetailExport(LoginRequiredMixin, GenericAPIView):
    queryset = Exports.objects.all()
    serializer_class = ExportsSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer_class()
        try:
            user = request.user
            export = Exports(created_by=user, search_qs=self.request.GET.dict())
            export.save()
            logger.debug(f"EXPORT ID {export.id}")
            t = threading.Thread(target=gen_export, args=(user, export, self.request.GET))
            t.setDaemon(True)
            t.start()

        except Exception as e:
            logger.error(f"Exception type {e}")
            # return HttpResponse(f"Error: {e}")

        return Response(serializer(export).data)
