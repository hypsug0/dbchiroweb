# import the logging library
import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Count
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework_gis.filters import InBBoxFilter

from core.mixins import SmallResultsSetPagination
from sights.mixins import LargeGeoJsonPageNumberPagination, MetaplaceFilteringMixin
from sights.models import Metaplace

from .serializers import (
    MetaplaceFullGeoSerializer,
    MetaplaceLightGeoSerializer,
    MetaplaceLightSerializer,
    MetaplaceMediumGeoSerializer,
)

logger = logging.getLogger(__name__)

queryset = (
    Metaplace.objects.select_related("type")
    .select_related("created_by", "updated_by", "type")
    .prefetch_related("places")
    .prefetch_related("areas")
    .prefetch_related("areas__area_type")
    .annotate(count_places=Count("places", distinct=True))
)


class GenericMetaplaceSearch(LoginRequiredMixin, MetaplaceFilteringMixin, ListAPIView):
    queryset = queryset
    bbox_filter_include_overlapping = True  # Optional
    # filter_fields = ["name", "comment", "type"]
    filter_backends = (SearchFilter, DjangoFilterBackend, InBBoxFilter)
    search_fields = ["@name", "@comment", "type__code", "@type__descr", "@type__category"]
    permission_classes = [
        IsAuthenticated,
    ]


class MetaplaceGeoListApi(GenericMetaplaceSearch):
    # serializer_class = SimpleMetaplaceGeoSerializer
    pagination_class = LargeGeoJsonPageNumberPagination
    bbox_filter_field = "geom"
    filter_backends = [
        InBBoxFilter,
    ]
    bbox_filter_include_overlapping = True  # Optional

    def paginate_queryset(self, queryset, view=None):
        if "no_page" in self.request.query_params:
            return None
        else:
            return self.paginator.paginate_queryset(queryset, self.request, view=self)

    def get_serializer_class(self):
        levels = {
            "light": MetaplaceLightGeoSerializer,
            "medium": MetaplaceMediumGeoSerializer,
            "high": MetaplaceFullGeoSerializer,
        }
        detail_level = self.request.query_params.get("detail_level", "high")
        return levels[detail_level]


class MetaplaceListApi(GenericMetaplaceSearch):
    serializer_class = MetaplaceLightSerializer
    # queryset = Metaplace.objects.all().select_related("type").all()
    pagination_class = SmallResultsSetPagination
    filter_backends = (
        SearchFilter,
        DjangoFilterBackend,
    )
    # search_fields = ["name", "comment", "type"]
