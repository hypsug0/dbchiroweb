from rest_framework.serializers import IntegerField, ModelSerializer
from rest_framework_gis.serializers import GeoFeatureModelSerializer

from accounts.models import Profile
from dicts.models import MetaplaceType

from ..models import Metaplace, Place


class MetaplaceTypeSerializer(ModelSerializer):
    class Meta:
        model = MetaplaceType
        fields = ("id", "category", "descr")


class MetaplaceUserSerializer(ModelSerializer):
    class Meta:
        model = Profile
        fields = ("full_name",)


class MetaplaceTypeSimpleSerializer(ModelSerializer):
    class Meta:
        model = MetaplaceType
        fields = ("code", "category", "descr")


class VerySimplePlaceSerializer(ModelSerializer):
    class Meta:
        model = Place
        fields = ("id_place", "name")


class MetaplaceLightSerializer(ModelSerializer):
    type = MetaplaceTypeSimpleSerializer()

    class Meta:
        model = Metaplace
        fields = ("id_metaplace", "type", "name")


class MetaplaceLightGeoSerializer(GeoFeatureModelSerializer):
    type = MetaplaceTypeSimpleSerializer()

    class Meta:
        model = Metaplace
        geo_field = "geom"
        depth = 1
        fields = ("id_metaplace", "type", "name")


class MetaplaceMediumGeoSerializer(GeoFeatureModelSerializer):
    type = MetaplaceTypeSimpleSerializer()
    created_by = MetaplaceUserSerializer()
    count_places = IntegerField()

    class Meta:
        model = Metaplace
        depth = 1
        geo_field = "geom"
        fields = ("id_metaplace", "name", "type", "created_by", "count_places")


class MetaplaceFullGeoSerializer(GeoFeatureModelSerializer):
    type = MetaplaceTypeSimpleSerializer()
    created_by = MetaplaceUserSerializer()
    count_places = IntegerField()

    class Meta:
        model = Metaplace
        depth = 1
        geo_field = "geom"
        fields = (
            "id_metaplace",
            "name",
            "type",
            "comment",
            "extra_data",
            "created_by",
            "count_places",
            "timestamp_create",
            "timestamp_update",
        )
