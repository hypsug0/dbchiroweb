from django.urls import path

from sights.metaplace.api import MetaplaceGeoListApi, MetaplaceListApi
from sights.metaplace.views import (
    MetaplaceCreate,
    MetaplaceDelete,
    MetaplaceDetail,
    MetaplaceList,
    MetaplaceUpdate,
)

urlpatterns = [
    path("metaplace/add", MetaplaceCreate.as_view(), name="metaplace_create"),
    path("metaplace/", MetaplaceList.as_view(), name="metaplace_search"),
    path("metaplace/<int:pk>/detail", MetaplaceDetail.as_view(), name="metaplace_detail"),
    path("metaplace/<int:pk>/update", MetaplaceUpdate.as_view(), name="metaplace_update"),
    path("metaplace/<int:pk>/delete", MetaplaceDelete.as_view(), name="metaplace_delete"),
    path(
        "api/v1/metaplace/geosearch",
        MetaplaceGeoListApi.as_view(),
        name="metaplace_geosearch_api",
    ),
    path(
        "api/v1/metaplace",
        MetaplaceListApi.as_view(),
        name="metaplace_search_api",
    ),
]
