from django.urls import include, path

from sights.countdetail import urls as countdetail_urls
from sights.metaplace import urls as metaplace_urls
from sights.observation import urls as observation_urls
from sights.observation.views import SightingMyList, SightingUserList
from sights.place import urls as place_urls
from sights.place.views import PlaceMyList
from sights.session import urls as session_urls
from sights.session.views import SessionMyList

app_name = "sights"

urlpatterns = [
    path("user/place/list", PlaceMyList.as_view(), name="place_mylist"),
    path("user/session/list", SessionMyList.as_view(), name="session_mylist"),
    path("user/sighting/list", SightingMyList.as_view(), name="sighting_mylist"),
    path(
        "user/<int:pk>/sighting/list",
        SightingUserList.as_view(),
        name="user_sighting_list",
    ),
    path("", include(metaplace_urls)),
    path("", include(place_urls)),
    path("", include(session_urls)),
    path("", include(observation_urls)),
    path("", include(countdetail_urls)),
]
