"""
    Mixins du projet
"""

import datetime
import logging
from collections import OrderedDict

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import CharField, Q
from django.db.models.functions import Lower
from django.shortcuts import redirect
from django.urls import reverse
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

# Get an instance of a logger
logger = logging.getLogger(__name__)
CharField.register_lookup(Lower)


def get_study_confidentiality(session, user):
    study = session.study
    if study is not None:
        confidentiality = (
            study.project_manager == user
            or not study.confidential
            or (
                study.confidential and study.confidential_end_date < datetime.datetime.now().date()
            )
        )
        return confidentiality
    return True


class AdvancedUserViewMixin(LoginRequiredMixin):
    """
    Classe mixin de vérification que l'utilisateur possède les droits de modifier la donnée:
    Soit il est créateur de la donnée,
    Soit il dispose de l'authorisation de modifier toutes les données ( :get_user_model():access_all_data is True)
    """

    def dispatch(self, request, *args, **kwargs):
        user = request.user
        if not user.is_authenticated:
            return redirect(f"{reverse('auth_login')}?next={request.path}")
        if user.edit_all_data or user.access_all_data or user.is_resp or user.is_superuser:
            return super().dispatch(request, *args, **kwargs)
        return redirect("core:update_unauth")


class PlaceViewAuthMixin(LoginRequiredMixin):
    """
    Classe mixin de vérification que l'utilisateur possède les droits de modifier la donnée:
    Soit il est créateur de la donnée,
    Soit il dispose de l'authorisation de modifier toutes les données ( :get_user_model():access_all_data is True)
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        user = request.user
        if not user.is_authenticated:
            return redirect(f"{reverse('auth_login')}?next={request.path}")
        place_areas = self.obj.areas.all()
        if (
            user.edit_all_data
            or user.is_superuser
            or self.obj.created_by == user
            or (self.obj.is_hidden and user in self.obj.authorized_user.all())
            or (user.is_resp and any(x in user.resp_areas.all() for x in place_areas))
            or not self.obj.is_hidden
        ):
            return super().dispatch(request, *args, **kwargs)
        return redirect("core:view_unauth")


class PlaceEditAuthMixin:
    """
    Permission d'éditer la localité dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        L'utilisateur peut modifier Toutes les données,
        L'utilisateur est responsable du territoire de la localité,
        L'utilisateur est créateur de la localité.
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        user = request.user
        if not user.is_authenticated:
            return redirect(f"{reverse('auth_login')}?next={request.path}")
        place_areas = self.obj.areas.all()
        if (
            user.is_superuser
            or user.edit_all_data
            or (user.is_resp and any(x in user.resp_areas.all() for x in place_areas))
            or self.obj.created_by == user
        ):
            return super().dispatch(request, *args, **kwargs)
        return redirect("core:update_unauth")


class PlaceDetailViewAuthMixin:
    """
    Permission d'éditer le détail de la localité dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        L'utilisateur peut modifier Toutes les données,
        L'utilisateur est responsable du territoire de la localité,
        L'utilisateur est créateur de la localité.
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        user = request.user
        if not user.is_authenticated:
            return redirect(f"{reverse('auth_login')}?next={request.path}")
        place_areas = self.obj.place.areas.all()
        if (
            user.is_superuser
            or user.edit_all_data
            or user.access_all_data
            or (user.is_resp and any(x in user.resp_areas.all() for x in place_areas))
            or self.obj.created_by == user
        ):
            return super().dispatch(request, *args, **kwargs)
        return redirect("core:view_unauth")


class PlaceDetailEditAuthMixin:
    """
    Permission d'éditer le détail de la localité dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        L'utilisateur peut modifier Toutes les données,
        L'utilisateur est responsable du territoire de la localité,
        L'utilisateur est créateur de la localité.
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        user = request.user
        if not user.is_authenticated:
            return redirect(f"{reverse('auth_login')}?next={request.path}")
        place_areas = self.obj.place.areas.all()
        if (
            user.is_superuser
            or user.edit_all_data
            or (user.is_resp and any(x in user.resp_areas.all() for x in place_areas))
            or self.obj.created_by == user
        ):
            return super().dispatch(request, *args, **kwargs)
        return redirect("core:update_unauth")


class TreeGiteViewAuthMixin:
    """
    Permission d'éditer le détail de la localité dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        L'utilisateur peut modifier Toutes les données,
        L'utilisateur est responsable du territoire de la localité,
        L'utilisateur est créateur de la donnée.
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        user = request.user
        if not user.is_authenticated:
            return redirect(f"{reverse('auth_login')}?next={request.path}")
        place_areas = self.obj.tree.place.areas.all()
        if (
            user.is_superuser
            or user.edit_all_data
            or user.access_all_data
            or (user.is_resp and any(x in user.resp_areas.all() for x in place_areas))
            or self.obj.created_by == user
        ):
            return super().dispatch(request, *args, **kwargs)
        return redirect("core:view_unauth")


class TreeGiteEditAuthMixin:
    """
    Permission d'éditer le détail de la localité dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        L'utilisateur peut modifier Toutes les données,
        L'utilisateur est responsable du territoire de la localité,
        L'utilisateur est créateur de la donnée.
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        user = request.user
        if not user.is_authenticated:
            return redirect(f"{reverse('auth_login')}?next={request.path}")
        place_areas = self.obj.tree.place.areas.all()
        if (
            user.is_superuser
            or user.edit_all_data
            or (user.is_resp and any(x in user.resp_areas.all() for x in place_areas))
            or self.obj.created_by == user
        ):
            return super().dispatch(request, *args, **kwargs)
        return redirect("core:update_unauth")


class SessionViewAuthMixin:
    """
    Permet de voir la session dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        L'utilisateur peut voir toutes les données,
        L'utilisateur est responsable du territoire de la localité de cette session,
        L'utilisateur est créateur de la session.
    """

    def dispatch(self, request, *args, **kwargs):
        print("SessionViewAuthMixin test")
        self.obj = self.get_object()
        user = request.user
        if not request.user.is_authenticated:
            return redirect(f"{reverse('auth_login')}?next={request.path}")
        place_areas = self.obj.place.areas.all()
        if (
            user.is_superuser
            or user.edit_all_data
            or user.access_all_data
            or (user.is_resp and any(x in user.resp_areas.all() for x in place_areas))
            or self.obj.created_by == user
            or self.obj.main_observer == user
            or user in self.obj.other_observer.all()
            or (not self.obj.is_confidential and get_study_confidentiality(self.obj, user))
        ):
            return super().dispatch(request, *args, **kwargs)
        return redirect("core:view_unauth")


class SessionEditAuthMixin:
    """
    Permet de voir la session dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        ou L'utilisateur peut voir toutes les données,
        ou L'utilisateur est responsable du territoire de la localité de cette session,
        ou L'utilisateur est créateur de la session
        ou L'utilisateur est l'observateur principal de la session
        ou L'utilisateur est l'un des autres observateurs de la session
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        user = request.user
        if not user.is_authenticated:
            return redirect(f"{reverse('auth_login')}?next={request.path}")
        place_areas = self.obj.place.areas.all()
        if (
            user.is_superuser
            or user.edit_all_data
            or (user.is_resp and any(x in user.resp_areas.all() for x in place_areas))
            or self.obj.created_by == user
            or self.obj.main_observer == user
            or user in self.obj.other_observer.all()
        ):
            return super().dispatch(request, *args, **kwargs)
        return redirect("core:update_unauth")


class SightingViewAuthMixin:
    """
    Permet de voir l'observation dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        L'utilisateur peut voir toutes les données,
        L'utilisateur est responsable du territoire de la localité de cette session,
        L'utilisateur est créateur de la session de la donnée.
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        user = request.user
        if not user.is_authenticated:
            return redirect(f"{reverse('auth_login')}?next={request.path}")
        place_areas = self.obj.session.place.areas.all()
        if (
            user.is_superuser
            or user.edit_all_data
            or user.access_all_data
            or (user.is_resp and any(x in user.resp_areas.all() for x in place_areas))
            or self.obj.created_by == user
            or self.obj.session.main_observer == user
            or user in self.obj.session.other_observer.all()
            or (
                settings.SEE_ALL_NON_SENSITIVE_DATA
                and (
                    not self.obj.session.place.is_hidden
                    or user in self.obj.session.place.authorized_user.all()
                )
                and (
                    not self.obj.session.is_confidential
                    and get_study_confidentiality(self.obj.session, user)
                )
            )
        ):
            return super().dispatch(request, *args, **kwargs)
        return redirect("core:view_unauth")


class SightingEditAuthMixin:
    """
    Permet de voir l'observation dans les conditions suivantes:
        L'utilisateur est superutilisateur  :get_user_model():is_superuser is True,
        L'utilisateur peut voir toutes les données :get_user_model():edit_all_data is True,
        L'utilisateur est responsable du territoire de la localité de cette session :get_user_model():is_resp is True & territory in :get_user_model():resp_areas,
        L'utilisateur est créateur de la session de la donnée
        L'utilisateur est l'observateur principal de la session
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        user = request.user
        if not request.user.is_authenticated:
            return redirect(f"{reverse('auth_login')}?next={request.path}")
        place_areas = self.obj.session.place.areas.all()
        if (
            user.is_superuser
            or user.edit_all_data
            or (user.is_resp and any(x in user.resp_areas.all() for x in place_areas))
            or self.obj.created_by == user
            or self.obj.session.main_observer == user
        ):
            return super().dispatch(request, *args, **kwargs)
        return redirect("core:update_unauth")


class CountDetailViewAuthMixin:
    """
    Permet de voir l'observation dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        L'utilisateur peut voir toutes les données,
        L'utilisateur est responsable du territoire de la localité de cette session,
        L'utilisateur est créateur de la session de la donnée
        L'utilisateur est l'un des observateurs de la session
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        user = request.user
        if not user.is_authenticated:
            return redirect(f"{reverse('auth_login')}?next={request.path}")
        place_areas = self.obj.sighting.session.place.areas.all()
        if (
            user.is_superuser
            or user.edit_all_data
            or user.access_all_data
            or (user.is_resp and any(x in user.resp_areas.all() for x in place_areas))
            or self.obj.created_by == user
            or self.obj.sighting.session.main_observer == user
            or user in self.obj.sighting.session.other_observer.all()
            or (
                settings.SEE_ALL_NON_SENSITIVE_DATA
                and (
                    not self.obj.sighting.session.place.is_hidden
                    or user in self.obj.sighting.session.place.authorized_user.all()
                )
                and (
                    not self.obj.sighting.session.is_confidential
                    and get_study_confidentiality(self.obj.sighting.session, user)
                )
            )
        ):
            return super().dispatch(request, *args, **kwargs)
        return redirect("core:view_unauth")


class CountDetailEditAuthMixin:
    """
    Permet de voir l'observation dans les conditions suivantes:
        L'utilisateur est superutilisateur  :get_user_model():is_superuser is True,
        L'utilisateur peut voir toutes les données :get_user_model():edit_all_data is True,
        L'utilisateur est responsable du territoire de la localité de cette session :get_user_model():is_resp is True & territory in :get_user_model():resp_areas,
        L'utilisateur est créateur de la session de la donnée
        L'utilisateur est l'observateur principal de la session
    """

    def dispatch(self, request, *args, **kwargs):
        self.obj = self.get_object()
        user = request.user
        if not request.user.is_authenticated:
            return redirect(f"{reverse('auth_login')}?next={request.path}")
        place_areas = self.obj.sighting.session.place.areas.all()
        if (
            user.is_superuser
            or user.edit_all_data
            or (user.is_resp and any(x in user.resp_areas.all() for x in place_areas))
            or self.obj.created_by == user
            or self.obj.sighting.session.main_observer == user
        ):
            return super().dispatch(request, *args, **kwargs)
        return redirect("core:update_unauth")


class LargeGeoJsonPageNumberPagination(PageNumberPagination):
    page_size = 100
    page_size_query_param = "page_size"
    max_page_size = 5000

    def get_paginated_response(self, data):
        if self.request.query_params.get("format", "json") == "geojson":
            return Response(
                OrderedDict(
                    [
                        ("type", "FeatureCollection"),
                        ("count", self.page.paginator.count),
                        ("next", self.get_next_link()),
                        ("previous", self.get_previous_link()),
                        ("features", data["features"]),
                    ]
                )
            )
        return super().get_paginated_response(data)

    def paginate_queryset(self, queryset, request, view=None):
        logger.debug(f"query_params {type(request.query_params)}")
        if "no_page" in request.query_params:
            return None
        return super().paginate_queryset(queryset, request, view)


# class LargeGeoJsonPageNumberPagination(GeoJsonPagination):
#     page_size = 100
#     page_size_query_param = "page_size"
#     max_page_size = 5000


class MetaplaceFilteringMixin:
    """Mixin used for metaplace lists filtering"""

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset()

        name = self.request.query_params.get("name", None)
        type = self.request.query_params.get("type", None)
        creator = self.request.query_params.get("creator", None)
        all = self.request.query_params.get("all", None)
        area = self.request.query_params.get("area", None)

        qs = qs.order_by("-timestamp_create")
        qs = qs.filter(areas=area) if area is not None else qs
        qs = qs.filter(name__icontains=name) if name else qs
        qs = qs.filter(type=type) if type else qs
        qs = qs.filter(Q(created_by=creator) | Q(updated_by=creator)) if creator else qs
        qs = qs if all else qs.filter(places__isnull=False)

        return qs


class PlaceFilteringMixin:
    """Mixin used for Place lists filtering"""

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset()

        name = self.request.query_params.get("name", None)
        place_type = self.request.query_params.get("type", None)
        alt_min = self.request.query_params.get("alt_min", None)
        alt_max = self.request.query_params.get("alt_max", None)
        gite = self.request.query_params.get("gite", None)
        creator = self.request.query_params.get("creator", None)
        created_by_id = self.request.query_params.get("created_by_id", None)
        only_mine = self.request.query_params.get("only_mine", None)
        metaplace = self.request.query_params.get("metaplace", None)
        convention = self.request.query_params.get("convention", None)
        managed = self.request.query_params.get("managed", None)
        hidden = self.request.query_params.get("hidden", None)
        area = self.request.query_params.get("area", None)

        qs = qs.filter(areas=area) if area is not None else qs
        qs = qs.filter(name__unaccent__icontains=name) if name is not None else qs
        qs = qs.filter(type=place_type) if place_type is not None else qs
        qs = qs.filter(altitude__gte=alt_min) if alt_min is not None else qs
        qs = qs.filter(altitude__lte=alt_max) if alt_max is not None else qs
        qs = qs.filter(created_by__username__icontains=creator) if creator is not None else qs
        if created_by_id is not None:
            qs = qs.filter(created_by=created_by_id)
        if only_mine is not None:
            qs = qs.filter(created_by=self.request.user.id)
        qs = qs.filter(is_gite=True) if gite is not None else qs
        qs = qs.filter(metaplace__pk=metaplace) if metaplace is not None else qs
        qs = qs.filter(convention=True) if convention is not None else qs
        qs = qs.filter(is_managed=True) if managed is not None else qs
        qs = qs.filter(is_hidden=True) if hidden is not None else qs
        qs = (
            qs.filter(
                Q(created_by__first_name__icontains=creator)
                | Q(created_by__last_name__icontains=creator)
                | Q(created_by__username__icontains=creator)
            )
            if creator is not None
            else qs
        )
        logger.debug("QS %s", qs)
        return qs


class PlaceListPermissionsMixin:
    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset()

        user = self.request.user
        if user.access_all_data or user.edit_all_data or user.is_superuser:
            qs = qs

        # Right access
        else:
            qs = (
                qs.filter(
                    Q(created_by=user)
                    | Q(areas__in=user.resp_areas.all())
                    | Q(Q(is_hidden=False) | Q(authorized_user=user))
                )
                .distinct()
                .order_by("-timestamp_update")
            )

        return qs


class PlaceFilteredListWithPermissions(
    PlaceListPermissionsMixin,
    PlaceFilteringMixin,
):
    pass


class PlaceActorsListPermissionsMixin(object):
    """Mixin used for Sighting lists permissions
    Any user can see all non sensitive data

    """

    def get_queryset(self, *args, **kwargs):
        queryset = super().get_queryset()

        user = self.request.user

        if user.access_all_data or user.edit_all_data or user.is_superuser:
            logger.debug("ADVANCED USER")
            return queryset
        if user.is_resp:
            logger.debug("RESP USER")
            queryset = queryset.filter(
                Q(place__areas__in=user.resp_areas.all())
                | Q(Q(place__is_hidden=False) | Q(place__authorized_user=user))
            )
        else:
            logger.debug("OTHER SITUATION")
            queryset = queryset.filter(Q(place__is_hidden=False) | Q(place__authorized_user=user))

        return queryset


class PlaceActorsFilteringMixin(object):
    """Mixin used for Sighting lists permissions
    Any user can see all non sensitive data

    """

    def get_queryset(self, *args, **kwargs):
        queryset = super().get_queryset()
        # qs = Sighting.objects
        place = self.request.query_params.getlist("place", None)
        queryset = queryset.filter(place__in=place) if place else queryset

        return queryset


class SessionFilteringMixin:
    """Mixin used for Sighting lists filtering"""

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset()
        # qs = Sighting.objects
        place = self.request.query_params.getlist("place", None)
        area = self.request.query_params.getlist("area", None)
        specie = self.request.query_params.getlist("specie", None)
        breed_colo = self.request.query_params.getlist("breed_colo", None)
        count_min = self.request.query_params.get("count_min", None)
        count_max = self.request.query_params.get("count_max", None)
        place_type = self.request.query_params.getlist("place_type", None)
        contact = self.request.query_params.getlist("contact", None)
        alt_min = self.request.query_params.get("alt_min", None)
        alt_max = self.request.query_params.get("alt_max", None)
        gite = self.request.query_params.get("gite", None)
        creator = self.request.query_params.get("creator", None)
        metaplace = self.request.query_params.getlist("metaplace", None)
        date_min = self.request.query_params.get("date_min", None)
        date_max = self.request.query_params.get("date_max", None)
        only_mine = self.request.query_params.get("only_mine", None)
        user = self.request.query_params.get("user", None)
        study = self.request.query_params.getlist("study", None)
        period = self.request.query_params.getlist("period", None)
        organism = self.request.query_params.get("organism", None)
        is_comprehensive = self.request.query_params.get("is_comprehensive", None)

        qs = qs.filter(place__areas__in=area) if area else qs
        qs = qs.filter(place__in=place) if place else qs
        qs = qs.filter(place__type__in=place_type) if place_type else qs
        qs = qs.filter(contact__in=contact) if contact else qs
        qs = qs.filter(place__altitude__gte=alt_min) if alt_min is not None else qs
        qs = qs.filter(place__altitude__lte=alt_max) if alt_max is not None else qs
        qs = qs.filter(created_by=creator) if creator is not None else qs
        qs = (
            qs.filter(Q(created_by=user) | Q(main_observer=user) | Q(other_observer=user))
            if user is not None
            else qs
        )
        qs = (
            qs.filter(
                Q(created_by=self.request.user.id)
                | Q(main_observer=self.request.user.id)
                | Q(other_observer=self.request.user.id)
            )
            if only_mine is not None
            else qs
        )
        qs = qs.filter(place__is_gite=True) if gite is not None else qs
        qs = qs.filter(place__metaplace__in=metaplace) if metaplace else qs
        qs = qs.filter(study__in=study) if study else qs
        qs = qs.filter(observations__codesp__in=specie) if specie else qs
        qs = qs.filter(observations__breed_colo=True) if breed_colo else qs
        qs = qs.filter(observations__period__in=period) if period else qs
        qs = qs.filter(observations__total_count__gte=count_min) if count_min is not None else qs
        qs = qs.filter(observations__total_count__lte=count_max) if count_max is not None else qs
        qs = (
            qs.filter(Q(date_start__gte=date_min) | Q(date_end__gte=date_min))
            if date_min is not None
            else qs
        )
        qs = (
            qs.filter(Q(date_start__lte=date_max) | Q(date_end__lte=date_max))
            if date_max is not None
            else qs
        )
        # TODO: Manage organism on metadata when available
        qs = qs.filter(Q(main_observer__organisms=organism)) if organism is not None else qs
        if is_comprehensive:
            if is_comprehensive.lower() in ("1", "true", "yes"):
                qs = qs.filter(is_comprehensive=True)
            if is_comprehensive.lower() in ("0", "false", "no"):
                qs = qs.filter(is_comprehensive=False)

        return qs


class SessionListPermissionsMixin(object):
    """Mixin used for Sighting lists permissions
    Any user can see all non sensitive data

    """

    def get_queryset(self, *args, **kwargs):
        queryset = super().get_queryset()

        user = self.request.user

        if user.access_all_data or user.edit_all_data or user.is_superuser:
            logger.debug("ADVANCED USER")
            return queryset
        if user.is_resp:
            logger.debug("RESP USER")
            queryset = queryset.filter(
                Q(place__areas__in=user.resp_areas.all())
                | Q(
                    (Q(is_confidential=False))
                    & (Q(place__is_hidden=False) | Q(place__authorized_user=user))
                )
            )
        else:
            logger.debug("OTHER SITUATION")
            queryset = queryset.filter(
                (
                    (Q(is_confidential=False))
                    & (Q(place__is_hidden=False) | Q(place__authorized_user=user))
                )
            )

        return queryset


class PlaceActorListPermissionsMixin(object):
    """Mixin used for Sighting lists permissions
    Any user can see all non sensitive data

    """

    def get_queryset(self, *args, **kwargs):
        queryset = super().get_queryset()

        user = self.request.user

        if user.access_all_data or user.edit_all_data or user.is_superuser:
            logger.debug("ADVANCED USER")
            return queryset
        if user.is_resp:
            logger.debug("RESP USER")
            queryset = queryset.filter(
                Q(place__areas__in=user.resp_areas.all())
                | (Q(place__is_hidden=False) | Q(place__authorized_user=user))
            )
        else:
            logger.debug("OTHER SITUATION")
            queryset = queryset.filter(
                ((Q(place__is_hidden=False) | Q(place__authorized_user=user)))
            )

        return queryset


class SightingFilteringMixin:
    """Mixin used for Sighting lists filtering"""

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset()
        # qs = Sighting.objects
        place = self.request.query_params.getlist("place", None)
        area = self.request.query_params.getlist("area", None)
        specie = self.request.query_params.getlist("specie", None)
        count_min = self.request.query_params.get("count_min", None)
        count_max = self.request.query_params.get("count_max", None)
        place_type = self.request.query_params.getlist("place_type", None)
        contact = self.request.query_params.getlist("contact", None)
        alt_min = self.request.query_params.get("alt_min", None)
        alt_max = self.request.query_params.get("alt_max", None)
        gite = self.request.query_params.get("gite", None)
        creator = self.request.query_params.get("creator", None)
        metaplace = self.request.query_params.getlist("metaplace", None)
        date_min = self.request.query_params.get("date_min", None)
        date_max = self.request.query_params.get("date_max", None)
        period = self.request.query_params.get("period", None)
        sex = self.request.query_params.getlist("sex", None)
        sexual_status = self.request.query_params.get("sexual_state", None)
        age = self.request.query_params.getlist("age", None)
        bio_status = self.request.query_params.getlist("bio_status", None)
        behaviour = self.request.query_params.getlist("behaviour", None)
        breed_colo = self.request.query_params.get("breed_colo", None)
        only_mine = self.request.query_params.get("only_mine", None)
        user = self.request.query_params.get("user", None)
        study = self.request.query_params.getlist("study", None)
        organism = self.request.query_params.get("organism", None)

        qs = qs.filter(session__place__areas__in=area) if area else qs
        qs = qs.filter(session__place__in=place) if place else qs
        qs = qs.filter(session__place__type__in=place_type) if place_type else qs
        qs = qs.filter(session__contact__in=contact) if contact else qs
        qs = qs.filter(session__place__altitude__gte=alt_min) if alt_min is not None else qs
        qs = qs.filter(session__place__altitude__lte=alt_max) if alt_max is not None else qs
        qs = qs.filter(created_by=creator) if creator is not None else qs
        qs = (
            qs.filter(
                Q(created_by=user)
                | Q(session__main_observer=user)
                | Q(session__other_observer=user)
            )
            if user is not None
            else qs
        )
        qs = (
            qs.filter(
                Q(created_by=self.request.user.id)
                | Q(session__main_observer=self.request.user.id)
                | Q(session__other_observer=self.request.user.id)
            )
            if only_mine is not None
            else qs
        )
        qs = qs.filter(session__place__is_gite=True) if gite is not None else qs
        qs = qs.filter(session__place__metaplace__in=metaplace) if metaplace else qs
        qs = qs.filter(session__study__in=study) if study else qs
        qs = qs.filter(codesp__in=specie) if specie else qs
        qs = qs.filter(total_count__gte=count_min) if count_min is not None else qs
        qs = qs.filter(total_count__lte=count_max) if count_max is not None else qs
        qs = qs.filter(countdetails__sex__in=sex) if sex else qs
        qs = qs.filter(countdetails__age__in=age) if age else qs
        qs = qs.filter(countdetails__bio_status__in=bio_status) if bio_status else qs
        qs = qs.filter(countdetails__behaviour__in=behaviour) if behaviour else qs
        qs = (
            qs.filter(
                Q(countdetails__mamelle__code__icontains=sexual_status)
                | Q(countdetails__mamelle__descr__icontains=sexual_status)
                | Q(countdetails__gestation__code__icontains=sexual_status)
                | Q(countdetails__gestation__descr__icontains=sexual_status)
                | Q(countdetails__testicule__code__icontains=sexual_status)
                | Q(countdetails__testicule__descr__icontains=sexual_status)
                | Q(countdetails__etat_sexuel__icontains=sexual_status)
                | Q(countdetails__etat_sexuel__icontains=sexual_status)
            )
            if sexual_status is not None
            else qs
        )
        qs = (
            qs.filter(Q(session__date_start__gte=date_min) | Q(session__date_end__gte=date_min))
            if date_min is not None
            else qs
        )
        qs = (
            qs.filter(Q(session__date_start__lte=date_max) | Q(session__date_end__lte=date_max))
            if date_max is not None
            else qs
        )
        qs = qs.filter(period__icontains=period) if period is not None else qs
        qs = qs.filter(breed_colo=True) if breed_colo is not None else qs
        # TODO: Manage organism on metadata when available
        qs = (
            qs.filter(Q(session__main_observer__organisms=organism))
            if organism is not None
            else qs
        )

        logger.debug(f"QS {dir(qs)}")
        return qs


class SightingListPermissionsMixin(object):
    """Mixin used for Sighting lists permissions"""

    def get_queryset(self, *args, **kwargs):
        queryset = super().get_queryset()

        user = self.request.user
        user = get_user_model().objects.get(id=user.id)

        if user.access_all_data or user.edit_all_data or user.is_superuser:
            return queryset
        if settings.SEE_ALL_NON_SENSITIVE_DATA:
            if user.is_resp:
                queryset = (
                    queryset.filter(
                        Q(
                            Q(created_by=user)
                            | Q(observer=user)
                            | Q(session__main_observer=user)
                            | Q(session__other_observer=user)
                            | Q(session__study__project_manager=user)
                        )
                        | Q(session__place__areas__in=user.resp_areas.all())
                        | Q(
                            (Q(session__is_confidential=False))
                            & (
                                Q(session__place__is_hidden=False)
                                | Q(session__place__authorized_user=user)
                            )
                        )
                    )
                    .distinct()
                    .order_by("-timestamp_update")
                )
            else:
                queryset = (
                    queryset.filter(
                        (
                            Q(created_by=user)
                            | Q(observer=user)
                            | Q(session__main_observer=user)
                            | Q(session__other_observer=user)
                            | Q(session__study__project_manager=user)
                        )
                        | (
                            (Q(session__is_confidential=False))
                            & (
                                Q(session__place__is_hidden=False)
                                | Q(session__place__authorized_user=user)
                            )
                        )
                    )
                    .distinct()
                    .order_by("-timestamp_update")
                )
        else:
            if user.is_resp:
                queryset = (
                    queryset.filter(
                        Q(
                            Q(created_by=user)
                            | Q(observer=user)
                            | Q(session__main_observer=user)
                            | Q(session__other_observer=user)
                            | Q(session__study__project_manager=user)
                        )
                        | Q(session__place__areas__in=user.resp_areas.all())
                    )
                    .distinct()
                    .order_by("-timestamp_update")
                )
            else:
                queryset = (
                    queryset.filter(
                        Q(created_by=user)
                        | Q(observer=user)
                        | Q(session__other_observer__username__contains=user.username)
                        | Q(session__study__project_manager=user)
                    )
                    .distinct()
                    .order_by("-timestamp_update")
                )

        return queryset


class SightingFilteredListWithPermissions(
    SightingFilteringMixin,
    SightingListPermissionsMixin,
):
    pass


def countdetail_filtering(current_user, qs, query_params):
    """Set countdetail queryset from query string values

    :param current_user: Current logged user
    :type current_user: User
    :param qs: QuerySet
    :type qs: _type_
    :param query_params: Query string parameters
    :type query_params: QueryDict
    :return: QuerySet
    :rtype: _type_
    """
    place = query_params.getlist("place")
    area = query_params.getlist("area")
    specie = query_params.getlist("specie")
    count_min = query_params.get("count_min", None)
    count_max = query_params.get("count_max", None)
    place_type = query_params.getlist("place_type")
    contact = query_params.getlist("contact")
    alt_min = query_params.get("alt_min", None)
    alt_max = query_params.get("alt_max", None)
    gite = query_params.get("gite", None)
    creator = query_params.getlist("creator")
    metaplace = query_params.getlist("metaplace")
    date_min = query_params.get("date_min", None)
    date_max = query_params.get("date_max", None)
    period = query_params.getlist("period")
    sex = query_params.getlist("sex")
    sexual_status = query_params.getlist("sexual_state")
    age = query_params.getlist("age")
    breed_colo = query_params.get("breed_colo", None)
    only_mine = query_params.get("only_mine", None)
    user = query_params.get("user", None)
    study = query_params.get("study", None)

    qs = qs.filter(sighting__session__place__areas__in=area) if len(area) > 0 else qs
    qs = qs.filter(sighting__session__place__in=place) if len(place) > 0 else qs
    qs = qs.filter(sighting__session__place__type__in=place_type) if len(place_type) > 0 else qs
    qs = qs.filter(sighting__session__contact__in=contact) if len(contact) > 0 else qs
    qs = qs.filter(sighting__session__place__altitude__gte=alt_min) if alt_min is not None else qs
    qs = qs.filter(sighting__session__place__altitude__lte=alt_max) if alt_max is not None else qs
    qs = qs.filter(sighting__created_by__in=creator) if len(creator) > 0 else qs
    qs = (
        qs.filter(
            Q(sighting__created_by=current_user)
            | Q(sighting__session__main_observer=current_user)
            | Q(sighting__session__other_observer=current_user)
        )
        if only_mine is not None
        else qs
    )
    qs = (
        qs.filter(
            Q(sighting__created_by=user)
            | Q(sighting__session__main_observer=user)
            | Q(sighting__session__other_observer=user)
        )
        if user is not None
        else qs
    )
    qs = qs.filter(sighting__session__place__is_gite=True) if gite is not None else qs
    qs = qs.filter(sighting__session__place__metaplace__in=metaplace) if len(metaplace) > 0 else qs
    qs = qs.filter(sighting__codesp__in=specie) if len(specie) > 0 else qs
    qs = qs.filter(sighting__total_count__gte=count_min) if count_min is not None else qs
    qs = qs.filter(sighting__total_count__lte=count_max) if count_max is not None else qs
    qs = qs.filter(sex__in=sex) if len(sex) > 0 else qs
    qs = qs.filter(age__in=age) if len(age) > 0 else qs
    qs = qs.filter(sighting__session__study=study) if study else qs
    qs = (
        qs.filter(
            Q(mamelle__in=sexual_status)
            | Q(gestation__in=sexual_status)
            | Q(testicule__in=sexual_status)
            | Q(etat_sexuel__in=sexual_status)
        )
        if len(sexual_status) > 0
        else qs
    )
    qs = (
        qs.filter(
            Q(sighting__session__date_start__gte=date_min)
            | Q(sighting__session__date_end__gte=date_min)
        )
        if date_min is not None
        else qs
    )
    qs = (
        qs.filter(
            Q(sighting__session__date_start__lte=date_max)
            | Q(sighting__session__date_end__lte=date_max)
        )
        if date_max is not None
        else qs
    )
    qs = qs.filter(sighting__period__in=period) if len(period) > 0 else qs
    qs = qs.filter(sighting__breed_colo=True) if breed_colo is not None else qs
    logger.debug(f"Final QS {qs._query}")
    return qs


def countdetail_list_permission(user, qs):
    """_summary_

    :param user: _description_
    :type user: _type_
    :param qs: _description_
    :type qs: _type_
    :return: _description_
    :rtype: _type_
    """

    if user.access_all_data or user.edit_all_data or user.is_superuser or user.export_all_data:
        # Peut tout exporter sans limite
        qs = qs.distinct().order_by("-timestamp_update")
    else:
        if settings.EXPORT_ALL_NON_SENSITIVE_DATA:
            if user.is_resp:
                qs = (
                    qs.filter(
                        Q(
                            Q(sighting__created_by=user)
                            | Q(sighting__observer=user)
                            | Q(sighting__session__main_observer=user)
                            | Q(sighting__session__other_observer=user)
                            | Q(sighting__session__study__project_manager=user)
                        )
                        | Q(sighting__session__place__areas__in=user.resp_areas.all())
                        | Q(
                            (Q(sighting__session__is_confidential=False))
                            & (
                                Q(sighting__session__place__is_hidden=False)
                                | Q(sighting__session__place__authorized_user=user)
                            )
                        )
                    )
                    .distinct()
                    .order_by("-timestamp_update")
                )
            else:
                qs = (
                    qs.filter(
                        (
                            Q(sighting__created_by=user)
                            | Q(sighting__observer=user)
                            | Q(sighting__session__main_observer=user)
                            | Q(sighting__session__other_observer=user)
                            | Q(sighting__session__study__project_manager=user)
                        )
                        | (
                            (Q(sighting__session__is_confidential=False))
                            & (
                                Q(sighting__session__place__is_hidden=False)
                                | Q(sighting__session__place__authorized_user=user)
                            )
                        )
                    )
                    .distinct()
                    .order_by("-timestamp_update")
                )
        else:
            if user.is_resp:
                qs = (
                    qs.filter(
                        Q(
                            Q(sighting__created_by=user)
                            | Q(sighting__observer=user)
                            | Q(sighting__session__main_observer=user)
                            | Q(sighting__session__other_observer=user)
                            | Q(sighting__session__study__project_manager=user)
                        )
                        | Q(sighting__session__place__areas__in=user.resp_areas.all())
                    )
                    .distinct()
                    .order_by("-timestamp_update")
                )
            else:
                qs = (
                    qs.filter(
                        Q(sighting__created_by=user)
                        | Q(sighting__observer=user)
                        | Q(sighting__session__other_observer__username__contains=user.username)
                        | Q(sighting__session__study__project_manager=user)
                    )
                    .distinct()
                    .order_by("-timestamp_update")
                )

    return qs
