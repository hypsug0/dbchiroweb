# import the logging library
import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics
from rest_framework.filters import SearchFilter
from rest_framework.permissions import IsAuthenticated
from rest_framework_gis.filters import InBBoxFilter

from core.mixins import SmallResultsSetPagination
from sights.mixins import LargeGeoJsonPageNumberPagination, PlaceFilteredListWithPermissions
from sights.models import Place
from sights.place.serializers import CountModelMixin, PlaceSerializer, PlaceSimpleSerializer

# Get an instance of a logger
logger = logging.getLogger(__name__)

place_qs = (
    (
        Place.objects.select_related("metaplace")
        .select_related("metaplace__type")
        .select_related("created_by")
        .select_related("type")
        .prefetch_related("areas")
        .prefetch_related("areas__area_type")
    )
    .order_by("-timestamp_update")
    .all()
)


class PlacesSearchApi(PlaceFilteredListWithPermissions, generics.ListAPIView):
    serializer_class = PlaceSimpleSerializer
    pagination_class = SmallResultsSetPagination
    queryset = (
        Place.objects.select_related("type")
        .prefetch_related("areas__area_type")
        .order_by("name")
        .all()
    )
    filter_backends = (SearchFilter, DjangoFilterBackend, InBBoxFilter)
    filter_fields = ["id_place", "name"]
    search_fields = ["name"]
    permission_classes = [
        IsAuthenticated,
    ]

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset(*args, **kwargs)
        print(f"query {qs.query}")
        print(f"raw {qs.raw}")
        return qs


class GeoJSONPlaces(
    LoginRequiredMixin,
    PlaceFilteredListWithPermissions,
    CountModelMixin,
    generics.ListAPIView,
):
    serializer_class = PlaceSerializer
    pagination_class = LargeGeoJsonPageNumberPagination
    queryset = place_qs


class GeoJSONBBoxFilteredPlaces(
    LoginRequiredMixin,
    PlaceFilteredListWithPermissions,
    CountModelMixin,
    generics.ListAPIView,
):
    serializer_class = PlaceSerializer
    pagination_class = LargeGeoJsonPageNumberPagination
    bbox_filter_field = "geom"
    filter_backends = (InBBoxFilter,)
    bbox_filter_include_overlapping = True  # Optional
    queryset = place_qs
