# import the logging library
import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics
from rest_framework.filters import SearchFilter
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet
from rest_framework_gis.filters import InBBoxFilter

from core.mixins import SmallResultsSetPagination
from sights.mixins import (
    LargeGeoJsonPageNumberPagination,
    PlaceActorListPermissionsMixin,
    PlaceFilteredListWithPermissions,
)
from sights.models import Place, PlaceActors
from sights.place.serializers import (
    CountModelMixin,
    PlaceActorsReadOnlySerializer,
    PlaceActorsSerializer,
    PlaceSerializer,
    PlaceSimpleSerializer,
)

from .permissions import PlaceActorPermissions

# Get an instance of a logger
logger = logging.getLogger(__name__)

place_qs = (
    (
        Place.objects.select_related("metaplace")
        .select_related("metaplace__type")
        .select_related("created_by")
        .select_related("type")
        .prefetch_related("areas")
        .prefetch_related("areas__area_type")
    )
    .order_by("-timestamp_update")
    .all()
)


class PlacesSearchApi(PlaceFilteredListWithPermissions, generics.ListAPIView):
    serializer_class = PlaceSimpleSerializer
    pagination_class = SmallResultsSetPagination
    queryset = (
        Place.objects.select_related("type")
        .prefetch_related("areas__area_type")
        .order_by("name")
        .all()
    )
    filter_backends = (SearchFilter, DjangoFilterBackend, InBBoxFilter)
    filter_fields = ["id_place", "name"]
    search_fields = ["name"]
    permission_classes = [
        IsAuthenticated,
    ]

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset(*args, **kwargs)
        print(f"query {qs.query}")
        print(f"raw {qs.raw}")
        return qs


class GeoJSONPlaces(
    LoginRequiredMixin,
    PlaceFilteredListWithPermissions,
    CountModelMixin,
    generics.ListAPIView,
):
    serializer_class = PlaceSerializer
    pagination_class = LargeGeoJsonPageNumberPagination
    queryset = place_qs


class GeoJSONBBoxFilteredPlaces(
    LoginRequiredMixin,
    PlaceFilteredListWithPermissions,
    CountModelMixin,
    generics.ListAPIView,
):
    serializer_class = PlaceSerializer
    pagination_class = LargeGeoJsonPageNumberPagination
    bbox_filter_field = "geom"
    filter_backends = (InBBoxFilter,)
    bbox_filter_include_overlapping = True  # Optional
    queryset = place_qs


class PlaceActorsApi(PlaceActorListPermissionsMixin, ModelViewSet):
    queryset = (
        PlaceActors.objects.select_related("place")
        .select_related("role")
        .select_related("legal_person")
        .select_related("natural_person")
    )
    serializer_class = PlaceActorsReadOnlySerializer
    filter_backends = [DjangoFilterBackend]
    permission_classes = [
        IsAuthenticated,
        PlaceActorPermissions,
    ]
    filterset_fields = ["place", "role", "legal_person", "natural_person"]

    def get_serializer_class(self):
        if self.action in ["retrieve", "create", "update", "partial_update"]:
            return PlaceActorsSerializer
        return super().get_serializer_class()
