"""
    Vues de l'application Sights
"""

import logging

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.gis.geos import Point
from django.core.files.storage import FileSystemStorage
from django.db import IntegrityError
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import DetailView, TemplateView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

from core import js

from ..forms import (
    BridgeForm,
    BuildForm,
    CaveForm,
    PlaceForm,
    PlaceManagementForm,
    TreeForm,
    TreeGiteForm,
)
from ..mixins import (
    PlaceDetailEditAuthMixin,
    PlaceDetailViewAuthMixin,
    PlaceEditAuthMixin,
    PlaceViewAuthMixin,
    TreeGiteEditAuthMixin,
)
from ..models import Bridge, Build, Cave, Metaplace, Place, PlaceManagement, Tree, TreeGite
from ..tables import (
    BridgeTable,
    BuildTable,
    CaveTable,
    PlaceManagementTable,
    TreeGiteTable,
    TreeTable,
)

logger = logging.getLogger(__name__)

place_detail_params = {
    "tree": {
        "model": Tree,
        "table": TreeTable,
        "title": "Etats des lieux de l'arbre",
        "icon": "fa fa-tree",
    },
    "building": {
        "model": Build,
        "table": BuildTable,
        "title": _("Etats des lieux du bâtiment"),
        "icon": "mki mki-ruins",
    },
    "bridge": {
        "model": Bridge,
        "table": BridgeTable,
        "title": _("Etats des lieux du pont"),
        "icon": "mki mki-ruins",
    },
    "cave": {
        "model": Cave,
        "table": CaveTable,
        "title": _("Etats des lieux de la cave"),
        "icon": "mki mki-cave_entrance",
    },
}


class PlaceCreate(LoginRequiredMixin, CreateView):
    """Create view for the Place model."""

    model = Place
    form_class = PlaceForm
    file_storage = FileSystemStorage()
    template_name = "leaflet_form.html"

    def form_valid(self, form):
        srid = form.cleaned_data.pop("srid", settings.GEODATA_SRID)
        logger.debug(f"form cleaned_data {form.cleaned_data} {srid}")
        form.instance.created_by = self.request.user
        if self.kwargs.get("id_metaplace"):
            metaplace = Metaplace.objects.get(pk=self.kwargs.get("id_metaplace"))
            logger.debug("place_create metaplace %s", metaplace)
            form.instance.metaplace = metaplace
        # Check for manual x/y before the leaflet geom
        if form.instance.x and form.instance.y:
            geom = Point(x=form.instance.x, y=form.instance.y, srid=int(srid))
            if srid and srid != settings.GEODATA_SRID:
                geom.transform(settings.GEODATA_SRID)
            form.instance.geom = geom
            # form.instance.x = form.instance.geom.x
            # form.instance.y = form.instance.geom.y
        # else:
        #     form.instance.x = form.instance.geom.x
        #     form.instance.y = form.instance.geom.y
        if form.instance.type:
            if not form.instance.is_gite:
                form.instance.is_gite = True
        return super().form_valid(form)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if self.kwargs.get("metaplace"):
            kwargs["metaplace"] = self.kwargs.get("metaplace")
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-map"
        context["title"] = _("Ajout d'une localité")
        context[
            "js"
        ] = """

        """
        return context

        # Improve workflow - Go to Session
        def get_success_url(self):
            id_place = self.object.id_place
            if self.request.method == "POST" and "gotoSession" in self.request.POST:
                return reverse_lazy("sights:session_create", kwargs={"pk": id_place})
            return reverse_lazy("sights:place_detail", kwargs={"pk": id_place})


class PlaceDetail(PlaceViewAuthMixin, DetailView):
    model = Place
    template_name = "place_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        loggeduser = self.request.user
        logger.debug(f"loggeduser {loggeduser} f{dir(loggeduser)}")
        pk = self.kwargs.get("pk")
        placeobject = (
            Place.objects.select_related("created_by")
            .prefetch_related("areas")
            .prefetch_related("authorized_user")
        ).get(id_place=pk)
        placedetail = False
        if placeobject.type:
            if placeobject.type.category in ("tree", "building", "bridge", "cave"):
                placedetail = True
                category_param = place_detail_params[placeobject.type.category]
                placedetailcount = category_param["model"].objects.filter(place=pk).count()
                placedetailtable = category_param["table"](
                    category_param["model"].objects.filter(place=pk)
                )
                placedetailicon = category_param["icon"]
                placedetailtitle = category_param["title"]
                placedetailcreateurl = (
                    f"{{% url 'sights:{placeobject.type.category}_create' pk=object.pk %}}"
                )

        context["icon"] = "fa fa-fw fa-map"
        context["title"] = _("Détail d'une localité")
        context[
            "js"
        ] = """
        """
        if placedetail:
            context["placedetail"] = placedetail
            context["placedetailicon"] = placedetailicon
            context["placedetailtitle"] = placedetailtitle
            context["placedetailcount"] = placedetailcount
            context["placedetailtable"] = placedetailtable
            context["placedetailcreateurl"] = placedetailcreateurl
        if placeobject.is_managed:
            context["placemanagementicon"] = "fi-checkbox"
            context["placemanagementtitle"] = _("Actions de gestion")
            context["placemanagementcount"] = PlaceManagement.objects.filter(place=pk).count()
            context["placemanagementtable"] = PlaceManagementTable(
                PlaceManagement.objects.filter(place=pk).order_by("-date")
            )
            context[
                "placemanagementcreateurl"
            ] = "{% url 'management:management_create' pk=object.pk %}"
        return context


class PlaceUpdate(PlaceEditAuthMixin, UpdateView):
    model = Place
    form_class = PlaceForm
    file_storage = FileSystemStorage()
    template_name = "leaflet_form.html"

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        if not form.instance.geom:
            if form.instance.x and form.instance.y:
                geom = Point(form.instance.x, form.instance.y)
                if form.instance.srid != settings.GEODATA_SRID:
                    form.instance.geom = geom.transform(form.instance.srid)
                else:
                    form.instance.geom = Point(form.instance.x, form.instance.y)
        form.instance.x = form.instance.geom.x
        form.instance.y = form.instance.geom.y
        if form.instance.type:
            if not form.instance.is_gite:
                form.instance.is_gite = True
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-map"
        context["title"] = "Modification d'une localité"
        context[
            "js"
        ] = """

        """
        return context

    # Improve workflow - Go to Session
    def get_success_url(self):
        id_place = self.object.id_place
        if self.request.method == "POST" and "gotoSession" in self.request.POST:
            return reverse_lazy("sights:session_create", kwargs={"pk": id_place})
        return reverse_lazy("sights:place_detail", kwargs={"pk": id_place})


class PlaceDelete(PlaceEditAuthMixin, DeleteView):
    model = Place
    template_name = "confirm_delete.html"
    success_url = reverse_lazy("blog:home")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-trash"
        context["title"] = _("Suppression d'une localité")
        context["message_alert"] = _("Êtes-vous certain de vouloir supprimer la localité")
        return context


class PlaceList(LoginRequiredMixin, TemplateView):
    template_name = "place_search.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-map"
        context["title"] = _("Rechercher une localité")
        context["createplacebtn"] = True
        context[
            "js"
        ] = """
        """

        return context


class PlaceMyList(LoginRequiredMixin, TemplateView):
    template_name = "place_search.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-map"
        context["title"] = _("Rechercher dans mes localités")
        context[
            "js"
        ] = """
        """
        context["only_mine"] = True
        return context


class PlaceManagementCreate(LoginRequiredMixin, CreateView):
    """Create view for the Study model."""

    model = PlaceManagement
    form_class = PlaceManagementForm
    template_name = "normal_form.html"

    def get_initial(self):
        initial = super().get_initial()
        initial = initial.copy()
        initial["referent"] = self.request.user
        return initial

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.place_id = self.kwargs.get("pk")
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fi-star"
        context["title"] = _("Ajout d'une action de gestion")
        context["js"] = js.DateInput
        return context


class PlaceManagementUpdate(PlaceDetailEditAuthMixin, UpdateView):
    model = PlaceManagement
    form_class = PlaceManagementForm
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fi-star"
        context["title"] = _("Mise à jour d'une action de gestion")
        context[
            "js"
        ] = """
        $(function () {
            $('.dateinput').fdatepicker({
                format: 'dd/mm/yyyy',
                disableDblClickSelection: true,
                leftArrow: '<i class="fa fa-fw fa-chevron-left"></i>',
                rightArrow: '<i class="fa fa-fw fa-chevron-right"></i>',
            });
        });
        """
        return context


class PlaceManagementDelete(PlaceDetailEditAuthMixin, DeleteView):
    model = PlaceManagement
    template_name = "confirm_delete.html"
    success_url = reverse_lazy("management:study_list")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-trash"
        context["title"] = _("Suppression d'une action de gestion")
        context["message_alert"] = _("Êtes-vous certain de vouloir supprimer l'action")
        return context


#################################################
#               Bridge model views                #
#################################################


class BridgeCreate(LoginRequiredMixin, CreateView):
    model = Bridge
    form_class = BridgeForm
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.place_id = self.kwargs.get("pk")
        try:
            return super().form_valid(form)
        except IntegrityError:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _("Cette état des lieux existe déjà"))
            return HttpResponseRedirect(self.request.path)
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "mki mki-cave_entrance"
        context["title"] = _("Ajout d'un état des lieux d'un pont")
        context["js"] = js.DateInput
        return context


class BridgeUpdate(PlaceDetailEditAuthMixin, UpdateView):
    model = Bridge
    form_class = BridgeForm
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        try:
            return super().form_valid(form)
        except IntegrityError:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _("Cette état des lieux existe déjà"))
            return HttpResponseRedirect(self.request.path)
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "mki mki-cave_entrance"
        context["title"] = _("Modification d'un état des lieux d'un pont")
        context["js"] = js.DateInput
        return context


class BridgeDelete(PlaceDetailEditAuthMixin, DeleteView):
    model = Bridge
    template_name = "confirm_delete.html"

    def get_success_url(self):
        return reverse_lazy("sights:place_detail", kwargs={"pk": self.object.place_id})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-trash"
        context["title"] = _("Suppression d'un état des lieux d'un pont")
        context["message_alert"] = _(
            "Êtes-vous certain de vouloir supprimer l'état des lieux d'un pont"
        )
        return context


#################################################
#               Tree model views                #
#################################################


class TreeCreate(LoginRequiredMixin, CreateView):
    model = Tree
    form_class = TreeForm
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.place_id = self.kwargs.get("pk")
        try:
            return super().form_valid(form)
        except IntegrityError:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _("Cette état des lieux existe déjà"))
            return HttpResponseRedirect(self.request.path)
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-tree"
        context["title"] = _("Ajout d'un état des lieux d'un arbre")
        context["js"] = js.DateInput
        return context


class TreeUpdate(PlaceDetailEditAuthMixin, UpdateView):
    model = Tree
    form_class = TreeForm
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        try:
            return super().form_valid(form)
        except IntegrityError:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _("Cette état des lieux existe déjà"))
            return HttpResponseRedirect(self.request.path)
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "mki mki-ruins"
        context["title"] = _("Modification d'un état des lieux d'un arbre")
        context["js"] = js.DateInput
        return context


class TreeDelete(PlaceDetailEditAuthMixin, DeleteView):
    model = Tree
    template_name = "confirm_delete.html"

    def get_success_url(self):
        return reverse_lazy("sights:place_detail", kwargs={"pk": self.object.place_id})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-trash"
        context["title"] = _("Suppression d'un état des lieux d'un arbre")
        context["message_alert"] = _(
            "Êtes-vous certain de vouloir supprimer l'état des lieux d'un arbre"
        )
        return context


class TreeDetail(PlaceDetailViewAuthMixin, DetailView):
    model = Tree
    template_name = "tree_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        pk = self.kwargs.get("pk")
        # Rendu des sessions
        context["icon"] = "fi-trees"
        context["title"] = _("Détail de l'état des lieux d'un arbre")
        context[
            "js"
        ] = """
        """
        # Rendu des observations
        context["treegiteicon"] = "fi-home"
        context["treegitetitle"] = _("Détails des gîtes")
        context["treegitecount"] = TreeGite.objects.filter(tree=pk).distinct().count()
        context["treegitetable"] = TreeGiteTable(TreeGite.objects.filter(tree=pk))
        return context


#################################################
#               TreeGite model views                #
#################################################


class TreeGiteCreate(LoginRequiredMixin, CreateView):
    model = TreeGite
    form_class = TreeGiteForm
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.tree_id = self.kwargs.get("pk")
        try:
            return super().form_valid(form)
        except IntegrityError:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _("Ce gîte existe déjà"))
            return HttpResponseRedirect(self.request.path)
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-tree"
        context["title"] = _("Ajout d'un détail de gîte arboricole")
        context[
            "js"
        ] = """
        """
        return context


class TreeGiteUpdate(TreeGiteEditAuthMixin, UpdateView):
    model = TreeGite
    form_class = TreeGiteForm
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        form.instance.tree_id = self.kwargs.get("pk")
        try:
            return super().form_valid(form)
        except IntegrityError:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _("Ce gîte existe déjà"))
            return HttpResponseRedirect(self.request.path)
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-tree"
        context["title"] = _("Modification d'un détail de gîte arboricole")
        context[
            "js"
        ] = """
        """
        return context


class TreeGiteDelete(TreeGiteEditAuthMixin, DeleteView):
    model = Tree
    template_name = "confirm_delete.html"

    def get_success_url(self):
        return reverse_lazy("sights:place_detail", kwargs={"pk": self.object.place_id})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-trash"
        context["title"] = _("Suppression d'un détail de gîte arboricole")
        context["message_alert"] = _("Êtes-vous certain de vouloir supprimer ce détail de gîte")
        return context


#################################################
#               Cave model views                #
#################################################


class CaveCreate(LoginRequiredMixin, CreateView):
    model = Cave
    form_class = CaveForm
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.place_id = self.kwargs.get("pk")
        try:
            return super().form_valid(form)
        except IntegrityError:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _("Cette état des lieux existe déjà"))
            return HttpResponseRedirect(self.request.path)
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "mki mki-cave_entrance"
        context["title"] = _("Ajout d'un état des lieux d'une cavité")
        context["js"] = js.DateInput
        return context


class CaveUpdate(PlaceDetailEditAuthMixin, UpdateView):
    model = Cave
    form_class = CaveForm
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        try:
            return super().form_valid(form)
        except IntegrityError:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _("Cette état des lieux existe déjà"))
            return HttpResponseRedirect(self.request.path)
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "mki mki-cave_entrance"
        context["title"] = _("Modification d'un état des lieux d'une cavité")
        context["js"] = js.DateInput
        return context


class CaveDelete(PlaceDetailEditAuthMixin, DeleteView):
    model = Cave
    template_name = "confirm_delete.html"

    def get_success_url(self):
        return reverse_lazy("sights:place_detail", kwargs={"pk": self.object.place_id})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-trash"
        context["title"] = _("Suppression d'un état des lieux d'une cavité")
        context["message_alert"] = _(
            "Êtes-vous certain de vouloir supprimer l'état des lieux d'une cavité"
        )
        return context


#################################################
#               Build model views                #
#################################################


class BuildCreate(LoginRequiredMixin, CreateView):
    model = Build
    form_class = BuildForm
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.place_id = self.kwargs.get("pk")
        try:
            return super().form_valid(form)
        except IntegrityError:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _("Cette état des lieux existe déjà"))
            return HttpResponseRedirect(self.request.path)
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "mki mki-ruins"
        context["title"] = _("Ajout d'un état des lieux de bâtiment")
        context["js"] = js.DateInput
        return context


class BuildUpdate(PlaceDetailEditAuthMixin, UpdateView):
    model = Build
    form_class = BuildForm
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        try:
            return super().form_valid(form)
        except IntegrityError:
            messages.error(self.request, _("Cette état des lieux existe déjà"))
            return HttpResponseRedirect(self.request.path)
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "mki mki-ruins"
        context["title"] = _("Modification d'un état des lieux de bâtiment")
        context["js"] = js.DateInput
        return context


class BuildDelete(PlaceDetailEditAuthMixin, DeleteView):
    model = Build
    template_name = "confirm_delete.html"

    def get_success_url(self):
        return reverse_lazy("sights:place_detail", kwargs={"pk": self.object.place_id})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-trash"
        context["title"] = _("Suppression d'un état des lieux de bâtiment")
        context["message_alert"] = _(
            "Êtes-vous certain de vouloir supprimer l'état des lieux de bâtiment"
        )
        return context
