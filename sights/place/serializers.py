from django.contrib.auth import get_user_model
from django.utils import timezone
from rest_framework import serializers
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework_gis.serializers import GeoFeatureModelSerializer, ModelSerializer
from sinp_nomenclatures.serializers import NomenclatureSerializer
from sinp_organisms.serializers import OrganismSerializer

from accounts.models import Profile
from accounts.serializer import ProfileSearchSerializer
from dicts.models import TypePlace
from geodata.serializers import AreasSimpleSerializer
from sights.models import Place, PlaceActors

from ..metaplace.serializers import MetaplaceLightSerializer

User = get_user_model()


class CountModelMixin(object):
    """
    Count a queryset.
    """

    @action(detail=False)
    def count(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        content = {"count": queryset.count()}
        return Response(content)


class PlaceTypeSerializer(ModelSerializer):
    class Meta:
        model = TypePlace
        fields = ("code", "category", "descr")


class PlaceUserSerializer(ModelSerializer):
    class Meta:
        model = Profile
        fields = ("full_name",)


class PlaceSession:
    pass


class PlaceSimpleSerializer(ModelSerializer):
    type = PlaceTypeSerializer()
    areas = AreasSimpleSerializer(many=True)

    class Meta:
        model = Place
        fields = ("id_place", "name", "type", "areas")


class PlaceSerializer(GeoFeatureModelSerializer):
    areas = AreasSimpleSerializer(many=True)
    type = PlaceTypeSerializer()
    created_by = PlaceUserSerializer()
    metaplace = MetaplaceLightSerializer()

    class Meta:
        model = Place
        depth = 1
        geo_field = "geom"
        fields = (
            "id_place",
            "name",
            "altitude",
            "areas",
            "metaplace",
            "type",
            "is_managed",
            "is_hidden",
            "convention",
            "geom",
            "created_by",
            "timestamp_create",
        )


class SimplePlaceSerializer(ModelSerializer):
    areas = AreasSimpleSerializer(many=True)

    class Meta:
        model = Place
        fields = ("id_place", "name", "areas")


class PlaceActorsSerializer(ModelSerializer):
    """Place actors serializer"""

    created_by = serializers.HiddenField(
        default=serializers.CreateOnlyDefault(serializers.CurrentUserDefault())
    )
    updated_by = serializers.HiddenField(default=serializers.CurrentUserDefault())
    timestamp_create = serializers.DateTimeField(
        default=serializers.CreateOnlyDefault(timezone.now)
    )
    timestamp_create = serializers.DateTimeField(read_only=True, default=timezone.now)
    # legal_person = PrimaryKeyRelatedField(
    #     queryset=Organism.objects.filter(enabled=True), default=OrganismSerializer()
    # )
    # role = PrimaryKeyRelatedField(
    #     queryset=Nomenclature.objects.all(), default=NomenclatureSerializer()
    # )
    # natural_person = PrimaryKeyRelatedField(
    #     queryset=User.objects.all(), default=ProfileSerializer()
    # )

    class Meta:
        model = PlaceActors
        fields = (
            "id",
            "place",
            "uuid",
            "role",
            "legal_person",
            "natural_person",
            "date_start",
            "date_end",
            "created_by",
            "updated_by",
            "timestamp_create",
            "timestamp_update",
        )

    def validate(self, attrs):
        """
        Checks
        """
        if (
            not any(v is None for v in (attrs["date_start"], attrs["date_end"]))
            and attrs["date_start"] > attrs["date_end"]
        ):
            raise serializers.ValidationError("End date must occur after start")
        if (
            len([i for i in (attrs["legal_person"], attrs["natural_person"]) if i is not None])
            != 1
        ):
            raise serializers.ValidationError("You should choose ONE legal or natural actor")
        return attrs


class PlaceActorsReadOnlySerializer(PlaceActorsSerializer):
    legal_person = OrganismSerializer()
    role = NomenclatureSerializer()
    natural_person = ProfileSearchSerializer()
