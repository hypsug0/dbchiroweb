from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework_gis.serializers import GeoFeatureModelSerializer, ModelSerializer

from accounts.models import Profile
from dicts.models import TypePlace
from geodata.serializers import AreasSimpleSerializer
from sights.models import Place

from ..metaplace.serializers import MetaplaceLightSerializer


class CountModelMixin(object):
    """
    Count a queryset.
    """

    @action(detail=False)
    def count(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        content = {"count": queryset.count()}
        return Response(content)


class PlaceTypeSerializer(ModelSerializer):
    class Meta:
        model = TypePlace
        fields = ("code", "category", "descr")


class PlaceUserSerializer(ModelSerializer):
    class Meta:
        model = Profile
        fields = ("full_name",)


class PlaceSession:
    pass


class PlaceSimpleSerializer(ModelSerializer):
    type = PlaceTypeSerializer()
    areas = AreasSimpleSerializer(many=True)

    class Meta:
        model = Place
        fields = ("id_place", "name", "type", "areas")


class PlaceSerializer(GeoFeatureModelSerializer):
    areas = AreasSimpleSerializer(many=True)
    type = PlaceTypeSerializer()
    created_by = PlaceUserSerializer()
    metaplace = MetaplaceLightSerializer()

    class Meta:
        model = Place
        depth = 1
        geo_field = "geom"
        fields = (
            "id_place",
            "name",
            "altitude",
            "areas",
            "metaplace",
            "type",
            "is_managed",
            "convention",
            "geom",
            "created_by",
            "timestamp_create",
        )


class SimplePlaceSerializer(ModelSerializer):
    areas = AreasSimpleSerializer(many=True)

    class Meta:
        model = Place
        fields = ("id_place", "name", "areas")
