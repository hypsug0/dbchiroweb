#!/usr/bin/env python3
"""Place urls"""

from django.contrib.auth.decorators import login_required
from django.urls import path
from django.views.generic import TemplateView
from rest_framework.routers import DefaultRouter

from sights.place.api import GeoJSONBBoxFilteredPlaces, PlaceActorsApi, PlacesSearchApi
from sights.place.views import (
    BridgeCreate,
    BridgeDelete,
    BridgeUpdate,
    BuildCreate,
    BuildDelete,
    BuildUpdate,
    CaveCreate,
    CaveDelete,
    CaveUpdate,
    PlaceCreate,
    PlaceDelete,
    PlaceDetail,
    PlaceList,
    PlaceManagementCreate,
    PlaceManagementDelete,
    PlaceManagementUpdate,
    PlaceUpdate,
    TreeCreate,
    TreeDelete,
    TreeDetail,
    TreeGiteCreate,
    TreeGiteDelete,
    TreeGiteUpdate,
    TreeUpdate,
)

# router = routers.DefaultRouter()
# router.register(r"search", GeoJSONPlaces, "place_search")
router = DefaultRouter()

router.register(r"api/v1/place/actors", PlaceActorsApi, basename="place_actors")

urlpatterns = router.urls

urlpatterns += [
    # url de recherches de localités
    path("place/search", PlaceList.as_view(), name="place_search"),
    # Place views
    path(
        "place/map",
        login_required(TemplateView.as_view(template_name="place_map.html")),
        name="place_map",
    ),
    path("place/add", PlaceCreate.as_view(), name="place_create"),
    path(
        "metaplace/<int:id_metaplace>/place/add",
        PlaceCreate.as_view(),
        name="metaplace_place_create",
    ),
    path("place/<int:pk>/detail", PlaceDetail.as_view(), name="place_detail"),
    path("place/<int:pk>/update", PlaceUpdate.as_view(), name="place_update"),
    path("place/<int:pk>/delete", PlaceDelete.as_view(), name="place_delete"),
    # Place management relative URLS
    path(
        "place/<int:pk>/management/add",
        PlaceManagementCreate.as_view(),
        name="management_create",
    ),
    path(
        "management/<int:pk>/update",
        PlaceManagementUpdate.as_view(),
        name="management_update",
    ),
    path(
        "management/<int:pk>/delete",
        PlaceManagementDelete.as_view(),
        name="management_delete",
    ),
    # Build views
    path("place/<int:pk>/build/add", BuildCreate.as_view(), name="build_create"),
    path("build/<int:pk>/update", BuildUpdate.as_view(), name="build_update"),
    path("build/<int:pk>/delete", BuildDelete.as_view(), name="build_delete"),
    # Tree views
    path("place/<int:pk>/tree/add", TreeCreate.as_view(), name="tree_create"),
    path("tree/<int:pk>/update", TreeUpdate.as_view(), name="tree_update"),
    path("tree/<int:pk>/delete", TreeDelete.as_view(), name="tree_delete"),
    path("tree/<int:pk>/detail", TreeDetail.as_view(), name="tree_detail"),
    # TreeGite views
    path("tree/<int:pk>/treegite/add", TreeGiteCreate.as_view(), name="treegite_create"),
    path("treegite/<int:pk>/update", TreeGiteUpdate.as_view(), name="treegite_update"),
    path("treegite/<int:pk>/delete", TreeGiteDelete.as_view(), name="treegite_delete"),
    # Cave views
    path("place/<int:pk>/cave/add", CaveCreate.as_view(), name="cave_create"),
    path("cave/<int:pk>/update", CaveUpdate.as_view(), name="cave_update"),
    path("cave/<int:pk>/delete", CaveDelete.as_view(), name="cave_delete"),
    # Bridge views
    path("place/<int:pk>/bridge/add", BridgeCreate.as_view(), name="bridge_create"),
    path("bridge/<int:pk>/update", BridgeUpdate.as_view(), name="bridge_update"),
    path("bridge/<int:pk>/delete", BridgeDelete.as_view(), name="bridge_delete"),
    path("api/v1/place/search", PlacesSearchApi.as_view(), name="place_search_api"),
    path(
        "api/v1/place/geosearch",
        GeoJSONBBoxFilteredPlaces.as_view(),
        name="place_geosearch_api",
    ),
]
