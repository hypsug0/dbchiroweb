import logging

from crispy_forms.bootstrap import Accordion
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Button, Column, Div, Fieldset, Layout, Row, Submit
from dal import autocomplete
from django.contrib.auth import get_user_model
from django.contrib.gis import forms
from django.core.exceptions import NON_FIELD_ERRORS
from django.db.models import Q
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from leaflet.forms.widgets import LeafletWidget

from core.forms import DangerAccordionGroup, InfoAccordionGroup, PrimaryAccordionGroup
from dicts.models import CountPrecision, CountUnit, Method, TypeDevice
from management.models import Transmitter

from .models import (
    Bridge,
    Build,
    Cave,
    CountDetail,
    Device,
    Metaplace,
    Place,
    PlaceManagement,
    Session,
    SessionLink,
    Sighting,
    Tree,
    TreeGite,
)

logger = logging.getLogger(__name__)


class MetaplaceForm(forms.ModelForm):
    class Meta:
        model = Metaplace
        fields = ("name", "type", "geom", "autogeom")
        widgets = {"geom": LeafletWidget()}

    def __init__(self, *args, **kwargs):
        # input_sm_helper(self)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = "submit"
        self.helper.form_show_errors = True
        self.helper.layout = Layout(
            Row(
                Column(
                    Submit("submit", _("Enregistrer"), css_class="btn-primary btn-sm"),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group right",
                    role="button",
                )
            ),
            Accordion(
                PrimaryAccordionGroup(
                    "Informations principales",
                    Row(
                        Column("name", css_class="col-xs-12 col-md-6"),
                        Column("type", css_class="col-xs-12 col-md-6"),
                        Fieldset(
                            _("Localisation"),
                            Column("autogeom", css_class="col-lg-12"),
                            Column("geom", css_class="col-lg-12"),
                            css_class="col-lg-12",
                        ),
                        css_class="col-lg-12",
                    ),
                ),
            ),
            Row(
                Column(
                    Submit("submit", _("Enregistrer"), css_class="btn-primary btn-sm"),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group right",
                    role="button",
                )
            ),
        )
        super().__init__(*args, **kwargs)


class MetaplaceSearchFilterForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = "get"

        self.helper.layout = Layout(
            Row(
                Div(Column("name", css_class="col-sm-12"), css_class="form-group"),
                Div(Column("type", css_class="col-sm-12"), css_class="form-group"),
            ),
            Submit("submit", _("Rechercher"), css_class="btn-primary btn-block btn-sm"),
        )

        super().__init__(*args, **kwargs)


class PlaceForm(forms.ModelForm):
    SRID = (
        (4326, _("WGS84 (srid=4326) - default")),
        (2154, _("LAMBERT93 (srid=2154)")),
    )

    srid = forms.ChoiceField(choices=SRID, label=_("Coordinate system"))

    class Meta:
        model = Place
        fields = (
            "name",
            "metaplace",
            "is_hidden",
            "authorized_user",
            "precision",
            "type",
            "domain",
            "geom",
            "proprietary",
            "is_gite",
            "x",
            "y",
            "convention",
            "map_file",
            "is_managed",
            "convention_file",
            "photo_file",
            "bdsource",
            "id_bdsource",
            "comment",
        )
        widgets = {
            "geom": LeafletWidget(),
            "authorized_user": autocomplete.ModelSelect2Multiple(url="api:all_user_autocomplete"),
            "metaplace": autocomplete.ModelSelect2(url="api:metaplace_autocomplete"),
        }
        readonly_fields = "id_place"

    def __init__(self, *args, **kwargs):
        # input_sm_helper(self)
        logger.debug("POSTITION PlaceForm > __init__")
        super().__init__(*args, **kwargs)
        metaplace = kwargs.pop("id_metaplace", None)
        if metaplace:
            del self.fields["id_metaplace"]

        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = "submit"
        self.helper.form_show_errors = True
        self.helper.layout = Layout(
            Row(
                Column(
                    Submit(
                        "gotoSession",
                        _("Enregistrer et ajouter une session"),
                        css_class="btn-success btn-sm",
                    ),
                    Submit(
                        "submit",
                        _("Enregistrer et quitter"),
                        css_class="btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group right",
                    role="button",
                )
            ),
            Accordion(
                PrimaryAccordionGroup(
                    "Informations principales",
                    Fieldset(
                        _("Caractéristiques principales"),
                        Row(
                            Column(
                                Column("name", css_class="col-xs-12 col-md-4"),
                                Column("precision", css_class="col-xs-6 col-md-4"),
                                Column("domain", css_class="col-xs-6 col-md-4"),
                            ),
                        ),
                        Row(
                            Column("metaplace", css_class="col-lg-12"),
                        ),
                        css_class="col-lg-12",
                    ),
                    Fieldset(
                        _("Sensibilité du site"),
                        Row(
                            Column("is_hidden", css_class="12 col-md-3 col-lg-2"),
                            Column(
                                "authorized_user",
                                css_class="col-xs-12 col-md-9 col-lg-10",
                            ),
                        ),
                        css_class="col-lg-12",
                    ),
                    Fieldset(
                        _("Gite et gestion"),
                        Row(
                            Column("is_gite", css_class="col-xs-12 col-lg-4"),
                            Column("type", css_class="col-xs-12 col-lg-4"),
                            Column("is_managed", css_class="col-xs-12 col-lg-4"),
                        ),
                        css_class="col-lg-12",
                    ),
                    Fieldset(
                        _("Localisation"),
                        Row(
                            Column("x", css_class="col-lg-4"),
                            Column("y", css_class="col-lg-4"),
                            Column("srid", css_class="col-lg-4"),
                            Column("geom", css_class="col-lg-12"),
                        ),
                        css_class="col-lg-12",
                    ),
                ),
                InfoAccordionGroup(
                    "Propriété, conventions et topo",
                    Row(
                        Column("proprietary", css_class="col-xs-12 col-md-12 col-lg-4"),
                        Column("convention", css_class="col-xs-12 col-md-6 col-lg-4"),
                        Column("convention_file", css_class="col-xs-12 col-md-6 col-lg-4"),
                    ),
                ),
                InfoAccordionGroup("Photo", Row(Column("photo_file", css_class="col-lg-12"))),
                InfoAccordionGroup("Plan / Topo", Row(Column("map_file", css_class="col-lg-12"))),
                InfoAccordionGroup(
                    "Source",
                    Row(
                        Column("bdsource", css_class="col-md-6 col-xs-12", readonly=True),
                        Column("id_bdsource", readonly=True, css_class="col-md-6 col-xs-12"),
                    ),
                ),
                InfoAccordionGroup("Commentaire", Row(Column("comment", css_class="col-lg-12"))),
            ),
            Row(
                Column(
                    Submit(
                        "gotoSession",
                        _("Enregistrer et ajouter une session"),
                        css_class="btn-success btn-sm",
                    ),
                    Submit(
                        "submit",
                        _("Enregistrer et quitter"),
                        css_class="btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group right",
                    role="button",
                )
            ),
        )

    def clean(self):
        cleaned_data = super().clean()
        is_gite = cleaned_data.get("is_gite")
        type_gite = cleaned_data.get("type")

        if is_gite and not type_gite:
            msg = _("Veuillez préciser le type de gîte")
            self.add_error("type", msg)


class PlaceSearchFilterForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = "get"

        self.helper.layout = Layout(
            Row(
                Column("name", css_class="col-sm-12 col-md-4"),
                Column("type", css_class="col-sm-12 col-md-4"),
                Column("metaplace", css_class="col-sm-12 col-md-4"),
                Column("areas", css_class="col-sm-12 col-md-6 col-lg-6"),
            ),
            Submit("submit", _("Rechercher"), css_class="btn-primary btn-sm"),
        )

        super().__init__(*args, **kwargs)


class PlaceSearchAdvancedFilterForm(forms.Form):
    class Meta:
        widgets = {
            "area": autocomplete.Select2Multiple(url="api:area_autocomplete"),
        }

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = "post"

        self.helper.layout = Layout(
            Row(
                Column("area", css_class="col-sm-12 col-md-4"),
                Column("type", css_class="col-sm-12 col-md-4"),
                Column("metaplace", css_class="col-sm-12 col-md-4"),
                Column("is_gite", css_class="col-sm-12 col-md-4"),
            ),
            Submit("submit", _("Rechercher"), css_class="btn-primary btn-sm"),
        )

        super().__init__(*args, **kwargs)


class PlaceManagementForm(forms.ModelForm):
    class Meta:
        model = PlaceManagement
        fields = ("date", "action", "referent", "comment", "file")
        widgets = {"referent": autocomplete.ModelSelect2(url="api:all_user_autocomplete")}

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = "submit"
        # ======================================================================
        # self.fields['transmitter'].queryset = Transmitter.objects.filter(
        #     available is True)
        # ======================================================================

        self.helper.layout = Layout(
            Accordion(
                PrimaryAccordionGroup(
                    _("Caractéristiques de l'action"),
                    Row(
                        Column("date", css_class="col-lg-4 col-sm-12"),
                        Column("action", css_class="col-lg-4 col-sm-12"),
                        Column("referent", css_class="col-lg-4 col-sm-12"),
                    ),
                ),
                InfoAccordionGroup(_("Fichier"), Row(Column("file", css_class="col-lg-12"))),
                InfoAccordionGroup(
                    _("commentaire"), Row(Column("comment", css_class="col-lg-12"))
                ),
            ),
            Row(
                Column(
                    Submit("submit", _("Enregistrer"), css_class="btn-primary btn-sm"),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group",
                    role="button",
                )
            ),
        )
        super().__init__(*args, **kwargs)


class BuildForm(forms.ModelForm):
    class Meta:
        model = Build
        fields = (
            "visit_date",
            "cavity_front",
            "attic",
            "attic_access",
            "bell_tower",
            "bell_tower_screen",
            "bell_tower_access",
            "cover",
            "ext_light",
            "access_light",
            "cellar",
            "cellar_access",
            "comment",
        )

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = "submit"
        self.helper.layout = Layout(
            Row(
                Column(
                    Submit("submit", _("Enregistrer"), css_class="btn-primary btn-sm"),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group",
                    role="button",
                )
            ),
            Accordion(
                PrimaryAccordionGroup(
                    "Informations principales",
                    Row(
                        Column("visit_date", css_class="col-lg-12"),
                        Fieldset(
                            _("Combles"),
                            Column("attic", css_class="col-lg-6"),
                            Column("attic_access", css_class="col-lg-6"),
                            css_class="col-lg-12",
                        ),
                        Fieldset(
                            _("Clocher"),
                            Column("bell_tower", css_class="col-lg-4 col-sm-6"),
                            Column("bell_tower_screen", css_class="col-lg-4 col-sm-6"),
                            Column("bell_tower_access", css_class="col-lg-4 col-sm-12"),
                            css_class="col-lg-12",
                        ),
                        Fieldset(
                            _("Caves"),
                            Column("cellar", css_class="col-lg-6"),
                            Column("cellar_access", css_class="col-lg-6"),
                            css_class="col-lg-12",
                        ),
                        Fieldset(
                            _("Autres éléments"),
                            Column("cavity_front", css_class="col-md-3 col-sm-6"),
                            Column("cover", css_class="col-md-3 col-sm-6"),
                            Column("ext_light", css_class="col-md-3 col-sm-6"),
                            Column("access_light", css_class="col-md-3 col-sm-6"),
                            css_class="col-lg-12",
                        ),
                    ),
                ),
                InfoAccordionGroup("Commentaire", Row(Column("comment", css_class="col-lg-12"))),
            ),
            Row(
                Column(
                    Submit("submit", _("Enregistrer"), css_class="btn-primary btn-sm"),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group",
                    role="button",
                )
            ),
        )

        super().__init__(*args, **kwargs)


class CaveForm(forms.ModelForm):
    access_walk_duration = forms.TimeField(
        widget=forms.TimeInput(format="%H:%M", attrs={"type": "time"}),
        required=False,
        label=_("Durée de la marche d'approche"),
    )

    class Meta:
        model = Cave
        fields = (
            "visit_date",
            "interest",
            "length",
            "altdiff",
            "n_entry",
            "equipment",
            "access_walk_duration",
            "comment",
        )

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = "submit"
        self.helper.layout = Layout(
            Row(
                Column(
                    Submit("submit", _("Enregistrer"), css_class="btn-primary btn-sm"),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group",
                    role="button",
                )
            ),
            Accordion(
                PrimaryAccordionGroup(
                    _("Informations principales"),
                    Row(
                        Column("visit_date", css_class="col-md-6 col-sm-6"),
                        Column("interest", css_class="col-md-6 col-sm-6"),
                        Fieldset(
                            _("Description"),
                            Column("length", css_class="col-lg-3 col-sm-6"),
                            Column("altdiff", css_class="col-lg-3 col-sm-6"),
                            Column("n_entry", css_class="col-lg-3 col-sm-6"),
                            Column("access_walk_duration", css_class="col-lg-3 col-sm-6"),
                            Column("equipment", css_class="col-lg-12"),
                            css_class="col-lg-12",
                        ),
                    ),
                ),
                InfoAccordionGroup(
                    _("Commentaire"), Row(Column("comment", css_class="col-lg-12"))
                ),
            ),
            Row(
                Column(
                    Submit("submit", _("Enregistrer"), css_class="btn-primary btn-sm"),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group",
                    role="button",
                )
            ),
        )

        super().__init__(*args, **kwargs)


class BridgeForm(forms.ModelForm):
    class Meta:
        model = Bridge
        fields = (
            "visit_date",
            "interest",
            "renovated",
            "renovated_date",
            "joint",
            "rift",
            "expansion",
            "drain",
            "cornice",
            "volume",
            "comment",
        )

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = "submit"
        self.helper.layout = Layout(
            Row(
                Column(
                    Submit("submit", _("Enregistrer"), css_class="btn-primary btn-sm"),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group",
                    role="button",
                )
            ),
            Accordion(
                PrimaryAccordionGroup(
                    _("Informations principales"),
                    Row(
                        Column("visit_date", css_class="col-md-6"),
                        Column("interest", css_class="col-md-6"),
                        Fieldset(
                            "Historique de l'ouvrage",
                            Column("renovated", css_class="col-lg-6 col-md-4 col-sm-12"),
                            Column(
                                "renovated_date",
                                css_class="col-lg-6 col-md-4 col-sm-12",
                            ),
                            css_class="col-lg-12",
                        ),
                        Fieldset(
                            "Types de gîtes",
                            Column("joint", css_class="col-sm-12 col-md-4"),
                            Column("rift", css_class="col-sm-12 col-md-4"),
                            Column("expansion", css_class="col-sm-12 col-md-4"),
                            Column("drain", css_class="col-sm-12 col-md-4"),
                            Column("cornice", css_class="col-sm-12 col-md-4"),
                            Column("volume", css_class="col-sm-12 col-md-4"),
                            css_class="col-lg-12",
                        ),
                    ),
                ),
                InfoAccordionGroup(
                    _("Commentaire"), Row(Column("comment", css_class="col-lg-12"))
                ),
            ),
            Row(
                Column(
                    Submit("submit", _("Enregistrer"), css_class="btn-primary btn-sm"),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group",
                    role="button",
                )
            ),
        )

        super().__init__(*args, **kwargs)


class TreeForm(forms.ModelForm):
    class Meta:
        model = Tree
        fields = (
            "visit_date",
            "context",
            "forest_stands",
            "situation",
            "circumstance",
            "tree_specie",
            "health",
            "tree_diameter",
            "standing",
            "protected",
            "bat_specie",
            "gite_type",
            "gite_origin",
            "gite_localisation",
            "comment",
        )
        widgets = {
            "visit_date": forms.DateInput,
            "gite_type": forms.CheckboxSelectMultiple,
            "gite_origin": forms.CheckboxSelectMultiple,
            "gite_localisation": forms.CheckboxSelectMultiple,
            "bat_specie": autocomplete.ModelSelect2Multiple(url="api:taxa_autocomplete"),
        }

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = "submit"
        self.helper.layout = Layout(
            Row(
                Column(
                    Submit("submit", _("Enregistrer"), css_class="btn-primary btn-sm"),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group",
                    role="button",
                )
            ),
            Accordion(
                PrimaryAccordionGroup(
                    "Informations principales",
                    Row(
                        Column("visit_date", css_class="col-lg-12"),
                        Fieldset(
                            _("Contexte"),
                            Column("context", css_class="col-lg-6 col-sm-12"),
                            Column("forest_stands", css_class="col-lg-6 col-sm-12"),
                            Column("circumstance", css_class="col-lg-6 col-sm-12"),
                            Column("situation", css_class="col-lg-6 col-sm-12"),
                            css_class="col-lg-12",
                        ),
                        Fieldset(
                            _("Caractéristiques de l'arbre"),
                            Column("tree_specie", css_class="col-lg-4 col-md-12"),
                            Column("health", css_class="col-lg-4 col-md-6"),
                            Column("tree_diameter", css_class="col-lg-4 col-md-6"),
                            Column("standing", css_class="col-lg-6"),
                            Column("protected", css_class="col-lg-6"),
                            css_class="col-lg-12",
                        ),
                        Fieldset(
                            _("Occupation"),
                            Column("bat_specie", css_class="col-lg-12"),
                            css_class="col-lg-12",
                        ),
                        Fieldset(
                            _("Caractérisation des gîtes"),
                            Column("gite_type", css_class="col-lg-6 col-sm-12"),
                            Column("gite_origin", css_class="col-lg-6 col-sm-12"),
                            Column("gite_localisation", css_class="col-lg-6 col-sm-12"),
                            css_class="col-lg-12",
                        ),
                    ),
                ),
                InfoAccordionGroup(
                    _("Commentaire"), Row(Column("comment", css_class="col-lg-12"))
                ),
            ),
            Row(
                Column(
                    Submit("submit", _("Enregistrer"), css_class="btn-primary btn-sm"),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group",
                    role="button",
                )
            ),
        )

        super().__init__(*args, **kwargs)


class TreeGiteForm(forms.ModelForm):
    class Meta:
        model = TreeGite
        fields = (
            "bat_specie",
            "gite_type",
            "gite_origin",
            "gite_localisation",
            "gite_high",
            "gite_tree_diameter",
            "gite_access_orientation",
            "gite_access_size",
            "comment",
        )
        widgets = {"bat_specie": autocomplete.ModelSelect2Multiple(url="api:taxa_autocomplete")}

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = "submit"
        self.helper.layout = Layout(
            Row(
                Column(
                    Submit("submit", _("Enregistrer"), css_class="btn-primary btn-sm"),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group",
                    role="button",
                )
            ),
            Accordion(
                PrimaryAccordionGroup(
                    _("Informations principales"),
                    Row(
                        Fieldset(_("Occupation"), Column("bat_specie", css_class="col-lg-12")),
                        Fieldset(
                            _("Caractéristiques du gîte"),
                            Column("gite_type", css_class="col-lg-3 col-md-3 col-sm-6"),
                            Column("gite_origin", css_class="col-lg-3 col-md-3 col-sm-6"),
                            Column(
                                "gite_localisation",
                                css_class="col-lg-3 col-md-3 col-sm-6",
                            ),
                            Column("gite_high", css_class="col-lg-3 col-md-3 col-sm-6"),
                            Column(
                                "gite_tree_diameter",
                                css_class="col-lg-3 col-md-3 col-sm-6",
                            ),
                            Column(
                                "gite_access_orientation",
                                css_class="col-lg-3 col-md-3 col-sm-6",
                            ),
                            Column(
                                "gite_access_size",
                                css_class="col-lg-3 col-md-3 col-sm-6",
                            ),
                        ),
                    ),
                ),
                InfoAccordionGroup(
                    _("Commentaire"), Row(Column("comment", css_class="col-lg-12"))
                ),
            ),
            Row(
                Column(
                    Submit("submit", _("Enregistrer"), css_class="btn-primary btn-sm"),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group",
                    role="button",
                )
            ),
        )

        super().__init__(*args, **kwargs)


class SessionForm(forms.ModelForm):
    class Meta:
        model = Session
        fields = (
            "id_session",
            "contact",
            "date_start",
            "time_start",
            "date_end",
            "data_file",
            "study",
            "is_confidential",
            "time_end",
            "main_observer",
            "other_observer",
            "comment",
        )
        widgets = {
            "id_session": forms.HiddenInput(),
            "date_end": forms.DateInput(),
            "date_start": forms.DateInput(),
            "main_observer": autocomplete.ModelSelect2(url="api:all_user_autocomplete"),
            "other_observer": autocomplete.ModelSelect2Multiple(url="api:all_user_autocomplete"),
            "place": autocomplete.ModelSelect2(url="api:place_autocomplete"),
            "study": autocomplete.ModelSelect2(url="api:study_autocomplete"),
        }
        error_messages = {
            NON_FIELD_ERRORS: {
                "unique_together": "%(model_name)s's %(field_labels)s are not unique."
            }
        }

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = "submit"
        self.base_fields["contact"].empty_label = "Choisissez un type de contact"
        self.base_fields["contact"].initial = 0
        place_id = kwargs.pop("place_id", None)
        if place_id:
            cancel_url = reverse("sights:place_detail", kwargs={"pk": place_id})
        self.helper.layout = Layout(
            Row(
                Column(
                    Submit(
                        "gotoObs",
                        _("Enregistrer et ajouter des observations"),
                        css_class="btn-success btn-sm",
                    ),
                    Submit(
                        "submit",
                        _("Enregistrer et quitter"),
                        css_class="btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick='window.location.href="{}"'.format(cancel_url),
                    ),
                    css_class="col-lg-12 btn-group",
                    role="button",
                )
            ),
            Accordion(
                PrimaryAccordionGroup(
                    "Informations principales",
                    Row(
                        Column(
                            Column("contact", css_class="col-lg-6 col-sm-12"),
                            Column("main_observer", css_class="col-lg-6 col-sm-12"),
                        ),
                    ),
                    Row(
                        Column("other_observer", css_class="col-lg-12"),
                    ),
                    Row(
                        Column("date_start", css_class="col-lg-3 col-sm-6"),
                        Column("time_start", css_class="col-lg-3 col-sm-6"),
                        Column("date_end", css_class="col-lg-3 col-sm-6"),
                        Column("time_end", css_class="col-lg-3 col-sm-6"),
                    ),
                    Row(
                        Column("is_confidential", css_class="col-lg-3 col-sm-6"),
                        Column("study", css_class="col-lg-9 col-sm-6"),
                    ),
                ),
                InfoAccordionGroup(
                    "Charger un fichier",
                    Row(Column("data_file", css_class="col-lg-12")),
                ),
                InfoAccordionGroup("Commentaire", Row(Column("comment", css_class="col-lg-12"))),
            ),
            Row(
                Column(
                    Submit(
                        "gotoObs",
                        _("Enregistrer et ajouter des observations"),
                        css_class="btn-success btn-sm",
                    ),
                    Submit(
                        "submit",
                        _("Enregistrer et quitter"),
                        css_class="btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick='window.location.href="{}"'.format(cancel_url),
                    ),
                    css_class="col-lg-12 btn-group",
                    role="button",
                )
            ),
        )

        super().__init__(*args, **kwargs)


class SessionChangePlaceForm(forms.ModelForm):
    class Meta:
        model = Session
        fields = ("place",)
        widgets = {"place": autocomplete.ModelSelect2(url="api:place_autocomplete")}
        error_messages = {
            NON_FIELD_ERRORS: {
                "unique_together": "%(model_name)s's %(field_labels)s are not unique."
            }
        }

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = "submit"
        self.helper.layout = Layout(
            Row(
                Column(
                    Submit("submit", _("Enregistrer"), css_class="btn-primary btn-sm"),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group",
                    role="button",
                )
            ),
            Accordion(
                DangerAccordionGroup(
                    "Informations principales",
                    Row(Column("place", css_class="col-lg-12")),
                )
            ),
            Row(
                Column(
                    Submit("submit", _("Enregistrer"), css_class="btn-primary btn-sm"),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group",
                    role="button",
                )
            ),
        )

        super().__init__(*args, **kwargs)


class DeviceForm(forms.ModelForm):
    class Meta:
        model = Device
        fields = ("ref", "type", "height", "width", "context", "photo_file", "comment")

    def __init__(self, *args, **kwargs):
        contact = kwargs.pop("contact", None)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = "submit"
        self.helper.layout = Layout(
            Row(
                Column(
                    Submit("submit", _("Enregistrer"), css_class="btn-primary btn-sm"),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group",
                    role="button",
                )
            ),
            Accordion(
                PrimaryAccordionGroup(
                    "Dispositif",
                    Row(
                        Column("ref", css_class="col-lg-12"),
                        Column("type", css_class="col-lg-4 col-sm-6"),
                        Column("height", css_class="col-lg-4 col-sm-6"),
                        Column("width", css_class="col-lg-4 col-sm-6"),
                        Column("context", css_class="col-lg-12 col-sm-12"),
                    ),
                ),
                InfoAccordionGroup(
                    "Photo du dispositif",
                    Row(Column("photo_file", css_class="col-lg-12")),
                ),
                InfoAccordionGroup("Commentaire", Row(Column("comment", css_class="col-lg-12"))),
            ),
            Row(
                Column(
                    Submit("submit", _("Enregistrer"), css_class="btn-primary btn-sm"),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group",
                    role="button",
                )
            ),
        )

        super().__init__(*args, **kwargs)
        self.fields["type"].queryset = TypeDevice.objects.filter(contact=contact)


class SessionLinkForm(forms.ModelForm):
    class Meta:
        model = SessionLink
        fields = ("type_link", "link", "comment")
        error_messages = {
            NON_FIELD_ERRORS: {
                "unique_together": "%(model_name)s's %(field_labels)s are not unique."
            }
        }

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = "submit"
        self.helper.layout = Layout(
            Row(
                Column(
                    Submit("submit", _("Enregistrer"), css_class="btn-primary btn-sm"),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group",
                    role="button",
                )
            ),
            Accordion(
                PrimaryAccordionGroup(
                    "Informations",
                    Row(Column("type_link", css_class="col-lg-12")),
                    Row(Column("link", css_class="col-lg-12")),
                    Row(Column("comment", css_class="col-lg-12")),
                )
            ),
            Row(
                Column(
                    Submit("submit", _("Enregistrer"), css_class="btn-primary btn-sm"),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group",
                    role="button",
                )
            ),
        )

        super().__init__(*args, **kwargs)


class SightingForm(forms.ModelForm):
    class Meta:
        model = Sighting
        fields = (
            "codesp",
            "total_count",
            "breed_colo",
            "is_doubtful",
            "bdsource",
            "id_bdsource",
            "comment",
        )
        widgets = {
            "geom": LeafletWidget(),
            "codesp": autocomplete.ModelSelect2(url="api:taxa_autocomplete"),
        }
        error_messages = {
            NON_FIELD_ERRORS: {
                "unique_together": "%(model_name)s's %(field_labels)s are not unique."
            }
        }

    def __init__(self, *args, **kwargs):
        # main_observer = kwargs.pop("main_observer", None)
        # other_observer = kwargs.pop("other_observer", None)
        session_id = kwargs.pop("session_id", None)
        contact = kwargs.pop("contact", None)
        if session_id:
            cancel_url = reverse("sights:session_detail", kwargs={"pk": session_id})
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = "submit"
        self.helper.layout = Layout(
            Row(
                Column(
                    Submit(
                        "gotoCount",
                        _("Enregistrer et ajouter des détails"),
                        css_class="btn btn-success btn-sm",
                    ),
                    Submit(
                        "_addanother",
                        _("Enregistrer et ajouter une autre observation"),
                        css_class="btn btn-info btn-sm",
                        css_id="_addanother",
                    ),
                    Submit(
                        "submit",
                        _("Enregistrer et quitter"),
                        css_class="btn btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        "Annuler",
                        css_class="btn-warning",
                        onclick='window.location.href="{}"'.format(cancel_url),
                    ),
                    css_class="col-lg-12 btn-group btn-group-sm right",
                    role="button",
                )
            ),
            Accordion(
                PrimaryAccordionGroup(
                    "Espèces",
                    Row(
                        Column("codesp", css_class="col-lg-6 col-sm-12"),
                        Column("total_count", css_class="col-lg-6 col-sm-12"),
                        Column("breed_colo", css_class="col-lg-6 col-sm-12"),
                        Column("is_doubtful", css_class="col-lg-6 col-sm-12"),
                    ),
                ),
                InfoAccordionGroup(
                    "Autres informations",
                    Row(
                        Column("bdsource", css_class="col-md-6 col-sm-12", readonly=True),
                        Column("id_bdsource", readonly=True, css_class="col-lg-6 col-sm-12"),
                    ),
                ),
                InfoAccordionGroup("Commentaire", Row(Column("comment", css_class="col-lg-12"))),
            ),
            Row(
                Column(
                    Submit(
                        "gotoCount",
                        _("Enregistrer et ajouter des détails"),
                        css_class="btn btn-success btn-sm",
                    ),
                    Submit(
                        "_addanother",
                        _("Enregistrer et ajouter une autre observation"),
                        css_class="btn btn-info btn-sm",
                        css_id="_addanother",
                    ),
                    Submit(
                        "submit",
                        _("Enregistrer et quitter"),
                        css_class="btn btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        "Annuler",
                        css_class="btn-warning",
                        onclick='window.location.href="{}"'.format(cancel_url),
                    ),
                    css_class="col-lg-12 btn-group btn-group-sm right",
                    role="button",
                )
            ),
        )

        super().__init__(*args, **kwargs)
        if contact in ("du", "vm"):
            self.fields["total_count"].widget.attrs["readonly"] = True


class CountDetailBiomForm(forms.ModelForm):
    class Meta:
        model = CountDetail
        fields = (
            "sex",
            "age",
            "time",
            "device",
            "method",
            "manipulator",
            "validator",
            "transmitter",
            "ab",
            "d5",
            "d3",
            "pouce",
            "queue",
            "tibia",
            "pied",
            "cm3",
            "tragus",
            "poids",
            "testicule",
            "epididyme",
            "tuniq_vag",
            "gland_taille",
            "gland_coul",
            "mamelle",
            "gestation",
            "epiphyse",
            "chinspot",
            "usure_dent",
            "comment",
        )

    def __init__(self, *args, **kwargs):
        contact = kwargs.pop("contact", None)
        id_session = kwargs.pop("session", None)
        sighting_id = kwargs.pop("sighting_id", None)
        if sighting_id:
            cancel_url = reverse("sights:sighting_detail", kwargs={"pk": sighting_id})
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = "submit"
        self.helper.layout = Layout(
            Row(
                Column(
                    Submit(
                        "_addanother",
                        _("Ajouter un autre détail"),
                        css_class="btn btn-info btn-sm",
                        css_id="_addanother",
                    ),
                    Submit(
                        "submit",
                        _("Enregistrer et quitter"),
                        css_class="btn btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        "Annuler",
                        css_class="btn-warning",
                        onclick='window.location.href="{}"'.format(cancel_url),
                    ),
                    css_class="col-lg-12 btn-group btn-group-sm right",
                    role="button",
                )
            ),
            Accordion(
                PrimaryAccordionGroup(
                    "Critères visuels et mesures",
                    Row(
                        Column(
                            Fieldset(
                                "Elements relatifs à l'observation",
                                Column("manipulator", css_class="col-lg-6 col-sm-6"),
                                Column("validator", css_class="col-lg-6 col-sm-6"),
                                Column("time", css_class="col-lg-4 col-sm-6"),
                                Column("device", css_class="col-lg-4 col-sm-6"),
                                Column("method", css_class="col-lg-4 col-sm-6 end"),
                                css_class="col-lg-12",
                            ),
                            Fieldset(
                                "Elements relevés",
                                Column("sex", css_class="col-lg-6"),
                                Column("age", css_class="col-lg-6"),
                                css_class="col-lg-12",
                            ),
                            Fieldset(
                                _("Mesures biométriques"),
                                Column("ab", css_class="col-lg-2 col-md-4 col-sm-6"),
                                Column("d5", css_class="col-lg-2 col-md-4 col-sm-6"),
                                Column("d3", css_class="col-lg-2 col-md-4 col-sm-6"),
                                Column("pouce", css_class="col-lg-2 col-md-4 col-sm-6"),
                                Column("queue", css_class="col-lg-2 col-md-4 col-sm-6"),
                                Column("tibia", css_class="col-lg-2 col-md-4 col-sm-6"),
                                Column("pied", css_class="col-lg-2 col-md-4 col-sm-6"),
                                Column("cm3", css_class="col-lg-2 col-md-4 col-sm-6"),
                                Column("tragus", css_class="col-lg-2 col-md-4 col-sm-6"),
                                Column("poids", css_class="col-lg-2 col-md-4 col-sm-6 end"),
                                css_class="col-lg-12",
                            ),
                            Fieldset(
                                _("Relevés visuels"),
                                Column("testicule", css_class="col-lg-2 col-md-4 col-sm-6"),
                                Column("epididyme", css_class="col-lg-2 col-md-4 col-sm-6"),
                                Column("tuniq_vag", css_class="col-lg-2 col-md-4 col-sm-6"),
                                Column(
                                    "gland_taille",
                                    css_class="col-lg-2 col-md-4 col-sm-6",
                                ),
                                Column("gland_coul", css_class="col-lg-2 col-md-4 col-sm-6"),
                                Column("mamelle", css_class="col-lg-2 col-md-4 col-sm-6"),
                                Column("gestation", css_class="col-lg-2 col-md-4 col-sm-6"),
                                Column("epiphyse", css_class="col-lg-2 col-md-4 col-sm-6"),
                                Column("chinspot", css_class="col-lg-2 col-md-4 col-sm-6"),
                                Column(
                                    "usure_dent",
                                    css_class="col-lg-2 col-md-4 col-sm-6 end",
                                ),
                                css_class="col-lg-12",
                            ),
                        )
                    ),
                ),
                InfoAccordionGroup(
                    "Emetteur de télémétrie",
                    Row(Column("transmitter", css_class="col-lg-12")),
                ),
                InfoAccordionGroup("Commentaire", Row(Column("comment", css_class="col-lg-12"))),
            ),
            Row(
                Column(
                    Submit(
                        "_addanother",
                        _("Ajouter un autre détail"),
                        css_class="btn btn-info btn-sm",
                        css_id="_addanother",
                    ),
                    Submit(
                        "submit",
                        _("Enregistrer et quitter"),
                        css_class="btn btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        "Annuler",
                        css_class="btn-warning",
                        onclick='window.location.href="{}"'.format(cancel_url),
                    ),
                    css_class="col-lg-12 btn-group btn-group-sm right",
                    role="button",
                )
            ),
        )

        super().__init__(*args, **kwargs)
        self.fields["method"].queryset = Method.objects.filter(contact=contact)
        session = Session.objects.get(id_session=id_session)
        self.fields["manipulator"].queryset = (
            get_user_model()
            .objects.filter(
                Q(id__in=session.other_observer.all()) | Q(id=session.main_observer_id)
            )
            .distinct()
        )
        self.fields["validator"].queryset = (
            get_user_model()
            .objects.filter(
                Q(id__in=session.other_observer.all()) | Q(id=session.main_observer_id)
            )
            .distinct()
        )
        self.fields["device"].queryset = Device.objects.filter(session=id_session)
        self.fields["transmitter"].queryset = Transmitter.objects.filter(available=True)


class CountDetailAcousticForm(forms.ModelForm):
    class Meta:
        model = CountDetail
        fields = ("time", "method", "count", "unit", "comment")

    def __init__(self, *args, **kwargs):
        contact = kwargs.pop("contact", None)
        sighting_id = kwargs.pop("sighting_id", None)
        if sighting_id:
            cancel_url = reverse("sights:sighting_detail", kwargs={"pk": sighting_id})
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = "submit"
        self.helper.layout = Layout(
            Row(
                Column(
                    Submit(
                        "_addanother",
                        _("Ajouter un autre détail"),
                        css_class="btn btn-info btn-sm",
                        css_id="_addanother",
                    ),
                    Submit(
                        "submit",
                        _("Enregistrer et quitter"),
                        css_class="btn btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        "Annuler",
                        css_class="btn-warning",
                        onclick='window.location.href="{}"'.format(cancel_url),
                    ),
                    css_class="col-lg-12 btn-group btn-group-sm right",
                    role="button",
                )
            ),
            Accordion(
                PrimaryAccordionGroup(
                    "Elements relatifs à l'Observation",
                    Row(
                        Column(
                            Fieldset(
                                "Heure et éléments biologiques",
                                Column("method", css_class="col-lg-6 col-sm-12"),
                                Column("time", css_class="col-lg-6 col-sm-12"),
                                css_class="col-lg-12",
                            ),
                            Fieldset(
                                "Comptage",
                                Column("count", css_class="col-lg-6 col-sm-6"),
                                Column("unit", css_class="col-lg-6 col-sm-6"),
                                css_class="col-lg-12",
                            ),
                        )
                    ),
                ),
                InfoAccordionGroup("Commentaire", Row(Column("comment", css_class="col-lg-12"))),
            ),
            Row(
                Column(
                    Submit(
                        "_addanother",
                        _("Ajouter un autre détail"),
                        css_class="btn btn-info btn-sm",
                        css_id="_addanother",
                    ),
                    Submit(
                        "submit",
                        _("Enregistrer et quitter"),
                        css_class="btn btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        "Annuler",
                        css_class="btn-warning",
                        onclick='window.location.href="{}"'.format(cancel_url),
                    ),
                    css_class="col-lg-12 btn-group btn-group-sm right",
                    role="button",
                )
            ),
        )

        super().__init__(*args, **kwargs)
        self.fields["method"].queryset = Method.objects.filter(contact=contact)
        self.fields["unit"].queryset = CountUnit.objects.filter(contact=contact)


class CountDetailOtherForm(forms.ModelForm):
    class Meta:
        model = CountDetail
        fields = (
            "time",
            "method",
            "sex",
            "age",
            "count",
            "unit",
            "precision",
            "transmitter",
            "comment",
        )

    def __init__(self, *args, **kwargs):
        contact = kwargs.pop("contact", None)
        sighting_id = kwargs.pop("sighting_id", None)
        if sighting_id:
            cancel_url = reverse("sights:sighting_detail", kwargs={"pk": sighting_id})
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = "submit"

        self.helper.layout = Layout(
            Row(
                Column(
                    Submit(
                        "_addanother",
                        _("Ajouter un autre détail"),
                        css_class="btn btn-info btn-sm",
                        css_id="_addanother",
                    ),
                    Submit(
                        "submit",
                        _("Enregistrer et quitter"),
                        css_class="btn btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        "Annuler",
                        css_class="btn-warning",
                        onclick='window.location.href="{}"'.format(cancel_url),
                    ),
                    css_class="col-lg-12 btn-group btn-group-sm right",
                    role="button",
                )
            ),
            Accordion(
                PrimaryAccordionGroup(
                    "Elements relatifs à l'Observation",
                    Row(
                        Column(
                            Fieldset(
                                "Heure et éléments biologiques",
                                Column("method", css_class="col-lg-6 col-sm-12"),
                                Column("time", css_class="col-lg-6 col-sm-12"),
                                Column("sex", css_class="col-lg-6"),
                                Column("age", css_class="col-lg-6"),
                                css_class="col-lg-12",
                            ),
                            Fieldset(
                                "Comptage",
                                Column("count", css_class="col-lg-4 col-sm-12"),
                                Column("unit", css_class="col-lg-4 col-sm-6"),
                                Column("precision", css_class="col-lg-4 col-sm-6"),
                                css_class="col-lg-12",
                            ),
                            css_class="secondary",
                        )
                    ),
                ),
                InfoAccordionGroup(
                    "Emetteur de télémétrie",
                    Row(Column("transmitter", css_class="col-lg-12")),
                ),
                InfoAccordionGroup("Commentaire", Row(Column("comment", css_class="col-lg-12"))),
            ),
            Row(
                Column(
                    Submit(
                        "_addanother",
                        _("Ajouter un autre détail"),
                        css_class="btn btn-info btn-sm",
                        css_id="_addanother",
                    ),
                    Submit(
                        "submit",
                        _("Enregistrer et quitter"),
                        css_class="btn btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        "Annuler",
                        css_class="btn-warning",
                        onclick='window.location.href="{}"'.format(cancel_url),
                    ),
                    css_class="col-lg-12 btn-group btn-group-sm right",
                    role="button",
                )
            ),
        )

        super().__init__(*args, **kwargs)
        self.fields["transmitter"].queryset = Transmitter.objects.filter(available=True)
        self.fields["method"].queryset = Method.objects.filter(contact=contact)
        self.fields["unit"].queryset = CountUnit.objects.filter(contact=contact)
        self.fields["precision"].queryset = CountPrecision.objects.filter(contact=contact)


class CountDetailTelemetryForm(forms.ModelForm):
    class Meta:
        model = CountDetail
        fields = ("time", "method", "transmitter", "comment")

    def __init__(self, *args, **kwargs):
        contact = kwargs.pop("contact", None)
        sighting_id = kwargs.pop("sighting_id", None)
        if sighting_id:
            cancel_url = reverse("sights:sighting_detail", kwargs={"pk": sighting_id})
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = "submit"
        self.helper.layout = Layout(
            Row(
                Column(
                    Submit(
                        "_addanother",
                        _("Ajouter un autre détail"),
                        css_class="btn btn-info btn-sm",
                        css_id="_addanother",
                    ),
                    Submit(
                        "submit",
                        _("Enregistrer et quitter"),
                        css_class="btn btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        "Annuler",
                        css_class="btn-warning",
                        onclick='window.location.href="{}"'.format(cancel_url),
                    ),
                    css_class="col-lg-12 btn-group btn-group-sm right",
                    role="button",
                )
            ),
            Accordion(
                PrimaryAccordionGroup(
                    "Elements relatifs à l'Observation",
                    Row(
                        Column("transmitter", css_class="col-xs-12 col-md-4"),
                        Column("method", css_class="col-xs-12 col-md-4"),
                        Column("time", css_class="col-xs-12 col-md-4"),
                    ),
                ),
                InfoAccordionGroup("Commentaire", Row(Column("comment", css_class="col-lg-12"))),
            ),
            Row(
                Column(
                    Submit(
                        "_addanother",
                        _("Ajouter un autre détail"),
                        css_class="btn btn-info btn-sm",
                        css_id="_addanother",
                    ),
                    Submit(
                        "submit",
                        _("Enregistrer et quitter"),
                        css_class="btn btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        "Annuler",
                        css_class="btn-warning",
                        onclick='window.location.href="{}"'.format(cancel_url),
                    ),
                    css_class="col-lg-12 btn-group btn-group-sm right",
                    role="button",
                )
            ),
        )

        super().__init__(*args, **kwargs)
        self.fields["transmitter"].queryset = Transmitter.objects.filter(available=True)
        self.fields["method"].queryset = Method.objects.filter(contact=contact)
