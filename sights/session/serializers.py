from rest_framework.serializers import ModelSerializer
from rest_framework_gis.serializers import GeoFeatureModelSerializer, GeometrySerializerMethodField

from accounts.serializer import ProfileSearchSerializer
from dicts.models import Contact, Specie
from dicts.serializers import DictsGenericSerializer
from management.models import Study
from sights.place.serializers import SimplePlaceSerializer

from ..models import Session, Sighting


class SightingSpecie(ModelSerializer):
    class Meta:
        model = Specie
        fields = (
            "codesp",
            "sci_name",
            "common_name_fr",
            "sp_true",
        )


class ContactSerializer(DictsGenericSerializer):
    class Meta:
        model = Contact
        fields = ("id", "code", "descr")


class StudySerializer(ModelSerializer):
    class Meta:
        model = Study
        fields = ("pk", "uuid", "name")


class SimpleObservationSerializer(ModelSerializer):
    codesp = SightingSpecie()

    class Meta:
        model = Sighting
        depth = 1
        fields = (
            "id_sighting",
            "codesp",
            "breed_colo",
            "total_count",
            "period",
            "is_doubtful",
            "comment",
        )


class SessionSerializer(ModelSerializer):
    place = SimplePlaceSerializer()
    contact = ContactSerializer()
    main_observer = ProfileSearchSerializer()
    other_observer = ProfileSearchSerializer(many=True)
    observations = SimpleObservationSerializer(many=True)
    study = StudySerializer()

    class Meta:
        model = Session
        fields = (
            "id_session",
            "name",
            "contact",
            "date_start",
            "time_start",
            "date_end",
            "time_end",
            "main_observer",
            "other_observer",
            "place",
            "observations",
            "comment",
            "study",
        )


class SessionGeoSerializer(GeoFeatureModelSerializer, SessionSerializer):
    geom = GeometrySerializerMethodField()

    class Meta(SessionSerializer.Meta):
        geo_field = "geom"

    def get_geom(self, session):
        return session.place.geom
