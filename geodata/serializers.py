from rest_framework_gis import serializers

from dicts.models import AreaType

from .models import Areas


class AreaTypeSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = AreaType
        fields = ("code", "name")


class AreasSimpleSerializer(serializers.ModelSerializer):
    area_type = AreaTypeSimpleSerializer()

    class Meta:
        model = Areas
        fields = ("id", "area_type", "code", "name")


class AreaGeomSerializer(serializers.GeoFeatureModelSerializer):
    area_type = AreaTypeSimpleSerializer()

    class Meta:
        model = Areas
        geo_field = "geom"
        fields = ("id", "area_type", "code", "name")
