import os

from django.contrib.gis.utils import LayerMapping

from .models import Areas

territory_mapping = {
    "id": "id",
    "name": "name",
    "code": "code",
    "geom": "geom",
}

territory_shp = os.path.abspath(
    os.path.join(os.path.dirname(__file__), "initial_data/shapefiles", "territory.shp"),
)


def run(verbose=True):
    lm = LayerMapping(
        Areas,
        territory_shp,
        territory_mapping,
        transform=False,
        encoding="utf-8",
    )
    lm.save(strict=True, verbose=verbose)
