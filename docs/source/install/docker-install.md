(docker-install)=

# Installation sous Docker

**Prerequis**:

- Un serveur linux 64 bits avec un OS récent (testé sous ubuntu >= 22.04, debian >= 10 )

## Installation de Docker

### Installation de Docker community edition

- Pour Debian: <https://docs.docker.com/install/linux/debian>
- Pour Ubuntu: <https://docs.docker.com/install/linux/ubuntu>

## Préparation des fichiers

Téléchargez les fichiers suivants depuis le dépot
    - `.env.sample`
    - `docker-compose.production.yaml`
    - `docker/nginx/nginx.conf`


```bash
mkdir dbchiroweb
cd dbchiroweb
curl -o docker-compose.yaml https://framagit.org/dbchiro/dbchiroweb/-/raw/master/docker-compose.production.yaml
curl -o .env https://framagit.org/dbchiro/dbchiroweb/-/raw/master/.env.sample
curl -o nginx.conf https://framagit.org/dbchiro/dbchiroweb/-/raw/master/docker/nginx/nginx.conf
```

Editez le fichier `.env`.

```bash
editor .env
```

```{literalinclude} ../../.env.sample
:language: ini
```

D'autres paramétrages sont tout à fait possible, cf. le fichier de configuration
Python avec toutes les variables déclarées dans les fonctions `config("MA_VAR", ...)`.


```{literalinclude} ../../dbchiro/settings.py
:language: python
```

## Lancemant de l'application

```bash
docker compose up -d
```

Vous pouvez en suivre l'avancement avec la commande :

```bash
docker compose logs -f
```
