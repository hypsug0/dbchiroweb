.. _`docker-install`:

************************
Installation sous Docker
************************

**Prerequis**:

* Un serveur linux 64 bits avec un OS récent (testé sous ubuntu >= 18.04, debian >= 9 )
* Un émulateur de terminal pour la connexion via ssh (natif sous Linux et osX, `Putty <http://www.chiark.greenend.org.uk/~sgtatham/putty/>`_ ou `mobaXterm <https://mobaxterm.mobatek.net/>`_ sous windows)

Installation de Docker
======================

Installation de Docker community edition
----------------------------------------

* Pour Debian: https://docs.docker.com/install/linux/debian
* Pour Ubuntu: https://docs.docker.com/install/linux/ubuntu

Lancer dbChiroWeb en mode développement
=======================================

Copier et éditer le fichier ``.env`` à partir de l'exemple ``.env.sample``.

.. code-block:: bash

    cp .env.sample .env
    editor .env

Lancez le docker-compose

.. code-block:: bash

    docker compose up --build

Lancer dbChiroWeb en mode développement
=======================================

copiez les fichiers ``docker-compose.production.yaml`` et ``.env.sample``. Renommez le fichier ``.env`` en ``.env`` et éditez le:

.. code-block:: bash

    cp .env.sample .env
    editor .env

Au besoin, adaptez le docker-compose et lancez la stack

.. code-block:: bash

    docker compose -f docker-compose.production.yaml up -d


Vous pouvez en suivre l'avancement avec la commande ``docker logs -f dbchiro_app``.
