.. dbchiro[web] documentation master file, created by
   sphinx-quickstart on Thu Jan  4 11:39:13 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to dbchiro[web]'s documentation!
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   classic-install
   docker-install
   update


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
* :ref:`classic-install`
* :ref:`docker-install`
