(contribute)=

# How to contribute

Preferable way to contribute to project is by using fork
et pull requests.

1. Create your own copy of project using (fork)
2. Create an issue to inform community on main repository
3. Create a branch to fix the issue et enjoy coding
4. Commit your changes. Once your devs are over, you can purpose an new pull request on main repository, destination should be `develop` branch. You can also anticipate your pull request by create your pulll request as draft.

Thanks for your contributions

## Creating a development environment

You can develop by using `docker compose`.

To do this, just copy `.env.sample` to `.env` and `docker-compose.dev.yaml` to `docker-compose.override.yaml`, edit `.env` file and
start app.

```bash
cp .env.sample .env
cp docker-compose.dev.yaml docker-compose.override.yaml
editor .env
```

Start apps (database, docs, app)

```bash
docker compose up
```

App is available at [http://127.0.0.1:8000](http://127.0.0.1:8000), docs at [http://127.0.0.1:8800](http://127.0.0.1:8800)

This are some helpfull commands using `docker compose`:

* Migrate database: `docker compose run --rm --entrypoint='' -it dbchiro python -m manage migrate`
* Create superuser: `docker compose run --rm --entrypoint='' -it dbchiro python -m manage createsuperuser`
* Migrate database: `docker compose run --rm --entrypoint='' -it dbchiro python -m manage loaddata dicts.xml sinp_dict_data_v1.0.json nomenclatures_dbchiro.json inpn_nomenclatures_organisms.json`
* Install geo areas: `docker compose run --rm --entrypoint='' -it dbchiro python -m manage loaddata dep.xml.gz mun.xml.gz g10x10.xml.gz`
