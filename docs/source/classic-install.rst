.. _`classic-install`:

************
Installation
************

L'installation de `dbChiro[web] <http://dbchiro.org>`_ a été uniquement testée sur serveur Debian Jessie en production, Debian Stretch en développement. Ce tutoriel d'installation ne traitera donc que de ces distributions.

**Prerequis**:

* Un serveur Debian Jessie ou Debian Stretch 64 bits
* Un émulateur de terminal pour la connexion via ssh (natif sous Linux, `Putty <http://www.chiark.greenend.org.uk/~sgtatham/putty/>`_ ou `mobaXterm <https://mobaxterm.mobatek.net/>`_ sous windows)

Installation de l'environnement
===============================

Installation de PostgreSQL, de PostGIS et de l'environnement python
-------------------------------------------------------------------

Dans un terminal, lancez les commandes suivantes:

.. code-block:: sh

    # Ajout des dépots officiels de PostgreSQL et PostGIS ainsi que Backports de Debian
    sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
    sudo sh -c 'echo "deb http://ftp.debian.org/debian $(lsb_release -cs)-backports main" > /etc/apt/sources.list.d/backports.list'
    sudo apt-get install wget ca-certificates
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
    # Mise à jour de la bibliothèque de paquest et mise à jour du système
    sudo apt-get update
    sudo apt-get upgrade
    # Installation des paquets requis pour dbchiroweb
    sudo apt-get install git python3 python3-pip virtualenv python3-virtualenv postgresql-11 postgresql-11-postgis-2.5 postgresql-11-postgis-2.5-scripts postgis
    # Installation des paquets requis pour le serveur apache
    sudo apt-get install python3-pip apache2 libapache2-mod-wsgi-py3
    # Installation des paquets requis pour la connexion https avec lets-encrypt (remplacer stretch-backports par jessie-backports pour Debian 8 Jessie)
    sudo apt-get install python-certbot-apache -t stretch-backports

Clonage du dépot dbChiro[web]
=============================

Méthode recommandée pour faciliter les mises à jour.

Dans cet exemple, dbchiroweb sera installé dans le dossier `/var/www/dbchiro`

.. code-block:: sh

    mkdir /var/www/dbchiro
    cd /var/www/dbchiro
    git clone https://framagit.org/dbchiro/dbchiroweb.git

Création de l'environnement virtuel
===================================

.. code-block:: sh

    cd /var/www/dbchiro
    virtualenv -p python3 venv
    source venv/bin/activate
    cd /var/www/dbchiro/dbchiroweb
    pip install -r requirements.txt

Création de la base de donnée et du superutilisateur de la base de donnée
=========================================================================

.. code-block:: sh

    # en tant que root
    su postgres
    psql

.. code:: psql

    CREATE DATABASE dbchirodb;
    CREATE ROLE dbchiropguser LOGIN SUPERUSER ENCRYPTED PASSWORD 'motdepasse';
    ALTER DATABASE dbchirodb OWNER TO dbchiropguser;
    -- Se connecter à la base de donnée avec la commande \c
    \c dbchirodb
    CREATE EXTENSION postgis;
    CREATE EXTENSION "uuid-ossp";
    -- Quitter psql
    \q


Configuration de dbChiro[web]
=============================

Copiez le fichier ``dbchiro/settings/configuration/config.py.sample`` en le renommant ``config.py`` dans ce même dossier

.. code-block:: sh

    cd /var/www/dbchiro/dbchiroweb
    cp dbchiro/settings/configuration/config.py.sample dbchiro/settings/configuration/config.py
    nano dbchiro/settings/configuration/config.py


.. literalinclude:: ../../dbchiro/settings/configuration/config.py.sample
    :language: python


**La répartition des périodes est la suivante**

+-----------------------------------------+----------+-------------+-----------+----------+
|Jour de l'année                          | 335      | 60          | 136       | 228      |
+-----------------------------------------+----------+-------------+-----------+----------+
|date                                     | 01/12    | 01/03       | 16/05     | 16/08    |
+=========================================+==========+=============+===========+==========+
|Hivernage / wintering (w)                |     ≥    |     <       |           |          |
+-----------------------------------------+----------+-------------+-----------+----------+
|Transit printanier / sping transit (st)  |          |     ≥       |     <     |          |
+-----------------------------------------+----------+-------------+-----------+----------+
|Estivage / Summering (e)                 |          |             |     ≥     |    <     |
+-----------------------------------------+----------+-------------+-----------+----------+
|Transit automnal / Automn transit (ta)   |     <    |             |     ≥     |          |
+-----------------------------------------+----------+-------------+-----------+----------+


Initialisation de la base de données
====================================

Pour ce projet, deux configurations de ``settings`` sont possibles:

* ``production`` pour le serveur en production
* ``dev`` pour le développement. Avec notamment le mode de débogage et l'appli django-debug-toolbar

Les deux peuvent être utilisés pour les commandes suivantes. Cette exemple utilisera la configuration ``dev``.

.. code-block:: sh

    cd /var/www/dbchiro/dbchiroweb
    source ../venv/bin/activate
    # Création des fichiers d'initialisation de la base de donnée
    python manage.py makemigrations --settings=dbchiro.settings.dev accounts blog dicts geodata management sights
    # Création de la structure de la base de donnée (à partir des fichiers créées la commande
    python manage.py migrate --settings=dbchiro.settings.dev
    # Création du dossier 'static' et importation des fichiers nécessaire au fonctionnement de l'interface graphique (css, js, polices).
    python manage.py collectstatic  --settings=dbchiro.settings.dev
    # Création du premier utilisateur, administrateur du site
    python manage.py createsuperuser --settings=dbchiro.settings.dev
    # Lancement du serveur
    python manage.py runserver --settings=dbchiro.settings.dev

Le terminal devrait alors renvoyer le résultat suivant:

.. code::

    Performing system checks...

    System check identified no issues (0 silenced).
    January 17, 2018 - 22:29:11
    Django version 1.11.3, using settings 'dbchiro.settings.dev'
    Starting development server at http://127.0.0.1:8000/
    Quit the server with CONTROL-C.

Vous pouvez donc tester l'application à cette adresse : http://127.0.0.1:8000/

Pour la tester à distance, il faut utiliser un tunnel ssh entre la machine distante et la machine locale.
Sur Linux ou osX, cela se fait tout simplement avec la commande suivante (8000 est le port par défaut de django):

.. code-block:: sh

    ssh -p portssh utilisateurserver@adresseserver -L 8000:localhost:8000

Depuis Windows, cela peut se faire avec Putty: http://www.eila.univ-paris-diderot.fr/sysadmin/windows/tunnels-putty

Intégration des données initiales (dictionnaires et geodata)
============================================================

L'étape suivante consiste à intégrer à la base de donnée les principales données nécessaires à son fonctionnemnt:
* les dictionnaires (application ``dicts``)
* les données géographiques (application ``geodata``)

Intégration des données de dictionnaire
---------------------------------------

Exécution du fichier `datas/sql/dicts_french.sql` avec la commande suivante:

.. code-block:: sh



Intégration des données cartographiques
---------------------------------------
Les données fournies sont en projection Lambert93 (epsg 2154). Il convient de les adapter à la projection de la base de donnée. Cela est réalisé par la commande d'importation suivante.
Plus d'informations sur cette commande ici: http://bostongis.com/pgsql2shp_shp2pgsql_quickguide.bqg

.. code-block:: sh

    cd /var/www/dbchiro/dbchiroweb
    python manage.py loaddata --settings=dbchiro.settings.dev datas/geodata.json.gz


Vous pouvez aussi éventuellemnet charger vos propres données (si structures de tables similaires) via les commandes suivantes:

.. code-block:: sh

    shp2pgsql -s 2154:4326 -a datas/shapefiles/territory.shp public.geodata_territory | psql -h localhost -U dbchiro  dbchirodb
    shp2pgsql -s 2154:4326 -a datas/shapefiles/municipality.shp public.geodata_municipality | psql -h localhost -p 5432 -U dbchiro  dbchirodb

Personalisation du logo
-----------------------
Le logo du bandeau supérieur du site est à installer dans ``core/static/img/logo_site.png``. Le logo du projet est fourni par défaut. Pour l'installer, il suffit d'éxécuter la commande suivante:

.. code-block:: sh

    cd /var/www/dbchiro/dbchiroweb
    cp core/static/img/logo_site.png.sample static/img/logo_site.png

Installation des fichiers statiques
-----------------------------------

Ce sont les fichiers ``*.css``, ``*.js`` et les images qui font le thème et le fonctionnement de l'interface graphique. pour les installer, il faut éxécuter la commande suivante dans le dossier de l'application:

.. code-block:: sh

    cd /var/www/dbchiro/dbchiroweb
    source ../venv/bin/activate
    python ./manage.py collectstatic --settings=dbchiro.settings.dev

Configuration du serveur apache
===============================

Crééez le fichier de configuration du serveur apache. Cela par du principe que le domaine, ou sous-domaine, dispose d'une redirection vers l'adresse IP du présent serveur.
Plus d'informations à cette adresse: https://docs.djangoproject.com/fr/1.11/howto/deployment/wsgi/modwsgi/

.. code-block:: sh

    # Activation du module wsgi
    a2enmod wsgi
    # Copiez l'exemple de fichier de configuration apache pour dbChiro dans le dossier des sites disponibles d'apache et adaptez le à votre installation.
    cd /var/www/dbchiro/dbchiroweb
    cp dbchiro/settings/configuration/apache_dbchiro.conf.sample /etc/apache2/sites-available/dbchiro.conf

Voici à quoi il ressemble:

.. code:: apache

    # Il est important de conserver WSGIDaemonProcess en dehors de <VirtualHost ...></VirtualHost> pour l'utilisation de letsencrypt
    WSGIDaemonProcess dbchiro python-home=/var/www/dbchiro/venv python-path=/var/www/dbchiro/dbchiroweb
    <VirtualHost moninstance.dbchiro.org:80>
        ServerName moninstance.dbchiro.org
        ServerAdmin admin@dbchiro.org
        Alias /static /var/www/dbchiro/dbchiroweb/dbchiro/static
        WSGIProcessGroup dbchiro
        WSGIScriptAlias / /var/www/dbchiro/dbchiroweb/dbchiro/wsgi.py
        <Directory /var/www/dbchiro/dbchiroweb/dbchiro>
            <files wsgi.py>
                Require all granted
            </files>
        </Directory>
        CustomLog /var/www/dbchiro/custom.log combined
        ErrorLog /var/www/dbchiro/error.log
    </VirtualHost>

Activez cette configuration avec la commande suivante:

.. code-block:: sh

    a2ensite dbchiro.conf
    service apache2 restart

Configuration du serveur nginx
===============================

Copiez/collez le fichier suivant dans /etc/nginx/conf.d et adaptez le

.. code-block:: sh

    cp dbchiro/settings/configuration/nginx_dbchiro.conf.sample /etc/nginx/conf.d/dbchiro.conf

Modifiez-le pour correspondre à votre configuration, vérifiez-le avec la commande ``nginx -t`` et si tout est ok, relancez nginx, avec la commande ``systemctl restart nginx``

Voici à quoi il ressemble:

.. code-block:: js

    upstream dbchiro {
        server 127.0.0.1:8000;
    }

    server {

        listen 80;

        server_name http://moninstance.dbchiro.org;

        location / {
            proxy_pass http://dbchiro;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header Host $host;
            proxy_redirect off;
        }

        location /static/ {
            alias /pathToApp/static/;
        }

        location /media/ {
            alias /pathToApp/media/;
        }

    }


Sécurisation HTTPS avec Let's encrypt
=====================================


Pour finir l'installation, il faut paramétrer le serveur pour l'utilisation de la connexion sécurisée https avec les certificat fournis par letsencrypt.
Lors de l'affichage des éléments suivants, choisissez l'option 2:

.. code-block:: sh

    certbot-auto -d moninstance.dbchiro.org


.. code-block:: none

    Please choose whether or not to redirect HTTP traffic to HTTPS, removing HTTP access.
    -------------------------------------------------------------------------------
    1: No redirect - Make no further changes to the webserver configuration.
    2: Redirect - Make all requests redirect to secure HTTPS access. Choose this for
    new sites, or if you're confident your site works on HTTPS. You can undo this
    change by editing your web server's configuration.
    -------------------------------------------------------------------------------




Pour renouveler automatiquement vos certificats, ajoutez la ligne suivante à votre  ``cron`` en lançant la commande crontab :

``0 2 * * 1 /usr/local/sbin/certbot-auto renew >> /var/log/le-renew.log``

.. code-block:: sh

    crontab -e

Vous devriez maintenant accéder à votre plateforme depuis le nom de domaine renseigné.

Connectez vous avec votre compte superuser précédemment créé.
