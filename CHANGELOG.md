# version 20180423

## Mise à jour du paramètre modèle ̀`sights.device`

   * Correction des paramètres `default='None'` des champs photo_file et context vers `default=None`.
   * Correction de la table d'affichage des périphériques à la fiche de session.
   * Patch de mise à jour de la base de donnée mettant les valeurs `None` à `NULL` dans la table `public.sights_device`
