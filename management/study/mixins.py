"""
    Study Mixins
"""
from django.shortcuts import redirect
from django.urls import reverse


class StudyAuthMixin:
    """
    Classe mixin de vérification que l'utilisateur possède les droits de créer le compte
    """

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect("%s?next=%s" % (reverse("auth_login"), request.path))
        else:
            self.obj = self.get_object()
            loggeduser = request.user
            if (
                loggeduser.edit_all_data
                or loggeduser.access_all_data
                or loggeduser.is_resp
                or loggeduser.is_staff
                or loggeduser.is_superuser
                or self.obj.project_manager == loggeduser
            ):
                return super().dispatch(request, *args, **kwargs)
            else:
                return redirect("core:view_unauth")
