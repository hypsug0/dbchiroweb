import django_tables2 as tables

from .models import CatchAuth, Study, Transmitter


class StudyTable(tables.Table):
    ACTIONS = """
                <div class="btn-group btn-group-xs">
                    <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Action <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a data-toggle="modal"
                               data-target=".bs-study-study-modal-md-{{ record.pk }}" title="Historique de la donnée"><i
                                class="fi-clock"></i> Historique de la donnée</a></li>
                        <li><a href="{% url 'management:study_update' record.pk %}" title="Modifier"><i class="fa fa-fw fa-edit"></i> Modifier</a>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li style="color:red;"><a href="{% url 'management:study_delete' record.pk %}" title="Supprimer"><i
                                class="fi-trash"></i> Supprimer</a></li>
                    </ul>
                </div>
                <div class="modal fade bs-study-study-modal-md-{{ record.pk }}" tabindex="-1" role="dialog"
                     aria-labelledby="history">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                                </button>
                                <h4 class="modal-title" id="exampleModalLabel">Historique</h4>
                            </div>
                            <div class="modal-body">
                                <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
                                    {% if record.timestamp_update %}
                                        et mise à jour le {{ record.timestamp_update }}
                                    {% endif %}
                                    par {{ record.updated_by }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                """
    LINK_NAME = '<a href="{% url "management:url_study_detail" record.pk %}" target="_blank" title="Voir">{{record.name}}'
    actions = tables.TemplateColumn(ACTIONS, verbose_name="Actions", orderable=False)
    link_name = tables.TemplateColumn(LINK_NAME, verbose_name="Nom", orderable=True)

    class Meta:
        model = Study
        template = "table_bootstrap.html"
        attrs = {"class": "table table-striped table-condensed"}
        fields = (
            "actions",
            "link_name",
            "uuid",
            "year",
            "project_manager",
            "type_etude",
            "type_espace",
            "public_funding",
            "public_report",
            "public_raw_data",
            "confidential",
            "confidential_end_data",
            "timestamp_create",
            "timestamp_update",
            "created_by",
            "comment",
        )


class TransmitterTable(tables.Table):
    ACTIONS = """
                <div class="btn-group btn-group-xs">
                    <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Action <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a data-toggle="modal"
                               data-target=".bs-transmitter-history-modal-md-{{ record.pk }}" title="Historique de la donnée"><i
                                class="fi-clock"></i> Historique de la donnée</a></li>
                        <li><a href="{% url 'management:transmitter_update' record.pk %}" title="Modifier"><i class="fa fa-fw fa-edit"></i> Modifier</a>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li style="color:red;"><a href="{% url 'management:transmitter_delete' record.pk %}" title="Supprimer"><i
                                class="fi-trash"></i> Supprimer</a></li>
                    </ul>
                </div>
                <div class="modal fade bs-transmitter-history-modal-md-{{ record.pk }}" tabindex="-1" role="dialog"
                     aria-labelledby="history">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                                </button>
                                <h4 class="modal-title" id="exampleModalLabel">Historique</h4>
                            </div>
                            <div class="modal-body">
                                <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
                                    {% if record.timestamp_update %}
                                        et mise à jour le {{ record.timestamp_update }}
                                    {% endif %}
                                    par {{ record.updated_by }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                """
    COMMENT = """
        {% if record.comment %}
        <button type="button" class="btn btn-primary btn-xs" data-toggle="modal"
                data-target=".bs-transm-comment-modal-md-{{ record.pk }}">
            <i class="fa fa-comment" aria-hidden="true"></i>
        </button>
        <div class="modal fade bs-transm-comment-modal-md-{{ record.pk }}" tabindex="-1" role="dialog"
             aria-labelledby="comment">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-fw fa-comment"></i>Commentaire</h4>
                    </div>
                    <div class="modal-body">
                        <p>{{ record.comment|safe }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        {% endif %}
    """
    actions = tables.TemplateColumn(ACTIONS, verbose_name="Actions", orderable=False)
    comment = tables.TemplateColumn(COMMENT, verbose_name="Commentaire", orderable=False)

    class Meta:
        model = Transmitter
        template = "table_bootstrap.html"
        attrs = {"class": "table table-striped table-condensed"}
        fields = (
            "actions",
            "name",
            "reference",
            "frequency",
            "weight",
            "autonomy",
            "model",
            "brand",
            "owner",
            "buying_date",
            "last_recond_date",
            "status",
            "available",
            "comment",
        )


class CatchAuthTable(tables.Table):
    ACTIONS = """
                <div class="btn-group btn-group-xs">
                    <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Action <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a data-toggle="modal"
                               data-target=".bs-catchauth-history-modal-md-{{ record.pk }}" title="Historique de la donnée"><i
                                class="fi-clock"></i> Historique de la donnée</a></li>
                        <li><a href="{% url 'management:catchauth_update' record.pk %}" title="Modifier"><i class="fa fa-fw fa-edit"></i> Modifier</a>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li style="color:red;"><a href="{% url 'management:catchauth_delete' record.pk %}" title="Supprimer"><i
                                class="fi-trash"></i> Supprimer</a></li>
                    </ul>
                </div>
                <div class="modal fade bs-catchauth-history-modal-md-{{ record.pk }}" tabindex="-1" role="dialog"
                     aria-labelledby="history">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                                </button>
                                <h4 class="modal-title" id="exampleModalLabel">Historique</h4>
                            </div>
                            <div class="modal-body">
                                <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
                                    {% if record.timestamp_update %}
                                        et mise à jour le {{ record.timestamp_update }}
                                    {% endif %}
                                    par {{ record.updated_by }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                """
    actions = tables.TemplateColumn(ACTIONS, verbose_name="Actions", orderable=False)

    class Meta:
        model = CatchAuth
        template = "table_bootstrap.html"
        attrs = {"class": "table table-striped table-condensed"}
        fields = (
            "actions",
            "areas",
            "official_reference",
            "date_start",
            "date_end",
            "file",
        )
