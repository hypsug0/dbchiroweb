"""
    Vues de l'application Sights
"""

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django_tables2 import SingleTableView

from core import js

from ..forms import CatchAuthForm
from ..mixins import ManagementAuthMixin
from ..models import CatchAuth
from ..tables import CatchAuthTable
from .mixins import CatchAuthCreateAuthMixin


class CatchAuthCreate(CatchAuthCreateAuthMixin, CreateView):
    """Update view for the Transmitter model."""

    model = CatchAuth
    form_class = CatchAuthForm
    file_storage = FileSystemStorage()
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fi-checkbox"
        context["title"] = _("Ajout d'un arrêté d'autorisation")
        context["js"] = js.DateInput
        return context


class CatchAuthUpdate(ManagementAuthMixin, UpdateView):
    """Update view for the Transmitter model."""

    model = CatchAuth
    form_class = CatchAuthForm
    file_storage = FileSystemStorage()
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        if form.instance.date_start > form.instance.date_end:
            messages.error(self.request, _("Cette session existe déjà"))
            return HttpResponseRedirect(self.request.path)
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fi-checkbox"
        context["title"] = _("Modification d'un arrêté d'autorisation")
        context["js"] = js.DateInput
        return context


class CatchAuthDelete(ManagementAuthMixin, DeleteView):
    """Delete view for the Transmitter model."""

    model = CatchAuth
    template_name = "confirm_delete.html"
    success_url = reverse_lazy("management:catchauth_list")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fi-trash"
        context["title"] = _("Suppression d'un arrêté d'autorisation")
        context["message_alert"] = _("Êtes-vous certain de vouloir supprimer l'arrêté")
        return context


class CatchAuthList(LoginRequiredMixin, SingleTableView):
    table_class = CatchAuthTable
    model = CatchAuth
    template_name = "regulatory_list.html"
    table_pagination = {"per_page": 25}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fi-checkbox"
        context["title"] = _("Liste des arrêtés d'autorisation")
        context[
            "js"
        ] = """
        """
        return context
