"""
    Mixins
"""
from django.contrib.auth import get_user_model
from django.shortcuts import redirect
from django.urls import reverse


class CatchAuthCreateAuthMixin:
    """
    Permission de créer un arrêté d'autorisation dans les conditions suivantes:
        L'utilisateur est superutilisateur,
        L'utilisateur peut modifier Toutes les données,
        L'utilisateur est responsable de territoire.
    """

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect("%s?next=%s" % (reverse("auth_login"), request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if loggeduser.is_superuser or loggeduser.edit_all_data or loggeduser.is_resp:
                return super().dispatch(request, *args, **kwargs)
            else:
                return redirect("core:update_unauth")
