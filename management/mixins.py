"""
    Mixins
"""
from django.contrib.auth import get_user_model
from django.shortcuts import redirect
from django.urls import reverse


class ManagementAuthMixin:
    """
    Classe mixin de vérification que l'utilisateur possède les droits de créer le compte
    """

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect("%s?next=%s" % (reverse("auth_login"), request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            obj = self.get_object()
            if (
                loggeduser.edit_all_data
                or loggeduser.access_all_data
                or loggeduser.is_resp
                or loggeduser.is_staff
                or loggeduser.is_superuser
                or loggeduser == obj.created_by
            ):
                return super().dispatch(request, *args, **kwargs)
            else:
                return redirect("core:view_unauth")
