#!/bin/python3

"""
Accounts module - Serializers
"""

from rest_framework import serializers
from sinp_nomenclatures.serializers import NomenclatureSerializer
from sinp_organisms.models import OrganismMember
from sinp_organisms.serializers import OrganismSerializer

from geodata.serializers import AreasSimpleSerializer

from .models import Profile


class ProfileSearchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ("id", "full_name")


class OrganismMemberSetSerializer(serializers.ModelSerializer):
    organism = OrganismSerializer()
    member_level = NomenclatureSerializer(many=True)

    class Meta:
        model = OrganismMember
        fields = "__all__"


class ProfileSerializer(serializers.ModelSerializer):
    resp_areas = AreasSimpleSerializer(many=True)
    organismmember_set = OrganismMemberSetSerializer(many=True, read_only=True)

    class Meta:
        model = Profile
        fields = (
            "id",
            "full_name",
            "email",
            "uuid",
            "resp_areas",
            "organisms",
            "organismmember_set",
            "is_superuser",
            "gcu_accepted",
            "access_all_data",
            "edit_all_data",
            "export_all_data",
            "username",
            "last_login",
            "is_active",
            "timestamp_create",
            "timestamp_update",
        )
