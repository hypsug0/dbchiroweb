#!/bin/python3

"""
Accounts module - Serializers
"""

from rest_framework import serializers

from geodata.serializers import AreasSimpleSerializer

from .models import Profile


class ProfileSearchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ("id", "full_name")


class ProfileSerializer(serializers.ModelSerializer):
    resp_areas = AreasSimpleSerializer(many=True)

    class Meta:
        model = Profile
        fields = (
            "id",
            "full_name",
            "email",
            "uuid",
            "resp_areas",
            "is_superuser",
            "gcu_accepted",
            "access_all_data",
            "edit_all_data",
            "export_all_data",
            "username",
            "last_login",
            "is_active",
            "timestamp_create",
            "timestamp_update",
        )
