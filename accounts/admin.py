#!/bin/python3

"""
Accounts module - Admin
"""

from django.contrib import admin
from leaflet.admin import LeafletGeoAdmin

from .models import Profile


class GeoUserAdmin(LeafletGeoAdmin):
    list_display = [
        "id",
        "username",
        "email",
        "full_name",
        "is_superuser",
        "is_staff",
        "is_resp",
        "is_active",
        "last_login",
    ]
    search_fields = ["username", "email", "first_name", "last_name"]
    list_filter = ("is_active", "is_staff", "is_superuser", "is_resp")
    exclude = ("resp_areas",)


admin.site.register(Profile, GeoUserAdmin)
