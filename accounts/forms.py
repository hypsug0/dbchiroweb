#!/bin/python3

"""
Accounts module - Forms
"""


from captcha.fields import CaptchaField
from crispy_forms.bootstrap import Accordion, Div, Field
from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, Button, ButtonHolder, Column, Fieldset, Layout, Row, Submit
from dal import autocomplete
from django.contrib.auth import get_user_model, password_validation
from django.contrib.auth.forms import PasswordChangeForm, PasswordResetForm, UserCreationForm
from django.contrib.gis import forms
from django.forms import CharField
from django.forms.widgets import HiddenInput
from django.utils.translation import gettext_lazy as _
from registration.forms import RegistrationForm

from core.forms import InfoAccordionGroup, PrimaryAccordionGroup

from .models import Profile


class UserCreateAdminForm(UserCreationForm):
    username = CharField(initial="tmpusername123")
    password_clear = CharField()

    # password_clear = CharField(
    #     max_length=50,
    #     label='Mot de passe',
    #     help_text=_('Le mot de passe prérempli est un mot de passe fort aléatoirement généré'),
    #     initial=BaseUserManager.make_random_password(12),
    # )

    class Meta:
        model = Profile
        fields = UserCreationForm.Meta.fields + (
            "first_name",
            "last_name",
            "email",
            "is_resp",
            "resp_areas",
            "access_all_data",
            "edit_all_data",
            "catchauth",
            "organism",
            "mobile_phone",
            "home_phone",
            "addr_appt",
            "addr_building",
            "addr_street",
            "addr_city",
            "addr_city_code",
            "addr_dept",
            "addr_country",
            "comment",
            "id_bdsource",
            "bdsource",
        )
        widgets = {
            "username": HiddenInput(),
            "password1": HiddenInput(),
            "password2": HiddenInput(),
            "resp_areas": autocomplete.ModelSelect2Multiple(url="api:areas_autocomplete"),
        }
        # readonly_fields = ('password_gen',)

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = "submit"
        self.helper.layout = Layout(
            Row(
                Column(
                    Submit(
                        "submit",
                        _("Enregistrer"),
                        css_class="btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group right",
                    role="button",
                )
            ),
            Accordion(
                PrimaryAccordionGroup(
                    "Informations principales",
                    Row(
                        Column(
                            Fieldset(
                                _("Identité"),
                                Row(
                                    Column("email", css_class="col-xs-12 col-lg-6"),
                                    Column(
                                        "organism",
                                        css_class="col-xs-12 col-lg-6",
                                    ),
                                    Column(
                                        "first_name",
                                        css_class="col-xs-12 col-lg-6",
                                    ),
                                    Column(
                                        "last_name",
                                        css_class="col-xs-12 col-lg-6",
                                    ),
                                ),
                                Row(
                                    Field("username", type="hidden"),
                                    Field("password1", type="hidden"),
                                    Field("password2", type="hidden"),
                                    Field("password_clear", type="hidden"),
                                ),
                                css_class="col-lg-12",
                            ),
                            Fieldset(
                                _("Responsabilité et autorisations"),
                                Row(
                                    Column(
                                        "is_resp",
                                        css_class="col-xs-12 col-md-3 col-lg-2",
                                    ),
                                    Column(
                                        "resp_areas",
                                        css_class="col-xs-12 col-md-9 col-lg-10",
                                    ),
                                ),
                                Row(
                                    Column(
                                        "access_all_data",
                                        css_class="col-xs-12 col-lg-6",
                                    ),
                                    Column(
                                        "edit_all_data",
                                        css_class="col-xs-12 col-lg-6",
                                    ),
                                    Column("catchauth", css_class="col-lg-12"),
                                ),
                                css_class="col-lg-12",
                            ),
                        )
                    ),
                ),
                InfoAccordionGroup(
                    _("Coordonnées"),
                    Row(
                        Column(
                            Fieldset(
                                _("Numéros de téléphone"),
                                Column("mobile_phone", css_class="col-lg-6"),
                                Column("home_phone", css_class="col-lg-6"),
                                css_class="col-lg-12",
                            ),
                            Fieldset(
                                _("Adresse postale"),
                                Column(
                                    "addr_appt",
                                    css_class="col-lg-2 col-md-3 col-xs-6",
                                ),
                                Column(
                                    "addr_building",
                                    css_class="col-lg-2 col-md-3 col-xs-6",
                                ),
                                Column(
                                    "addr_street",
                                    css_class="col-lg-8 col-md-6 col-xs-12",
                                ),
                                Column("addr_city", css_class="col-lg-6 col-md-12"),
                                Column(
                                    "addr_city_code",
                                    css_class="col-lg-3 col-md-12",
                                ),
                                Column("addr_dept", css_class="col-lg-3 col-md-12"),
                                Column("addr_country", css_class="col-lg-12"),
                                css_class="col-lg-12",
                            ),
                        )
                    ),
                ),
                InfoAccordionGroup(
                    "Source",
                    Row(
                        Column(
                            "bdsource",
                            css_class="col-md-6 col-xs-12",
                            readonly=True,
                        ),
                        Column(
                            "id_bdsource",
                            css_class="col-md-6 col-xs-12",
                            readonly=True,
                        ),
                    ),
                ),
                InfoAccordionGroup("Commentaire", Row(Column("comment", css_class="col-lg-12"))),
            ),
            Row(
                Column(
                    Submit(
                        "submit",
                        _("Enregistrer"),
                        css_class="btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group right",
                    role="button",
                )
            ),
        )
        self.helper.form_show_errors = True

        super().__init__(*args, **kwargs)


class UserCreateForm(UserCreationForm):
    username = CharField(initial="tmpusername123")
    password_clear = CharField()

    # password_clear = CharField(
    #     max_length=50,
    #     label='Mot de passe',
    #     help_text=_('Le mot de passe prérempli est un mot de passe fort aléatoirement généré'),
    #     initial=BaseUserManager.make_random_password(12),
    # )

    class Meta:
        model = Profile
        fields = UserCreationForm.Meta.fields + (
            "first_name",
            "last_name",
            "email",
            "catchauth",
            "organism",
            "mobile_phone",
            "home_phone",
            "addr_appt",
            "addr_building",
            "addr_street",
            "addr_city",
            "addr_city_code",
            "addr_dept",
            "addr_country",
            "comment",
            "id_bdsource",
            "bdsource",
        )
        widgets = {
            "username": HiddenInput(),
            "password1": HiddenInput(),
            "password2": HiddenInput(),
            "resp_areas": autocomplete.ModelSelect2Multiple(url="api:areas_autocomplete"),
        }
        # readonly_fields = ('password_gen',)

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = "submit"
        self.helper.layout = Layout(
            Row(
                Column(
                    Submit(
                        "submit",
                        _("Enregistrer"),
                        css_class="btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group right",
                    role="button",
                )
            ),
            Accordion(
                PrimaryAccordionGroup(
                    "Informations principales",
                    Row(
                        Column(
                            Fieldset(
                                _("Identité"),
                                Row(
                                    Column("email", css_class="col-xs-12 col-lg-6"),
                                    Column(
                                        "organism",
                                        css_class="col-xs-12 col-lg-6",
                                    ),
                                    Column(
                                        "first_name",
                                        css_class="col-xs-12 col-lg-6",
                                    ),
                                    Column(
                                        "last_name",
                                        css_class="col-xs-12 col-lg-6",
                                    ),
                                ),
                                Row(
                                    Field("username", type="hidden"),
                                    Field("password1", type="hidden"),
                                    Field("password2", type="hidden"),
                                    Field("password_clear", type="hidden"),
                                ),
                                css_class="col-lg-12",
                            ),
                            Fieldset(
                                _("Responsabilité et autorisations"),
                                Row(Column("catchauth", css_class="col-lg-12")),
                                css_class="col-lg-12",
                            ),
                        )
                    ),
                ),
                InfoAccordionGroup(
                    _("Coordonnées"),
                    Row(
                        Column(
                            Fieldset(
                                _("Numéros de téléphone"),
                                Column("mobile_phone", css_class="col-lg-6"),
                                Column("home_phone", css_class="col-lg-6"),
                                css_class="col-lg-12",
                            ),
                            Fieldset(
                                _("Adresse postale"),
                                Column(
                                    "addr_appt",
                                    css_class="col-lg-2 col-md-3 col-xs-6",
                                ),
                                Column(
                                    "addr_building",
                                    css_class="col-lg-2 col-md-3 col-xs-6",
                                ),
                                Column(
                                    "addr_street",
                                    css_class="col-lg-8 col-md-6 col-xs-12",
                                ),
                                Column("addr_city", css_class="col-lg-6 col-md-12"),
                                Column(
                                    "addr_city_code",
                                    css_class="col-lg-3 col-md-12",
                                ),
                                Column("addr_dept", css_class="col-lg-3 col-md-12"),
                                Column("addr_country", css_class="col-lg-12"),
                                css_class="col-lg-12",
                            ),
                        )
                    ),
                ),
                InfoAccordionGroup(
                    "Source",
                    Row(
                        Column(
                            "bdsource",
                            css_class="col-md-6 col-xs-12",
                            readonly=True,
                        ),
                        Column(
                            "id_bdsource",
                            css_class="col-md-6 col-xs-12",
                            readonly=True,
                        ),
                    ),
                ),
                InfoAccordionGroup("Commentaire", Row(Column("comment", css_class="col-lg-12"))),
            ),
            Row(
                Column(
                    Submit(
                        "submit",
                        _("Enregistrer"),
                        css_class="btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group right",
                    role="button",
                )
            ),
        )
        self.helper.form_show_errors = True

        super().__init__(*args, **kwargs)


class UserRegisterForm(RegistrationForm):
    captcha = CaptchaField()
    # gcu_accepted = forms.BooleanField(initial=False, required=True)
    field_order = [
        "username",
        "email",
        "last_name",
        "first_name",
        "password1",
        "password2",
        "gcu_accepted",
        "mobile_phone",
        "home_phone",
        "addr_appt",
        "addr_building",
        "addr_street",
        "addr_city",
        "addr_city_code",
        "addr_dept",
        "addr_country",
        "comment",
        "captcha",
    ]

    class Meta:
        model = Profile
        fields = RegistrationForm.Meta.fields + (
            "first_name",
            "last_name",
            "email",
            "gcu_accepted",
            "mobile_phone",
            "home_phone",
            "addr_appt",
            "addr_building",
            "addr_street",
            "addr_city",
            "addr_city_code",
            "addr_dept",
            "addr_country",
            "comment",
        )
        help_texts = {
            "gcu_accepted": _(
                "Je reconnais avoir lu et accepté les conditions générales d'"
                "utilisations <a href='/media/cgu.pdf'>CGU</a>"
            ),
        }
        # readonly_fields = ('password_gen',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["gcu_accepted"].required = True


# class PasswordUpdateForm(PasswordChangeForm):
#
#     def __init__(self, *args, **kwargs):
#         self.helper = FormHelper()
#         # self.helper.form_method = 'post'
#         self.helper.layout = Layout(
#             Row(
#                 Column(
#                     'old_password', css_class='col-xs-12 col-md-12 col-lg-4'),
#                 Column('new_password1',
#                        css_class='col-xs-12 col-md-6 col-lg-4'),
#                 Column('new_password1',
#                        css_class='col-xs-12 col-md-6 col-lg-4'),
#             ),
#             ButtonHolder(
#                 Submit('submit', 'Enregistrer', css_class='button large'),
#             ),
#         )
#
#         self.helper.form_action = 'submit'
#         self.helper.form_show_errors = True
#
#         super().__init__(*args, **kwargs)


class UserUpdateAdminForm(forms.ModelForm):
    class Meta:
        model = Profile
        exclude = ["username"]
        fields = UserCreationForm.Meta.fields + (
            "first_name",
            "last_name",
            "email",
            "organism",
            "is_resp",
            "resp_areas",
            "access_all_data",
            "edit_all_data",
            "export_all_data",
            "catchauth",
            "mobile_phone",
            "home_phone",
            "addr_appt",
            "addr_building",
            "addr_street",
            "addr_city",
            "addr_city_code",
            "addr_dept",
            "addr_country",
            "comment",
            "id_bdsource",
            "bdsource",
        )
        widgets = {"resp_areas": autocomplete.ModelSelect2Multiple(url="api:areas_autocomplete")}

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.layout = Layout(
            Row(
                Column(
                    Submit(
                        "submit",
                        _("Enregistrer"),
                        css_class="btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group right",
                    role="button",
                )
            ),
            Accordion(
                PrimaryAccordionGroup(
                    "Informations principales",
                    Row(
                        Column(
                            Fieldset(
                                _("Caractéristiques principales"),
                                Column("email", css_class="col-xs-12 col-lg-6"),
                                Column("organism", css_class="col-xs-12 col-lg-6"),
                                Column(
                                    "first_name",
                                    css_class="col-xs-12 col-lg-6",
                                ),
                                Column(
                                    "last_name",
                                    css_class="col-xs-12 col-lg-6",
                                ),
                                css_class="col-lg-12",
                            ),
                            Fieldset(
                                _("Responsabilité et autorisations"),
                                Column(
                                    "is_resp",
                                    css_class="col-xs-12 col-md-3 col-lg-2",
                                ),
                                Column(
                                    "resp_areas",
                                    css_class="col-xs-12 col-md-9 col-lg-10",
                                ),
                                Column(
                                    "access_all_data",
                                    css_class="col-xs-12 col-lg-4",
                                ),
                                Column(
                                    "edit_all_data",
                                    css_class="col-xs-12 col-lg-4",
                                ),
                                Column(
                                    "export_all_data",
                                    css_class="col-xs-12 col-lg-4",
                                ),
                                Column("catchauth", css_class="col-lg-12"),
                                css_class="col-lg-12",
                            ),
                        )
                    ),
                ),
                InfoAccordionGroup(
                    _("Coordonnées"),
                    Row(
                        Column(
                            Fieldset(
                                _("Numéros de téléphone"),
                                Column("mobile_phone", css_class="col-lg-6"),
                                Column("home_phone", css_class="col-lg-6"),
                                css_class="col-lg-12",
                            ),
                            Fieldset(
                                _("Adresse postale"),
                                Column(
                                    "addr_appt",
                                    css_class="col-lg-2 col-md-3 col-xs-6",
                                ),
                                Column(
                                    "addr_building",
                                    css_class="col-lg-2 col-md-3 col-xs-6",
                                ),
                                Column(
                                    "addr_street",
                                    css_class="col-lg-8 col-md-6 col-xs-12",
                                ),
                                Column("addr_city", css_class="col-lg-6 col-md-12"),
                                Column(
                                    "addr_city_code",
                                    css_class="col-lg-3 col-md-12",
                                ),
                                Column("addr_dept", css_class="col-lg-3 col-md-12"),
                                Column("addr_country", css_class="col-lg-12"),
                                css_class="col-lg-12",
                            ),
                        )
                    ),
                ),
                InfoAccordionGroup(
                    "Source",
                    Row(
                        Column(
                            "bdsource",
                            css_class="col-md-6 col-xs-12",
                            readonly=True,
                        ),
                        Column(
                            "id_bdsource",
                            css_class="col-md-6 col-xs-12",
                            readonly=True,
                        ),
                    ),
                ),
                InfoAccordionGroup("Commentaire", Row(Column("comment", css_class="col-lg-12"))),
            ),
            Row(
                Column(
                    Submit(
                        "submit",
                        _("Enregistrer"),
                        css_class="btn-primary btn-sm",
                    ),
                    Button(
                        "cancel",
                        _("Annuler"),
                        css_class="btn-warning btn-sm",
                        onclick="history.go(-1)",
                    ),
                    css_class="col-lg-12 btn-group right",
                    role="button",
                )
            ),
        )
        self.helper.form_action = "submit"
        self.helper.form_show_errors = True

        super().__init__(*args, **kwargs)


class UserAdminUpdatePasswordForm(forms.ModelForm):
    """
    A form that lets a user change set their password without entering the old
    password
    """

    error_messages = {"password_mismatch": _("The two password fields didn't match.")}
    new_password1 = forms.CharField(
        label=_("New password"),
        widget=forms.PasswordInput,
        strip=False,
        help_text=password_validation.password_validators_help_text_html(),
    )
    new_password2 = forms.CharField(
        label=_("New password confirmation"),
        strip=False,
        widget=forms.PasswordInput,
    )

    class Meta:
        model = Profile
        fields = ("new_password1", "new_password2")
        readonly_fields = ("password_generated",)

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = "submit"
        self.helper.layout = Layout(
            Div(
                # Div(
                #     HTML(_('<h4 class="panel-title"><i class="fa fa-fw fa-key"></i> Modifier le mot de passe</h4>')),
                #     css_class="panel-heading"),
                Div(
                    Row(
                        Column("new_password1", css_class="col-lg-6 col-xs-12"),
                        Column("new_password2", css_class="col-lg-6 col-xs-12"),
                    ),
                    ButtonHolder(
                        Submit(
                            "submit",
                            "Enregistrer",
                            css_class="btn btn-lg btn-primary btn-block",
                        )
                    ),
                    css_class="panel-body",
                ),
                css_class="panel panel-default",
            )
        )
        super().__init__(*args, **kwargs)

    #
    def clean_new_password2(self, **kwargs):
        password1 = self.cleaned_data.get("new_password1")
        password2 = self.cleaned_data.get("new_password2")
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages["password_mismatch"],
                    code="password_mismatch",
                )
        password_validation.validate_password(password2)
        return password2

    def save(self, commit=True, **kwargs):
        password = self.cleaned_data["new_password1"]
        username = self.instance.username
        user = get_user_model().objects.get(username=username)
        user.set_password(password)
        user.is_active = True
        if commit:
            user.save()
        return user


class UserUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = (
            "first_name",
            "last_name",
            "email",
            "gcu_accepted",
            "catchauth",
            "mobile_phone",
            "home_phone",
            "addr_appt",
            "addr_building",
            "addr_street",
            "addr_city",
            "addr_city_code",
            "addr_dept",
            "addr_country",
            "comment",
        )
        widgets = {"resp_areas": autocomplete.ModelSelect2Multiple(url="api:areas_autocomplete")}
        help_texts = {
            "gcu_accepted": _(
                "Je reconnais avoir lu et accepté les conditions générales d'"
                "utilisations <a href='/media/cgu.pdf'>CGU</a>"
            ),
        }
        # readonly_fields = ('password_gen',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["gcu_accepted"].required = True
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.layout = Layout(
            ButtonHolder(
                Submit("submit", "Enregistrer", css_class="button"),
                css_class="text-right",
            ),
            Accordion(
                PrimaryAccordionGroup(
                    "Informations principales",
                    Fieldset(
                        _("Caractéristiques principales"),
                        Row(
                            Column(
                                "email",
                                css_class="col-xs-12 col-md-12 col-lg-4",
                            ),
                            Column(
                                "first_name",
                                css_class="col-xs-12 col-md-6 col-lg-4",
                            ),
                            Column(
                                "last_name",
                                css_class="col-xs-12 col-md-6 col-lg-4",
                            ),
                            Column(
                                "gcu_accepted",
                                css_class="col-lg-12",
                            ),
                            Field("username", type="hidden"),
                        ),
                    ),
                    Fieldset(
                        _("Responsabilité et autorisations"),
                        Row(
                            # Column(
                            #     "is_resp",
                            #     css_class="col-xs-12 col-md-3 col-lg-2",
                            # ),
                            # Column(
                            #     "resp_areas",
                            #     css_class="col-xs-12 col-md-9 col-lg-10",
                            # ),
                            # Column(
                            #     "access_all_data",
                            #     css_class="col-xs-12 col-lg-6",
                            # ),
                            # Column("edit_all_data", css_class="col-xs-12 col-lg-6"),
                            Column("catchauth", css_class="col-lg-12"),
                        ),
                    ),
                ),
                InfoAccordionGroup(
                    _("Coordonnées"),
                    Fieldset(
                        _("Numéros de téléphone"),
                        Column("mobile_phone", css_class="col-lg-6"),
                        Column("home_phone", css_class="col-lg-6"),
                    ),
                    Fieldset(
                        _("Adresse postale"),
                        Column("addr_appt", css_class="col-lg-2 col-md-3 col-xs-6"),
                        Column(
                            "addr_building",
                            css_class="col-lg-2 col-md-3 col-xs-6",
                        ),
                        Column(
                            "addr_street",
                            css_class="col-lg-8 col-md-6 col-xs-12",
                        ),
                        Column("addr_city", css_class="col-lg-6 col-md-12"),
                        Column("addr_city_code", css_class="col-lg-3 col-md-12"),
                        Column("addr_dept", css_class="col-lg-3 col-md-12"),
                        Column("addr_country", css_class="col-lg-12"),
                    ),
                ),
                InfoAccordionGroup("Commentaire", Row(Column("comment", css_class="col-lg-12"))),
            ),
            ButtonHolder(
                Submit("submit", "Enregistrer", css_class="button"),
                css_class="text-right",
            ),
        )
        self.helper.form_action = "submit"
        self.helper.form_show_errors = True

    def clean_password(self):
        return self.initial["password"]


class PasswordUpdateForm(PasswordChangeForm):
    # this is now only needed for styling purposes
    helper = FormHelper()
    helper.form_tag = False
    helper.label_class = "col-md-5"
    helper.field_class = "col-md-7"
    helper.layout = Layout(
        "old_password",
        "new_password1",
        "new_password2",
        ButtonHolder(
            Submit("submit", "Enregistrer", css_class="button large"),
            css_class="text-right",
        ),
    )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop("user")
        super().__init__(user, *args, **kwargs)


# # ensures correct ordering of form fields
# PasswordUpdateForm.base_fields = OrderedDict(
#     (k, PasswordUpdateForm.base_fields[k])
#     for k in ['old_password', 'new_password1', 'new_password2']
# )


class PasswordResetForm(PasswordResetForm):
    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-xs-12 col-md-6 col-lg-4"
        self.helper.field_class = "col-xs-12 col-md-6 col-lg-8"
        self.helper.layout = Layout(
            "email",
            Div(
                Submit(
                    "submit",
                    "Réinitialiser le mot de passe",
                    css_class="button",
                ),
                HTML('<a class="button alert" href="/">Annuler</a>'),
                css_class="text-right",
            ),
        )


class ProfileSearchFilterForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_class = "horizontal-form"
        self.helper.form_method = "get"

        self.helper.layout = Layout(
            Row(
                Column("username", css_class="col-sm-12 col-lg-4"),
                Column("first_name", css_class="col-sm-12 col-lg-4"),
                Column("last_name", css_class="col-sm-12 col-lg-4"),
            ),
            Submit("submit", _("Rechercher"), css_class="btn-primary btn-sm"),
        )

        super().__init__(*args, **kwargs)
