#!/bin/python3

"""
Accounts module - API
"""

import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.db.models.query import QuerySet
from django.shortcuts import get_object_or_404
from django.utils.translation import gettext_lazy as _
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status
from rest_framework.filters import SearchFilter
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from core.mixins import SmallResultsSetPagination
from sights.models import Session

from .mixins import ManageAccountAuthMixin
from .models import Profile
from .serializer import ProfileSearchSerializer, ProfileSerializer

# Get an instance of a logger
logger = logging.getLogger(__name__)


def filter_user(qs: QuerySet, q: str) -> QuerySet:
    return qs.filter(
        Q(username__icontains=q)
        | Q(first_name__icontains=q)
        | Q(last_name__icontains=q)
        | Q(email__icontains=q)
    )


class ProfileAutocompleteSearch(
    LoginRequiredMixin,
    ListAPIView,
):
    pagination_class = None
    serializer_class = ProfileSearchSerializer

    def get_queryset(self):
        qs = Profile.objects

        q = self.request.query_params.get("q", None)
        id = self.request.query_params.get("id", None)
        session = self.request.query_params.get("session", None)
        logger.debug(f"ProfileAutocompleteSearch API querystring {q} {id} {session}")
        result = None
        if id:
            qs = qs.filter(id=int(id))
            result = qs.all()
        if q and not id:
            if session:
                mainobs = filter_user(Session.objects.get(pk=session).main_observer.all(), q)
                otherobs = filter_user(Session.objects.get(pk=session).other_observer.all(), q)
                result = set(mainobs + otherobs)
            else:
                qs = filter_user(qs, q)
                result = qs.order_by("full_name").order_by("is_active").all()
        logger.debug(_("ProfileAutocompleteSearch API result %s"), result)
        return result


class ProfileListApi(
    ManageAccountAuthMixin,
    LoginRequiredMixin,
    ListAPIView,
):
    serializer_class = ProfileSerializer
    queryset = Profile.objects.all().order_by("username").prefetch_related("resp_areas")
    pagination_class = SmallResultsSetPagination
    filter_backends = (
        SearchFilter,
        DjangoFilterBackend,
    )
    filterset_fields = [
        "id",
        "first_name",
        "last_name",
        "username",
        "email",
        "is_active",
        "is_resp",
        "gcu_accepted",
        "edit_all_data",
        "access_all_data",
        "export_all_data",
        "resp_areas__name",
        "created_by",
        "updated_by",
    ]
    search_fields = ["@last_name", "@first_name", "@username", "@email"]


class RevertUserActiveStatus(ManageAccountAuthMixin, LoginRequiredMixin, APIView):
    def patch(self, request, pk):
        model = get_object_or_404(Profile, pk=pk)
        data = {"is_active": not model.is_active}
        serializer = ProfileSerializer(model, data=data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
