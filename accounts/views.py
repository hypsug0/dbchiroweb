#!/bin/python3

"""
Accounts module - Views
"""


import logging

from django.conf import settings
from django.contrib.auth import get_user_model, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.mail import send_mail
from django.db.models import Q
from django.forms.models import model_to_dict
from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import DetailView
from django.views.generic.base import TemplateResponseMixin, TemplateView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from rest_framework.viewsets import ModelViewSet
from sinp_nomenclatures.models import Nomenclature
from sinp_organisms.models import Organism, OrganismMember

from core.functions import generate_username

from .forms import (
    OrganismMemberFormset,
    PasswordUpdateForm,
    UserAdminUpdatePasswordForm,
    UserCreateAdminForm,
    UserCreateForm,
    UserUpdateAdminForm,
    UserUpdateForm,
)
from .mixins import ManageAccountAuthMixin, ManageMyAccountAuthMixin
from .models import Profile
from .serializer import ProfileSerializer

logger = logging.getLogger(__name__)


class UserCreate(ManageAccountAuthMixin, CreateView):
    """
    User Creation View
    """

    model = Profile
    template_name = "leaflet_form.html"

    def get_form_class(self):
        user = self.request.user
        if user.edit_all_data or user.is_superuser:
            return UserCreateAdminForm
        return UserCreateForm

    def get_initial(self):
        initial = super().get_initial()
        initial = initial.copy()
        pwdgen = Profile.objects.make_random_password()
        initial["password_clear"] = pwdgen
        initial["password1"] = pwdgen
        initial["password2"] = pwdgen
        return initial

    def form_valid(self, form):
        form.instance.created_by = self.request.user.username
        form.instance.is_active = True
        form.instance.username = generate_username(
            form.instance.first_name, form.instance.last_name
        )
        form.instance.password1 = form.cleaned_data["password_clear"]
        form.instance.password2 = form.cleaned_data["password_clear"]

        obj = form.save()
        organism_id = form.data.get("organism")
        if organism_id:
            organism = Organism.objects.get(pk=organism_id)
            member_level_ids = form.data.getlist("member_level")
            print(f"member_level_ids {member_level_ids}")
            member_level = (
                Nomenclature.objects.filter(pk__in=member_level_ids)
                if member_level_ids
                else Nomenclature.objects.filter(type__mnemonic="member_level", code="observer")
            )
            print(member_level)
            om, _created = OrganismMember.objects.update_or_create(member=obj, organism=organism)
            om.member_level.set(member_level)
            for omd in OrganismMember.objects.filter(~Q(organism=om.organism) & Q(member=obj)):
                omd.delete()
        subject = _(f"Création de votre compte {obj.username}")
        system_sender = settings.DEFAULT_FROM_EMAIL
        real_sender = self.request.user.email
        txt_content = _(f"Identifiant: {obj.username} | mdp {form.instance.password1}")
        html_content = _(
            f"""
            <h1>Votre nouveau compte dbChiro.org</h1>
            <p><b>Identifiant</b> : {obj.username} </p>
            <p><b>Mot de passe</b> : {form.instance.password1} </p>
            <p><a href="http://{self.request.get_host()}">Connectez-vous ici</a></p>
            <p>--<br/>
            {self.request.user.full_name()}<br/>
            {self.request.user.email}</p>"""
        )
        receiptor = [obj.email, system_sender, real_sender]
        send_mail(
            subject,
            txt_content,
            system_sender,
            receiptor,
            fail_silently=True,
            html_message=html_content,
        )

        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context = super().get_context_data(**kwargs)
        user = self.request.user
        if user.edit_all_data or user.is_superuser:
            if self.request.POST:
                context["formset"] = OrganismMemberFormset(
                    self.request.POST or None,
                    self.request.FILES or None,
                    instance=self.object,
                )
            else:
                context["formset"] = OrganismMemberFormset(
                    self.request.POST or None,
                    self.request.FILES or None,
                    instance=self.object,
                )
        #

        context["icon"] = "fi-torsos"
        context["title"] = "Création d'un compte"
        context[
            "js"
        ] = """
        """
        return context

        # def get_success_url(self):
        #     return reverse_lazy('user_search')


class UserUpdate(ManageAccountAuthMixin, UpdateView):
    """
    User Creation View
    """

    model = Profile
    template_name = "leaflet_form.html"

    def get_form_class(self):
        user = self.request.user
        if user.edit_all_data or user.is_superuser:
            return UserUpdateAdminForm
        return UserUpdateForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        # user = self.request.user
        obj = self.get_object()
        om = OrganismMember.objects.filter(member=obj).first()
        if om:
            kwargs["member_level"] = om.member_level.all()
            kwargs["organism"] = om.organism
        return kwargs

    def form_valid(self, form):
        form.instance.updated_by = self.request.user.username
        form.instance.is_active = True
        obj = form.save()
        organism_id = form.data.get("organism")
        if organism_id:
            organism = Organism.objects.get(pk=organism_id)
            member_level_ids = form.data.getlist("member_level")
            print(f"member_level_ids {member_level_ids}")
            member_level = (
                Nomenclature.objects.filter(pk__in=member_level_ids)
                if member_level_ids
                else Nomenclature.objects.filter(type__mnemonic="member_level", code="observer")
            )
            print(member_level)
            om, _created = OrganismMember.objects.update_or_create(member=obj, organism=organism)
            om.member_level.set(member_level)
            for omd in OrganismMember.objects.filter(~Q(organism=om.organism) & Q(member=obj)):
                omd.delete()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fi-torsos"
        context["title"] = "Modification d'un compte"
        return context


class UserPassword(ManageAccountAuthMixin, UpdateView):
    model = Profile
    form_class = UserAdminUpdatePasswordForm
    success_url = reverse_lazy("password_reset_complete")
    template_name = "normal_form.html"
    title = _("Enter new password")

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        username = self.object.username
        logger.debug(username)
        return kwargs

    def get_success_url(self):
        return reverse("accounts:user_detail", kwargs={"pk": self.object.pk})

    def form_valid(self, form):
        form.instance.updated_by = self.request.user.username
        form.instance.is_active = True
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = _("Changement du mot de passe")
        context["icon"] = "fi-lock"
        context[
            "js"
        ] = """
        """
        return context


class MyProfileUpdate(ManageMyAccountAuthMixin, UpdateView):
    """
    User Creation View
    """

    model = Profile
    form_class = UserUpdateForm
    template_name = "leaflet_form.html"

    def form_valid(self, form):
        form.instance.updated_by = self.request.user.username
        return super().form_valid(form)

    def get_object(self):
        return get_user_model().objects.get(id=self.request.user.id)  # or request.POST

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fi-torsos"
        context["title"] = _("Modification de mon compte")
        return context


class UserDelete(ManageAccountAuthMixin, DeleteView):
    """Account delete CBV"""

    model = Profile
    template_name = "confirm_delete.html"
    success_url = reverse_lazy("accounts:user_search")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fi-trash"
        context["title"] = "Suppression d'un compte"
        context["message_alert"] = _("Êtes-vous certain de vouloir supprimer l'utilisateur")
        return context


class UserDetail(LoginRequiredMixin, DetailView):
    model = get_user_model()
    template_name = "profile.html"


class MyProfileDetail(LoginRequiredMixin, DetailView):
    model = get_user_model()
    template_name = "my_profile.html"

    def get_object(self):
        logger.info(self.request.user.id)
        return get_user_model().objects.get(id=self.request.user.id)


class MyProfileDetailExport(LoginRequiredMixin, DetailView, TemplateResponseMixin):
    model = get_user_model()
    content_type = "text/plain; charset=utf-8"
    template_name = "personnal_data.txt"

    def get_object(self):
        logger.debug(self.request.user.id)
        profile = get_user_model().objects.get(id=self.request.user.id)
        profile_as_dict = model_to_dict(profile)
        del profile_as_dict["password"]
        return profile_as_dict


@login_required()
def change_password(request):
    """
    Password update view
    :param request:
    :return:
    """
    form = PasswordUpdateForm(user=request.user)
    title = _("Modifier son mot de passe")
    icon = "fi-lock"
    js = ""
    if request.method == "POST":
        form = PasswordUpdateForm(user=request.user, data=request.POST)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)

    return render(
        request,
        "normal_form.html",
        {"form": form, "title": title, "icon": icon, "js": js},
    )


class UserListView(ManageAccountAuthMixin, TemplateView):
    template_name = "user_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fi-torsos-all-female"
        context["title"] = _("Rechercher un observateur")

        return context


class UserViewset(ManageAccountAuthMixin, ModelViewSet):
    model = Profile
    queryset = (
        Profile.objects.all().prefetch_related("organisms").prefetch_related("resp_areas").all()
    )
    serializer_class = ProfileSerializer

    def get_queryset(self):
        qs = super().get_queryset()
        return qs
