#!/bin/python3

"""
Accounts module - Filters
"""


import django_filters

from .forms import ProfileSearchFilterForm
from .models import Profile


class ProfileFilter(django_filters.FilterSet):
    """Profile filter"""

    username = django_filters.CharFilter(label="Utilisateur", lookup_expr="icontains")
    first_name = django_filters.CharFilter(label="Prénom", lookup_expr="icontains")
    last_name = django_filters.CharFilter(label="Nom de famille", lookup_expr="icontains")

    class Meta:
        model = Profile
        fields = [
            "username",
            "first_name",
            "last_name"
            # , 'addr_city_code'
        ]
        order_by = ["username"]
        form = ProfileSearchFilterForm
