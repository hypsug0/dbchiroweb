#!/bin/python3

"""
Accounts module - Models
"""

import uuid

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, UserManager
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.contrib.gis.db import models
from django.core.mail import send_mail
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from geodata.models import Areas

# Create your models here.


def get_sentinel_user():
    return get_user_model().objects.get_or_create(username="deleted")[0]


class Profile(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    Username and password are required. Other fields are optional.
    """

    username_validator = UnicodeUsernameValidator()
    uuid = models.UUIDField(default=uuid.uuid4, unique=True, editable=False)
    username = models.CharField(
        _("username"),
        max_length=150,
        unique=True,
        help_text=_("Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only."),
        validators=[username_validator],
        error_messages={"unique": _("A user with that username already exists.")},
    )
    first_name = models.CharField(_("first name"), max_length=30)
    last_name = models.CharField(_("last name"), max_length=30)
    email = models.EmailField(_("email address"), unique=True)
    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. "
            "Unselect this instead of deleting accounts."
        ),
    )
    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)
    is_resp = models.BooleanField(default=False, verbose_name=_("Responsable d'un territoire"))
    resp_areas = models.ManyToManyField(
        Areas,
        blank=True,
        verbose_name=_("Zonages du rôle de coordinateur"),
        related_name="resp_profiles",
    )
    access_all_data = models.BooleanField(
        default=False, verbose_name=_("Peut voir toutes les données")
    )
    edit_all_data = models.BooleanField(
        default=False, verbose_name=_("Peut éditer toutes les données")
    )
    export_all_data = models.BooleanField(
        default=False, verbose_name=_("Peut exporter toutes les données")
    )
    catchauth = models.ManyToManyField(
        "management.CatchAuth",
        blank=True,
        verbose_name=_("Authorisations de capture"),
        related_name="profile_catchauth",
    )
    organism = models.CharField(max_length=255, blank=True, null=True, verbose_name=_("Organisme"))
    home_phone = models.CharField(
        max_length=50, blank=True, null=True, verbose_name=_("Numéro de téléphone fixe")
    )
    mobile_phone = models.CharField(
        max_length=50,
        blank=True,
        null=True,
        verbose_name=_("Numéro de téléphone portable"),
    )
    addr_appt = models.CharField(
        max_length=50, blank=True, null=True, verbose_name=_("Appartement")
    )
    addr_building = models.CharField(
        max_length=255, blank=True, null=True, verbose_name=_("Immeuble")
    )
    addr_street = models.CharField(max_length=255, blank=True, null=True, verbose_name=_("Rue"))
    addr_city = models.CharField(max_length=255, blank=True, null=True, verbose_name=_("Commune"))
    addr_city_code = models.CharField(
        max_length=10, blank=True, null=True, verbose_name=_("Code postal")
    )
    addr_dept = models.CharField(
        max_length=255, blank=True, null=True, verbose_name=_("Département")
    )
    addr_country = models.CharField(max_length=255, blank=True, null=True, verbose_name=_("Pays"))
    comment = models.TextField(blank=True, null=True, verbose_name=_("Commentaire"))
    id_bdsource = models.CharField(max_length=100, blank=True, null=True)
    bdsource = models.CharField(max_length=100, blank=True, null=True)
    extra_data = models.JSONField(blank=True, null=True)
    geom = models.PointField(
        srid=settings.GEODATA_SRID,
        blank=True,
        null=True,
        verbose_name=_("Localisation géographique"),
    )
    gcu_accepted = models.BooleanField(
        blank=False,
        null=False,
        default=False,
        verbose_name=_("Conditions générales d'utilisation lues et acceptées"),
    )
    timestamp_create = models.DateTimeField(auto_now_add=True, editable=False)
    timestamp_update = models.DateTimeField(auto_now=True, editable=False)
    created_by = models.CharField(
        max_length=100, null=True, blank=True, db_index=True, editable=False
    )
    updated_by = models.CharField(
        max_length=100, null=True, blank=True, db_index=True, editable=False
    )

    objects = UserManager()

    EMAIL_FIELD = "email"
    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = ["email", "first_name", "last_name"]

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")

    def __str__(self):
        return f"{self.first_name} {self.last_name} - {self.username} ({self.email})"

    def save(self, *args, **kwargs):
        # self.username = generate_username(self.first_name, self.last_name)
        self.last_name = self.last_name.upper()
        super().save(*args, **kwargs)

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        return f"{self.first_name} {self.last_name}"

    @property
    def name_pretty(self):
        "If lastname exists, return firstname initial and lastname. Else return username."
        if self.last_name:
            first = self.first_name or " "
            return f"{first[0] + '.'} {self.last_name}".strip(" .")
        return self.username

    def email_user(self, subject, message, from_email=settings.DEFAULT_FROM_EMAIL, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def get_absolute_url(self):
        return reverse("accounts:user_detail", kwargs={"pk": self.id})


class UserFullName(Profile):
    class Meta:
        proxy = True

    def __str__(self):
        return self.full_name()
