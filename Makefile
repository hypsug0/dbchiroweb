$(eval SHELL:=/bin/bash)
DOCKER_USERNAME ?= dbchiro
APPLICATION_NAME ?= dbchiroweb
GIT_HASH ?= $(shell git log --format="%h" -n 1)

build:
	docker build --tag ${DOCKER_USERNAME}/${APPLICATION_NAME}:${GIT_HASH} .

release: build
	docker tag  ${DOCKER_USERNAME}/${APPLICATION_NAME}:${GIT_HASH} ${DOCKER_USERNAME}/${APPLICATION_NAME}:latest
	docker push ${DOCKER_USERNAME}/${APPLICATION_NAME}:latest

install:
	poetry install

run:
	poetry run python -m manage runserver

flake8:
	poetry run flake8 {accounts,api,blog,commons,core,dbchiro,dicts,exports,geodata,imports,management,metadata,sights,synthesis}

black:
	poetry run black {accounts,api,blog,commons,core,dbchiro,dicts,exports,geodata,imports,management,metadata,sights,synthesis}

pylint:
	poetry run pylint --load-plugins pylint_django --django-settings-module=dbchiro.settings {accounts,api,blog,commons,core,dbchiro,dicts,exports,geodata,imports,management,metadata,sights,synthesis}

requirements:
	poetry export -f requirements.txt -o requirements.txt --with=dev --without-hashes

test:
	poetry run python -m manage test
