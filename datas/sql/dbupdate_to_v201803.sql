/* Schéma de travail */

create schema data_to_reimport;

/* Table de sauvegarde des données concernées */
-- drop table data_to_reimport.agg_datas;
create table data_to_reimport.agg_datas as
select row_number() over ()        id,
       id_place                    id_place,
       sse.id_session              sse_id_session,
       sse.name                    sse_name,
       sse.date_start              sse_date_start,
       sse.time_start              sse_time_start,
       sse.date_end                sse_date_end,
       sse.time_end                sse_time_end,
       sse.data_file               sse_data_file,
       sse.is_confidential         sse_is_confidential,
       sse.comment                 sse_comment,
       sse.bdsource                sse_bdsource,
       sse.id_bdsource             sse_id_bdsource,
       sse.timestamp_create        sse_timestamp_create,
       sse.timestamp_update        sse_timestamp_update,
       sse.contact_id              sse_contact_id,
       sse.created_by_id           sse_created_by_id,
       sse.main_observer_id        sse_main_observer_id,
       sse.place_id                sse_place_id,
       sse.study_id                sse_study_id,
       sse.updated_by_id           sse_updated_by_id,
       sse.uuid                    sse_uuid,
       array_agg(sseoo.profile_id) sseoo_profile_id,
       ssi.id_sighting             ssi_id_sighting,
       ssi.period                  ssi_period,
       ssi.total_count             ssi_total_count,
       ssi.breed_colo              ssi_breed_colo,
       ssi.is_doubtful             ssi_is_doubtful,
       ssi.id_bdsource             ssi_id_bdsource,
       ssi.bdsource                ssi_bdsource,
       ssi.comment                 ssi_comment,
       ssi.timestamp_create        ssi_timestamp_create,
       ssi.timestamp_update        ssi_timestamp_update,
       ssi.codesp_id               ssi_codesp_id,
       ssi.created_by_id           ssi_created_by_id,
       ssi.observer_id             ssi_observer_id,
       ssi.session_id              ssi_session_id,
       ssi.updated_by_id           ssi_updated_by_id,
       ssi.uuid                    ssi_uuid,
       scd.id_countdetail          scd_countdetail,
       scd.time                    scd_time,
       scd.ab                      scd_ab,
       scd.d5                      scd_d5,
       scd.d3                      scd_d3,
       scd.pouce                   scd_pouce,
       scd.queue                   scd_queue,
       scd.tibia                   scd_tibia,
       scd.pied                    scd_pied,
       scd.cm3                     scd_cm3,
       scd.tragus                  scd_tragus,
       scd.poids                   scd_poids,
       scd.etat_sexuel             scd_etat_sexuel,
       scd.comment                 scd_comment,
       scd.id_fromsrc              scd_id_fromsrc,
       scd.bdsource                scd_bdsource,
       scd.other_imported_data     scd_other_imported_data,
       scd.timestamp_create        scd_timestamp_create,
       scd.timestamp_update        scd_timestamp_update,
       scd.age_id                  scd_age_id,
       scd.chinspot_id             scd_chinspot_id,
       scd.created_by_id           scd_created_by_id,
       scd.device_id               scd_device_id,
       scd.epididyme_id            scd_epididyme_id,
       scd.epiphyse_id             scd_epiphyse_id,
       scd.gestation_id            scd_gestation_id,
       scd.gland_coul_id           scd_gland_coul_id,
       scd.gland_taille_id         scd_gland_taille_id,
       scd.mamelle_id              scd_mamelle_id,
       scd.manipulator_id          scd_manipulator_id,
       scd.method_id               scd_method_id,
       scd.precision_id            scd_precision_id,
       scd.sex_id                  scd_sex_id,
       scd.sighting_id             scd_sighting_id,
       scd.testicule_id            scd_testicule_id,
       scd.transmitter_id          scd_transmitter_id,
       scd.tuniq_vag_id            scd_tuniq_vag_id,
       scd.unit_id                 scd_unit_id,
       scd.updated_by_id           scd_updated_by_id,
       scd.usure_dent_id           scd_usure_dent_id,
       scd.validator_id            scd_validator_id,
       scd.uuid                    scd_uuid
from sights_place sp
         inner join sights_session sse on sp.id_place = sse.place_id
         inner join sights_sighting ssi on sse.id_session = ssi.session_id
         left join sights_session_other_observer sseoo on sse.id_session = sseoo.session_id
         left join sights_countdetail scd on ssi.id_sighting = scd.sighting_id
where sse.contact_id in (5, 6, 7, 8, 9)
group by id_place,
         sse.id_session,
         sse.name,
         sse.date_start,
         sse.time_start,
         sse.date_end,
         sse.time_end,
         sse.data_file,
         sse.is_confidential,
         sse.comment,
         sse.bdsource,
         sse.id_bdsource,
         sse.timestamp_create,
         sse.timestamp_update,
         sse.contact_id,
         sse.created_by_id,
         sse.main_observer_id,
         sse.place_id,
         sse.study_id,
         sse.updated_by_id,
         sse.uuid,
         ssi.id_sighting,
         ssi.period,
         ssi.total_count,
         ssi.breed_colo,
         ssi.is_doubtful,
         ssi.id_bdsource,
         ssi.bdsource,
         ssi.comment,
         ssi.timestamp_create,
         ssi.timestamp_update,
         ssi.codesp_id,
         ssi.created_by_id,
         ssi.observer_id,
         ssi.session_id,
         ssi.updated_by_id,
         ssi.uuid,
         scd.id_countdetail,
         scd.time,
         scd.ab,
         scd.d5,
         scd.d3,
         scd.pouce,
         scd.queue,
         scd.tibia,
         scd.pied,
         scd.cm3,
         scd.tragus,
         scd.poids,
         scd.etat_sexuel,
         scd.comment,
         scd.id_fromsrc,
         scd.bdsource,
         scd.other_imported_data,
         scd.timestamp_create,
         scd.timestamp_update,
         scd.age_id,
         scd.chinspot_id,
         scd.created_by_id,
         scd.device_id,
         scd.epididyme_id,
         scd.epiphyse_id,
         scd.gestation_id,
         scd.gland_coul_id,
         scd.gland_taille_id,
         scd.mamelle_id,
         scd.manipulator_id,
         scd.method_id,
         scd.precision_id,
         scd.sex_id,
         scd.sighting_id,
         scd.testicule_id,
         scd.transmitter_id,
         scd.tuniq_vag_id,
         scd.unit_id,
         scd.updated_by_id,
         scd.usure_dent_id,
         scd.validator_id,
         scd.uuid
;

alter table data_to_reimport.agg_datas
    add column new_contact_id integer;
alter table data_to_reimport.agg_datas
    add column new_session_id integer;


/* Suppression des données dans dbChiro */
set role dbchirodbuser;
reset role;
grant usage on schema data_to_reimport to dbchirodbuser;
alter default privileges in schema data_to_reimport grant all privileges on tables to dbchirodbuser;
grant select, update, delete, insert on table data_to_reimport.agg_datas to dbchirodbuser;
delete
from sights_countdetail
where sighting_id in (select distinct scd_sighting_id from data_to_reimport.agg_datas);
delete
from sights_sighting
where session_id in (select distinct sse_id_session from data_to_reimport.agg_datas);
delete
from sights_session_other_observer
where session_id in (select distinct sse_id_session from data_to_reimport.agg_datas);
delete
from sights_session
where id_session in (select distinct sse_id_session from data_to_reimport.agg_datas);

/* TODO: Regénération des données sources dans dbChiro
   1. Création des sessions manquantes en VV et VM
   2. Mise en correspondance des données avec les sessions
   3. Création des observateurs secondaires
   4. Création des observations manquantes (espèces sur une session)
   5. Basculement des observations en données détaillées selon le contact devenu méthode)
   */
