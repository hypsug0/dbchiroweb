/*
DDL script for creating table and views
---------------------------------------
Import data table used to import datas into dbchiroweb table
*/

BEGIN
;

-- generate and fix potentiel errors on geom (due to x/y inversion)

UPDATE src_dbchirogcra.tmp_import
SET
    place_name=trim(place_name)
  , metaplace_name=trim(metaplace_name)
  , geom = st_setsrid(st_makepoint((CASE WHEN place_x > place_y THEN place_y ELSE place_x END),
                                   (CASE WHEN place_x > place_y THEN place_x ELSE place_y END)), 4326)
;

UPDATE src_dbchirogcra.tmp_import
SET
    place_name=trim(place_name)
  , metaplace_name=trim(metaplace_name)
  , geom = st_setsrid(st_makepoint(place_x, place_y), 4326)
;

-- Import studies

INSERT INTO
    src_dbchirogcra.management_study( name
                                    , year
                                    , public_funding
                                    , public_report
                                    , public_raw_data
                                    , confidential
                                    , confidential_end_date
                                    , type_etude
                                    , type_espace
                                    , comment
                                    , timestamp_create
                                    , timestamp_update
                                    , created_by_id
                                    , project_manager_id
                                    , updated_by_id
                                    , uuid)
SELECT
    name
  , year
  , public_funding
  , public_report
  , public_raw_data
  , confidential
  , confidential_end_date
  , type_etude
  , type_espace
  , comment
  , timestamp_create
  , timestamp_update
  , created_by_id
  , project_manager_id
  , updated_by_id
  , uuid
    FROM
        src_dbchirogcra.v_import_studies
ON CONFLICT(uuid) DO NOTHING
;


-- Import places

INSERT INTO
    src_dbchirogcra.sights_place( uuid
                                , name
                                , is_hidden
                                , is_gite
                                , is_managed
                                , proprietary
                                , convention
                                , convention_file
                                , map_file
                                , photo_file
                                , habitat
                                , altitude
                                , id_bdcavite
                                , plan_localite
                                , comment
                                , other_imported_data
                                , telemetric_crossaz
                                , bdsource
                                , id_bdsource
                                , x
                                , y
                                , geom
                                , timestamp_create
                                , timestamp_update
                                , created_by_id
                                , domain_id
                                , landcover_id
                                , metaplace_id
                                , municipality_id
                                , precision_id
                                , territory_id
                                , type_id
                                , updated_by_id
                                , extra_data)
SELECT
    uuid
  , name
  , is_hidden
  , is_gite
  , is_managed
  , proprietary
  , convention
  , convention_file
  , map_file
  , photo_file
  , habitat
  , altitude
  , id_bdcavite
  , plan_localite
  , comment
  , other_imported_data
  , telemetric_crossaz
  , bdsource
  , id_bdsource
  , x
  , y
  , geom
  , timestamp_create
  , timestamp_update
  , created_by_id
  , domain_id
  , landcover_id
  , metaplace_id
  , municipality_id
  , precision_id
  , territory_id
  , type_id
  , updated_by_id
  , extra_data
    FROM
        src_dbchirogcra.v_import_place
ON CONFLICT (uuid) DO NOTHING
;

-- import sessions


INSERT INTO
    src_dbchirogcra.sights_session( uuid
                                  , name
                                  , date_start
                                  , time_start
                                  , date_end
                                  , time_end
                                  , data_file
                                  , is_confidential
                                  , comment
                                  , bdsource
                                  , id_bdsource
                                  , timestamp_create
                                  , timestamp_update
                                  , contact_id
                                  , created_by_id
                                  , main_observer_id
                                  , place_id
                                  , study_id
                                  , updated_by_id
                                  , extra_data)
SELECT
    uuid
  , name
  , date_start
  , time_start
  , date_end
  , time_end
  , data_file
  , is_confidential
  , comment
  , bdsource
  , id_bdsource
  , timestamp_create
  , timestamp_update
  , contact_id
  , created_by_id
  , main_observer_id
  , place_id
  , study_id
  , updated_by_id
  , extra_data
    FROM
        src_dbchirogcra.v_import_session
    WHERE
        id_session IS NULL
ON CONFLICT(uuid)
    DO NOTHING
;

-- Import observations


INSERT INTO
    src_dbchirogcra.sights_sighting( uuid
                                   , period
                                   , total_count
                                   , breed_colo
                                   , is_doubtful
                                   , id_bdsource
                                   , bdsource
                                   , comment
                                   , timestamp_create
                                   , timestamp_update
                                   , codesp_id
                                   , created_by_id
                                   , observer_id
                                   , session_id
                                   , updated_by_id
                                   , extra_data)
SELECT
    new_uuid
  , period
  , total_count
  , breed_colo
  , is_doubtful
  , id_bdsource
  , bdsource
  , comment
  , timestamp_create
  , timestamp_update
  , codesp_id
  , created_by_id
  , observer_id
  , session_id
  , updated_by_id
  , extra_data
    FROM
        src_dbchirogcra.v_import_sightings
ON CONFLICT (uuid) DO NOTHING
;


-- Import countdetail

INSERT INTO
    src_dbchirogcra.sights_countdetail( uuid
                                      , count
                                      , comment
                                      , timestamp_create
                                      , timestamp_update
                                      , method_id
                                      , precision_id
                                      , sighting_id
                                      , unit_id
                                      , updated_by_id
                                      , extra_data)
SELECT
    uuid_generate_v4()
  , count
  , comment
  , timestamp_create
  , timestamp_update
  , method_id
  , precision_id
  , id_sighting
  , unit_id
  , updated_by_id
  , extra_data
    FROM
        src_dbchirogcra.v_import_sightings_with_single_countdetail
;

COMMIT
;
