"""dbchiro URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  path(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import static
from django.contrib.auth.urls import views
from django.contrib.gis import admin
from django.urls import include, path, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

from dbchiro.settings import REGISTRATION_APPROVAL

admin.autodiscover()
admin.site.index_title = "Tables de données"
admin.site.site_header = "Interface de gestion"

schema_view = get_schema_view(
    openapi.Info(
        title="dbChiroWev API",
        default_version="v1",
        description="dbChiro API docs",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="contact@snippets.local"),
        license=openapi.License(name="AGPL v3"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

if REGISTRATION_APPROVAL:
    registration_include = "registration.backends.admin_approval.urls"
else:
    registration_include = "registration.backends.default.urls"

urlpatterns = [
    re_path(
        r"^swagger(?P<format>\.json|\.yaml)$",
        schema_view.without_ui(cache_timeout=0),
        name="schema-json",
    ),
    path(
        "swagger/",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    path("redoc/", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"),
    path(
        "accounts/",
        include(registration_include),
        name="registration",
    ),
    path("password_reset/", views.PasswordResetView.as_view(), name="password_reset"),
    path(
        "password_reset/done/",
        views.PasswordResetDoneView.as_view(),
        name="password_reset_done",
    ),
    re_path(
        r"^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$",
        views.PasswordResetConfirmView.as_view(),
        name="password_reset_confirm",
    ),
    path(
        "reset/done/",
        views.PasswordResetCompleteView.as_view(),
        name="password_reset_complete",
    ),
    path("admin/", admin.site.urls),
    path("admin/doc/", include("django.contrib.admindocs.urls"), name="dbgestion_doc"),
    path("captcha/", include("captcha.urls")),
    path("", include("core.urls", namespace="core")),
    path("accounts/", include("accounts.urls", namespace="accounts")),
    path("", include("blog.urls", namespace="blog")),
    path("", include("exports.urls", namespace="exports")),
    path("", include("sights.urls", namespace="sights")),
    path("", include("management.urls", namespace="management")),
    path("", include("dicts.urls", namespace="dicts")),
    path("", include("geodata.urls", namespace="geodata")),
    # path("", include("metadata.urls", namespace="metadata")),
    path("synthesis/", include("synthesis.urls", namespace="synthesis")),
    path("api/", include("api.urls", namespace="api")),
] + static.static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
